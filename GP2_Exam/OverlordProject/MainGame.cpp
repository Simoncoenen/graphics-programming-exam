//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"

#include "MainGame.h"
#include "Base\GeneralStructs.h"
#include "Scenegraph\SceneManager.h"
#include "Physx\PhysxProxy.h"
#include "Diagnostics\DebugRenderer.h"

#define EXAM

#ifdef W1
#include "CourseObjects/Week 1/TestScene.h"
#include "CourseObjects/Week 1/ComponentTestScene.h"
#endif

#ifdef W2
#include "CourseObjects/Week 2/ModelTestScene.h"
#include "CourseObjects/Week 2/CharacterTest.h"
#endif

#ifdef W3
#include "CourseObjects/Week 3/SpikeyScene.h"
#include "CourseObjects/Week 3/SpriteTestScene.h"
#include "CourseObjects/Week 3/UberShaderTestScene.h"
#endif

#ifdef W4
#include "CourseObjects/Week 4/RaycastTestScene.h"
#endif

#ifdef W5
#include "CourseObjects/Week 5/TerrainScene.h"
#include "CourseObjects/Week 5/SkyboxScene.h"
#endif

#ifdef W6
#include "CourseObjects/Week 6/SoftwareSkinningScene_1.h"
#include "CourseObjects/Week 6/SoftwareSkinningScene_2.h"
#include "CourseObjects/Week 6/SoftwareSkinningScene_3.h"
#endif W6

#ifdef W7
#include "CourseObjects/Week 7/HardwareSkinningScene.h"
#endif

#ifdef W8
#include "CourseObjects/Week 8/PostProcessingScene.h"
#endif

#ifdef W9
#include "CourseObjects/Week 9/ShadowMappingScene.h"
#endif

#ifdef W10
#include "CourseObjects/Week 10/ParticleTestScene.h"
#endif

#ifdef EXAM
#include "Exam/Scenes/MainMenu.h"
#endif

#ifdef SHADER
#include "GeometryShader/GeometryShaderScene.h"
#endif

MainGame::MainGame(void)
{
}

MainGame::~MainGame(void)
{
}

//Game is preparing
void MainGame::OnGamePreparing(GameSettings& gameSettings)
{
	//Game resolution
	gameSettings.Window.Width = 1240;
	gameSettings.Window.Height = 720;
	gameSettings.Window.VerticalSync = true;
	gameSettings.Window.Style = WindowStyle::BORDERLESS;
	gameSettings.Window.Title = L"BoxHead 3D - Simon Coenen";

	//Enable logging to file
	Logger::StartFileLogging(L"./Resources/GameLog.txt");
}

void MainGame::Initialize()
{

#ifdef W1
	SceneManager::GetInstance()->AddGameScene(new TestScene());
	SceneManager::GetInstance()->AddGameScene(new ComponentTestScene());
#endif

#ifdef W2
	SceneManager::GetInstance()->AddGameScene(new ModelTestScene());
	SceneManager::GetInstance()->AddGameScene(new CharacterTest());

	SceneManager::GetInstance()->SetActiveGameScene(L"CharacterTest");
#endif

#ifdef W3
	SceneManager::GetInstance()->AddGameScene(new SpikeyScene());
	SceneManager::GetInstance()->AddGameScene(new SpriteTestScene());
	SceneManager::GetInstance()->AddGameScene(new UberShaderTestScene());
	SceneManager::GetInstance()->SetActiveGameScene(L"SpriteTestScene");
#endif

#ifdef W4
	SceneManager::GetInstance()->AddGameScene(new RaycastTestScene());
#endif

#ifdef W5
	SceneManager::GetInstance()->AddGameScene(new TerrainScene());
	SceneManager::GetInstance()->AddGameScene(new SkyboxScene());
#endif

#ifdef W6
	SceneManager::GetInstance()->AddGameScene(new SoftwareSkinningScene_1());
	SceneManager::GetInstance()->AddGameScene(new SoftwareSkinningScene_2());
	SceneManager::GetInstance()->AddGameScene(new SoftwareSkinningScene_3());
	SceneManager::GetInstance()->SetActiveGameScene(L"SoftwareSkinningScene_3");
#endif

#ifdef W7
	SceneManager::GetInstance()->AddGameScene(new HardwareSkinningScene());
#endif

#ifdef W8
	SceneManager::GetInstance()->AddGameScene(new PostProcessingScene());
#endif

#ifdef W9
	SceneManager::GetInstance()->AddGameScene(new ShadowMappingScene());
#endif

#ifdef W10
	SceneManager::GetInstance()->AddGameScene(new ParticleTestScene());
#endif

#ifdef EXAM
	//With menu, only add the MainMenu. The MainMenu scene itself adds the ExamScene so the player can choose levels
	SceneManager::GetInstance()->AddGameScene(new MainMenu());
	//SceneManager::GetInstance()->AddGameScene(new DeathScene());
	//SceneManager::GetInstance()->AddGameScene(new ExamScene());
#endif

#ifdef SHADER
	SceneManager::GetInstance()->AddGameScene(new GeometryShaderScene());
#endif
}

LRESULT MainGame::WindowProcedureHook(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(hWnd);
	//UNREFERENCED_PARAMETER(message);
	//UNREFERENCED_PARAMETER(wParam);
	//UNREFERENCED_PARAMETER(lParam);

	switch (message)
	{
		case WM_KEYUP:
		{
			if ((lParam & 0x80000000) != 0x80000000)
				return -1;

			//NextScene
			if (wParam == VK_F3)
			{
				SceneManager::GetInstance()->NextScene();
				return 0;
			}
			//PreviousScene
			if (wParam == VK_F2)
			{
				SceneManager::GetInstance()->PreviousScene();
				return 0;
			}
			if (wParam == VK_F4)
			{
				DebugRenderer::ToggleDebugRenderer();
				return 0;
			}

			if (wParam == VK_F6)
			{
				auto activeScene = SceneManager::GetInstance()->GetActiveScene();
				activeScene->GetPhysxProxy()->NextPhysXFrame();
			}
		}
	}

	return -1;
}
