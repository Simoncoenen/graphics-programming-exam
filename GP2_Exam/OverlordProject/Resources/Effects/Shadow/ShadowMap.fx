float4x4 gWorld : WORLD;
float4x4 gLightVP : LIGHT;
float4x4 gBones[70];
bool gHasBones = false;

DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
};

RasterizerState NoCulling 
{ 
	CullMode = NONE; 
};

struct VS_INPUT
{
	float3 Position : POSITION;
	//BlendWeights & BlendIndices
	float4 blendIndex : BLENDINDICES;
	float4 blendWeight : BLENDWEIGHTS;
};

struct PS_INPUT
{
	float4 Position : SV_POSITION;	
};

PS_INPUT VS(VS_INPUT input)
{
	PS_INPUT output = (PS_INPUT)0;

	//Skinning
	float4 originalPosition = float4(input.Position, 1);
	float4 transformedPosition = 0;

	if(gHasBones)
	{
		for(int i = 0; i < 4; ++i)
		{
			int boneIdx = input.blendIndex[i];
			if(boneIdx > -1)
			{
				transformedPosition += input.blendWeight[i] * mul(originalPosition, gBones[boneIdx]);
				transformedPosition.w = 1;
			}
		}
		output.Position = mul(transformedPosition, mul(gWorld, gLightVP));
		return output;
	}
	output.Position = mul(originalPosition, mul(gWorld, gLightVP));
	return output;
	
}

void PS(PS_INPUT input)
{
	
}

technique10 Default
{
	pass P0
    {
   		SetRasterizerState(NoCulling);
		SetDepthStencilState(EnableDepth, 0);
        SetVertexShader( CompileShader( vs_4_0, VS() ) );
        SetGeometryShader( NULL );
        SetPixelShader( CompileShader( ps_4_0, PS() ) );
    }
}