#pragma once
#include "Graphics\Material.h"

class ColorMaterial: public Material
{
public:
	ColorMaterial(bool enableTransparency = false);
	~ColorMaterial();
	void SetColor(const XMFLOAT4& color);
protected:
	virtual void LoadEffectVariables();
	virtual void UpdateEffectVariables(const GameContext& gameContext, ModelComponent* pModelComponent);
private:
	static ID3DX11EffectVectorVariable* m_pColorVariable;
	XMFLOAT4 m_Color = {0.9f, 0.9f, 0.9f, 1.0f};
private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	ColorMaterial(const ColorMaterial &obj);
	ColorMaterial& operator=(const ColorMaterial& obj);
};

