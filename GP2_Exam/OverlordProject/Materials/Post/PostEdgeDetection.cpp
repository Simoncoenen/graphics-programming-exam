//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"
#include "PostEdgeDetection.h"
#include "Graphics/RenderTarget.h"

ID3DX11EffectShaderResourceVariable*	PostEdgeDetection::m_pTextureMapVariable = nullptr;
ID3DX11EffectShaderResourceVariable*	PostEdgeDetection::m_pDepthTextureVariable = nullptr;
ID3DX11EffectScalarVariable*			PostEdgeDetection::m_pThresholdVariable = nullptr;

PostEdgeDetection::PostEdgeDetection()
	: PostProcessingMaterial(L"./Resources/Effects/Post/EdgeDetection.fx")
{
}

PostEdgeDetection::~PostEdgeDetection(void)
{
}

void PostEdgeDetection::SetThreshold(const float threshold)
{
	m_Threshold = threshold;
}

void PostEdgeDetection::LoadEffectVariables()
{
	m_pTextureMapVariable = m_pEffect->GetVariableByName("gTexture")->AsShaderResource();
	if (!m_pTextureMapVariable->IsValid())
		Logger::LogWarning(L"PostEdgeDetection: GetVariableByName 'gTexture' not valid!");

	m_pDepthTextureVariable = m_pEffect->GetVariableByName("gDepth")->AsShaderResource();
	if (!m_pDepthTextureVariable->IsValid())
		Logger::LogWarning(L"PostEdgeDetection: GetVariableByName 'gDepth' not valid!");

	m_pThresholdVariable = m_pEffect->GetVariableByName("gThreshold")->AsScalar();
	if (!m_pThresholdVariable->IsValid())
		Logger::LogWarning(L"PostEdgeDetection: GetVariableByName 'gThreshold' not valid!");
}

void PostEdgeDetection::UpdateEffectVariables(RenderTarget* rendertarget)
{
	m_pTextureMapVariable->SetResource(rendertarget->GetShaderResourceView());
	m_pDepthTextureVariable->SetResource(rendertarget->GetDepthShaderResourceView());
	m_pThresholdVariable->SetFloat(m_Threshold);
}