#pragma once
#include "Graphics/PostProcessingMaterial.h"

class ID3D11EffectShaderResourceVariable;

class PostVignette : public PostProcessingMaterial
{
public:
	PostVignette();
	~PostVignette();

	void SetRadius(const float radius) { m_Radius = radius; }
	void SetSmoothness(const float smoothness) { m_Smoothness = smoothness; }
	void SetColor(const XMFLOAT4& color) { m_Color = color; }
	void SetCenter(const XMFLOAT2& center) { m_Center = center; }
	const float& GetRadius() const { return m_Radius; }
	const float& GetSmoothness() const { return m_Smoothness; }

protected:
	virtual void LoadEffectVariables();
	virtual void UpdateEffectVariables(RenderTarget* rendertarget);

	static ID3DX11EffectShaderResourceVariable*	m_pTextureMapVariable;
	static ID3DX11EffectScalarVariable*			m_pRadiusVariable;
	static ID3DX11EffectScalarVariable*			m_pSmoothnessVariable;
	static ID3DX11EffectVectorVariable*			m_pColorVariable;
	static ID3DX11EffectVectorVariable*			m_pCenterVariable;

	float m_Radius = 0.75f;
	float m_Smoothness = 0.2f;
	XMFLOAT2 m_Center = { 0.5f , 0.5f };
	XMFLOAT4 m_Color = {0.0f, 0.0f, 0.0f, 1.0f};
private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	PostVignette(const PostVignette &obj);
	PostVignette& operator=(const PostVignette& obj);
};
