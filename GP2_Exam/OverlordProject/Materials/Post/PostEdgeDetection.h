#pragma once
#include "Graphics/PostProcessingMaterial.h"

class ID3D11EffectShaderResourceVariable;

class PostEdgeDetection : public PostProcessingMaterial
{
public:
	PostEdgeDetection();
	~PostEdgeDetection();

	void SetThreshold(const float threshold);

protected:
	virtual void LoadEffectVariables();
	virtual void UpdateEffectVariables(RenderTarget* rendertarget);

	static ID3DX11EffectShaderResourceVariable*	m_pTextureMapVariable;
	static ID3DX11EffectShaderResourceVariable*	m_pDepthTextureVariable;
	static ID3DX11EffectScalarVariable*			m_pThresholdVariable;
	float m_Threshold = 0.000005f;
private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	PostEdgeDetection(const PostEdgeDetection &obj);
	PostEdgeDetection& operator=(const PostEdgeDetection& obj);
};
