//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"
#include "PostGrayscale.h"
#include "Graphics/RenderTarget.h"

ID3DX11EffectShaderResourceVariable* PostGrayscale::m_pTextureMapVariabele = nullptr;
ID3DX11EffectScalarVariable* PostGrayscale::m_pStrengthVariable = nullptr;

PostGrayscale::PostGrayscale()
	: PostProcessingMaterial(L"./Resources/Effects/Post/Grayscale.fx", L"GrayscaleLuminosity")
{
}

PostGrayscale::~PostGrayscale(void)
{
}

void PostGrayscale::SetStrength(const float strength)
{
	m_pStrengthVariable->SetFloat(strength);
}

void PostGrayscale::LoadEffectVariables()
{
	//Bind the 'gTexture' variable with 'm_pTextureMapVariable'
	//Check if valid!
	m_pTextureMapVariabele = m_pEffect->GetVariableByName("gTexture")->AsShaderResource();
	if (!m_pTextureMapVariabele->IsValid())
		Logger::LogWarning(L"PostGrayscale: GetVariableByName 'gTexture' not valid!");

	m_pStrengthVariable = m_pEffect->GetVariableByName("gStrength")->AsScalar();
	if(!m_pStrengthVariable->IsValid())
		Logger::LogWarning(L"PostGrayscale: GetVariableByName 'gStrength' not valid!");
}

void PostGrayscale::UpdateEffectVariables(RenderTarget* rendertarget)
{
	//Update the TextureMapVariable with the Color ShaderResourceView of the given RenderTarget
	m_pTextureMapVariabele->SetResource(rendertarget->GetShaderResourceView());
}