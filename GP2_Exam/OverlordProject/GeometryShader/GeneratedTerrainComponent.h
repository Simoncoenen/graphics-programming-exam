#pragma once
#include <vector>
#include <Components/BaseComponent.h>
class RenderTarget;

class GeneratedTerrainComponent : public BaseComponent
{
public:
	GeneratedTerrainComponent(int width, int depth);
	virtual ~GeneratedTerrainComponent(void);

	void Generate();
	//SET
	void SetLacunarity(const float lac) { m_Lacunarity = lac; Clamp(m_Lacunarity, 20.0f, 1.0f); }
	void SetSeed(const int seed) { m_Seed = seed; }
	void SetScale(const float scale) { m_Scale = scale; Clamp(m_Scale, 200.0f, 50.0f); }
	void SetOctaves(const int octaves) { m_Octaves = octaves; Clamp(m_Octaves, 15, 0); }
	void SetPersistance(const float persistance) { m_Persistance = persistance; Clamp(m_Persistance, 1.0f, 0.0f); }
	//GET
	float GetLacunarity() const { return m_Lacunarity; }
	float GetScale() const { return m_Scale; }
	float GetPersistance() const { return m_Persistance; }
	int GetSeed() const { return m_Seed; }
	int GetOctaves() const { return m_Octaves; }

	void SetTechnique(int idx) { m_pTechnique = m_pEffect->GetTechniqueByIndex(idx); }

protected:
	
	virtual void Update(const GameContext& gameContext);
	virtual void Draw(const GameContext& gameContext);
	virtual void Initialize(const GameContext& gameContext);

private:
	void UpdateBuffer();
	void LoadEffect(const GameContext& gameContext);
	void CreateVertexBuffer(const GameContext& gameContext);
	void CreateInputLayout(const GameContext& gameContext, ID3DX11EffectTechnique* pTechnique);

	float InverseLerp(const float min, const float max, const float value) const { return (value - min) / (max - min); }

	size_t m_Capacity;
	float m_Lacunarity = 2.2f;
	float m_Persistance = 0.2f;
	float m_Scale = 150.0f;
	int m_Octaves = 4;
	int m_Seed = 0;
	int m_Width;
	int m_Depth;
	vector<XMFLOAT3> m_Vertices;

	//D3D
	ID3D11Buffer *m_pVertexBuffer = nullptr;
	ID3DX11Effect* m_pEffect = nullptr;
	ID3DX11EffectTechnique* m_pTechnique = nullptr;
	ID3D11InputLayout* m_pInputLayout = nullptr;

	//Shader vaiables
	static ID3DX11EffectMatrixVariable* m_pWorldVar;
	static ID3DX11EffectMatrixVariable* m_pWvpVar;
	static ID3DX11EffectShaderResourceVariable* m_pTextureVariable;
	
private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	GeneratedTerrainComponent(const GeneratedTerrainComponent& yRef);									
	GeneratedTerrainComponent& operator=(const GeneratedTerrainComponent& yRef);

};

