//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"

#include "Character.h"
#include "Components\Components.h"
#include "Prefabs\Prefabs.h"
#include "Physx/PhysxManager.h"

Character::Character(float radius, float height, float moveSpeed) :
	m_pCamera(nullptr),
	m_pController(nullptr),
	m_TotalPitch(0),
	m_TotalYaw(0),
	m_MoveSpeed(moveSpeed),
	m_RotationSpeed(90.f),
	m_Radius(radius),
	m_Height(height),
	//Running
	m_MaxRunVelocity(50.0f),
	m_TerminalVelocity(20),
	m_Gravity(9.81f),
	m_RunAccelerationTime(0.3f),
	m_JumpAccelerationTime(0.8f),
	m_RunAcceleration(m_MaxRunVelocity / m_RunAccelerationTime),
	m_JumpAcceleration(m_Gravity / m_JumpAccelerationTime),
	m_RunVelocity(0),
	m_JumpVelocity(0),
	m_Velocity(0, 0, 0)
{
}


Character::~Character(void)
{
}

void Character::Initialize(const GameContext& gameContext)
{

	PxPhysics * physX = PhysxManager::GetInstance()->GetPhysics();

	// Create controller
	PxMaterial* defaultMaterial = physX->createMaterial(0, 0, 0);
	m_pController = new ControllerComponent(defaultMaterial);
	GameObject* character = new GameObject();
	character->AddComponent(m_pController);
	AddChild(character);

	// Add a fixed camera as child
	m_pCamera = new CameraComponent();
	FixedCamera* camera = new FixedCamera();
	camera->AddComponent(m_pCamera);
	character->AddChild(camera);

	// Register all Input Actions
	gameContext.pInput->AddInputAction(InputAction(LEFT, Down, VK_LEFT));
	gameContext.pInput->AddInputAction(InputAction(RIGHT, Down, VK_RIGHT));
	gameContext.pInput->AddInputAction(InputAction(FORWARD, Down, VK_UP));
	gameContext.pInput->AddInputAction(InputAction(BACKWARD, Down, VK_DOWN));
	gameContext.pInput->AddInputAction(InputAction(JUMP, Down, VK_SPACE));
}

void Character::PostInitialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	// Set the camera as active
	// We need to do this in the PostInitialize because child game objects only get initialized after the Initialize of the current object finishes
	m_pCamera->SetActive();
}

XMFLOAT3 operator*(const float &a, const XMFLOAT3 &b)
{
	XMFLOAT3 o;
	o.x = a * b.x;
	o.y = a * b.y;
	o.z = a * b.z;
	return o;
}

XMFLOAT3 operator+(const XMFLOAT3 &a, const XMFLOAT3 &b)
{
	XMFLOAT3 o;
	o.x = a.x + b.x;
	o.y = a.y + b.y;
	o.z = a.z + b.z;
	return o;
}

float Clamp(const float i, const float min, const float max)
{
	if (i > max)
		return max;
	if (i < min)
		return min;
	return i;
}


void Character::Update(const GameContext& gameContext)
{
	float deltaTime = gameContext.pGameTime->GetElapsed();

	//Mouse looking
	POINT mouseMovement = { 0,0 };
	if (gameContext.pInput->IsMouseButtonDown(VK_LBUTTON))
		mouseMovement = gameContext.pInput->GetMouseMovement();
	m_TotalYaw += mouseMovement.x * deltaTime * m_RotationSpeed;
	m_TotalPitch = Clamp(m_TotalPitch + mouseMovement.y * deltaTime * m_RotationSpeed, -70, 70);
	m_pCamera->GetTransform()->Translate(0.0f, 0.0f, 0.0f);
	m_pCamera->GetTransform()->Rotate(m_TotalPitch, 0.0f, 0.0f);
	m_pController->GetTransform()->Rotate(0.0f, m_TotalYaw, 0.0f);

	XMFLOAT3 forward = m_pCamera->GetTransform()->GetForward();
	XMFLOAT3 right = m_pCamera->GetTransform()->GetRight();

	//Movement
	bool isMoving = false;

	if (gameContext.pInput->IsActionTriggered(FORWARD))
	{
		isMoving = true;
		m_Velocity.z = Clamp(m_Velocity.z + m_RunAcceleration * deltaTime, -m_MaxRunVelocity, m_MaxRunVelocity);
	}
	else if (gameContext.pInput->IsActionTriggered(BACKWARD))
	{
		isMoving = true;
		m_Velocity.z = Clamp(m_Velocity.z - m_RunAcceleration * deltaTime, -m_MaxRunVelocity, m_MaxRunVelocity);
	}
	if (gameContext.pInput->IsActionTriggered(RIGHT))
	{
		isMoving = true;
		m_Velocity.x = Clamp(m_Velocity.x + m_RunAcceleration * deltaTime, -m_MaxRunVelocity, m_MaxRunVelocity);
	}
	else if (gameContext.pInput->IsActionTriggered(LEFT))
	{
		isMoving = true;
		m_Velocity.x = Clamp(m_Velocity.x - m_RunAcceleration * deltaTime, -m_MaxRunVelocity, m_MaxRunVelocity);
	}

	if (isMoving == false)
		m_Velocity = { 0,0,0 };

	//Jumping
	if(m_pController->GetCollisionFlags() != PxControllerCollisionFlag::eCOLLISION_DOWN)
	{
		m_JumpVelocity = Clamp(m_JumpVelocity - m_JumpAcceleration * deltaTime, 0, m_MaxRunVelocity);
		m_FlyTime += deltaTime;
	}
	else
	{
		m_FlyTime = 0;
		if (gameContext.pInput->IsActionTriggered(JUMP))
		{
			m_JumpVelocity = 200.0f;
		}
	}

	m_Velocity.y = m_JumpVelocity - m_Gravity * m_FlyTime;

	XMFLOAT3 deltaMove;
	right.y = 0;
	forward.y = 0;
	deltaMove = m_Velocity.x * right + m_Velocity.z * forward;
	deltaMove.y = m_Velocity.y;

	m_pController->Move(deltaTime * deltaMove);
}