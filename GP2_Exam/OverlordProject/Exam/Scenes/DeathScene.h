#pragma once
#include "Scenegraph/GameScene.h"
#pragma region
class Button;
class ButtonManager;
class TextureData;
class SpriteFont;
class MenuSelector;
class WindowSettings;
class HighscoreEntry;
#pragma endregion Class Forwards

class DeathScene : public GameScene
{
public:
	DeathScene(void);
	virtual ~DeathScene(void);
	
	void SetScore(const int score);

protected:

	virtual void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);
	virtual void Draw(const GameContext& gameContext);

private:

	void InitializeInput(const GameContext &gameContext);

	enum InputKey
	{
		RETURN
	};

	TextureData* m_pMenuBackground = nullptr;

	void CreateButtons();

	int m_Score = 0;

	SpriteFont* m_pSmallFont = nullptr;
	SpriteFont* m_pBigFont = nullptr;
	SpriteFont* m_pTinyFont = nullptr;

	XMFLOAT2 m_ScreenMiddle;
	GameSettings::WindowSettings* m_WindowSettings = nullptr;
	Button* m_pBackButton = nullptr;

	//Sounds
	FMOD::Sound* m_pDefeatSound = nullptr;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	DeathScene(const DeathScene &obj);
	DeathScene& operator=(const DeathScene& obj);
};

