#pragma once
#include "Scenegraph/GameScene.h"
#pragma region
class Player;
class TopDownCamera;

//Managers
class CrateManager;
class EnemyManager;
class LevelLoader;

//Pathfinding
class Grid;
class Pathfinding;

#pragma endregion Class Forwards

class ExamScene : public GameScene
{
public:
	ExamScene(void);
	virtual ~ExamScene(void);

	void SetLevel(const wstring& level) { m_LevelToLoad = level; }

protected:

	virtual void Initialize(const GameContext& gameContext);
	virtual void Update(const GameContext& gameContext);
	virtual void Draw(const GameContext& gameContext);

private:
	void InitializePlayer();
	void InitializeCamera();
	void LoadMaterials(const GameContext& gameContext);

	Player* m_pPlayer;
	TopDownCamera* m_pCamera;

	wstring m_LevelToLoad;

	//Managers
	CrateManager* m_pCrateManager;
	EnemyManager* m_pEnemyManager;
	LevelLoader* m_pLevelLoader;

	//Pathfinding
	Grid* m_pGrid;
	XMFLOAT3 m_InitLightPos;

	FMOD::Sound* m_pBackgroundMusic = nullptr;

	//Highscores
	float m_DeathScreenDelayTimer = 0.0f;
	float m_PlayTime = 0.0f;
	bool m_RegisterHighscore = true;
	bool m_DeathSequenceCompleted = false;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	ExamScene(const ExamScene &obj);
	ExamScene& operator=(const ExamScene& obj);
};

