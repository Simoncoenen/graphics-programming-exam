//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"
#pragma region
#include "ExamScene.h"
#include "Scenegraph\GameObject.h"
#include "Components/Components.h"
#include "../Helpers/Enumerations.h"
#include "Physx/PhysxProxy.h"

//Prefabs
#include "./Exam/Prefabs/TopDownCamera.h"
#include "./Exam/Prefabs/Player.h"

//Managers
#include "../Managers/LevelLoader.h"
#include "../Managers/CrateManager.h"
#include "../Managers/EnemyManager.h"
#include "../Managers/DecalManager.h"

//AI
#include <Exam/AI/Pathfinding/Grid.h>
#include <Exam/AI/Pathfinding/Pathfinding.h>

//Materials
#include <Graphics/ShadowMapRenderer.h>
#include <Materials/SelfIlluminatedMaterial.h>
#include <Materials/Shadow/SkinnedDiffuseMaterial_Shadow.h>
#include <Materials/Shadow/DiffuseMaterial_Shadow.h>

//Post processing
#include <Materials/Post/PostEdgeDetection.h>
#include <Materials/Post/PostVignette.h>
#include <Base/SoundManager.h>
#include <Exam/Managers/Leaderboards/OfflineLeaderboard.h>
#include "DeathScene.h"
#include <Exam/Managers/Leaderboards/OnlineLeaderboard.h>
#include <Scenegraph/SceneManager.h>
#include <Materials/Post/PostGrayscale.h>
#include <Exam/Helpers/MathHelp.h>
#include <Diagnostics/DebugRenderer.h>

#define EDGE_DETECTION
#define RANDOM_SEED
#define ONLINE_HIGHSCORES
//#define DEBUG_AI

#pragma endregion Includes

ExamScene::ExamScene(void) :
	GameScene(L"ExamScene"),
	m_pPlayer(nullptr),
	m_pCamera(nullptr),
	m_pCrateManager(nullptr),
	m_pEnemyManager(nullptr),
	m_pLevelLoader(nullptr),
	m_pGrid(nullptr)
{
}

ExamScene::~ExamScene(void)
{
	SafeDelete(m_pGrid);
	SafeDelete(m_pLevelLoader);
	Pathfinding::DestroyInstance();
	DecalManager::DestroyInstance();
}

//Game has to be loaded in a specific order:
/*
1. LevelLoader
2. Player							(needs LevelLoader)
3. CrateManager						(needs LevelLoader)
4. EnemyManager						(needs LevelLoader)
5. Grid								(needs LevelLoader)
6. Pathfinding						(needs Grid)
7. Camera							(needs Player and LevelLoader)
*/

void ExamScene::Initialize(const GameContext& gameContext)
{
	//Initialize randomness
	unsigned int seed = 0;
#ifdef RANDOM_SEED
	seed = static_cast<unsigned int>(time(nullptr));
	Logger::LogFormat(LogLevel::Info, L"Initializing with seed: %i", seed);
#endif
	srand(seed);
	rand(); rand(); rand();

	//Enable physics debugging
#ifdef _DEBUG
	GetPhysxProxy()->EnablePhysxDebugRendering(true);
#else
	DebugRenderer::ToggleDebugRenderer();
#endif

	//Load all the used materials
	LoadMaterials(gameContext);

	//Set the resolution of the shadow map (Optional)
	gameContext.pShadowMapper->SetShadowmapResolution(gameContext, 1920, 1080);

	//INITIALIZE OBJECTS

	//1. LevelLoader
	m_pLevelLoader = new LevelLoader();
	m_pLevelLoader->Initialize(this);

	wstringstream levelPath;
	//Failsafe if menu has issues
	if (m_LevelToLoad == L"")
	{
		Logger::LogWarning(L"ExamScene::Initialize > LevelToLoad is empty, falling back to 'Galdiator'");
		m_LevelToLoad = L"Gladiator";
	}
	levelPath << "./Resources/Levels/" << m_LevelToLoad << ".json";
	m_pLevelLoader->LoadLevel(levelPath.str());

	//2. Player
	InitializePlayer();

	//3. Crate manager
	m_pCrateManager = new CrateManager();
	AddChild(m_pCrateManager);
	m_pCrateManager->SetSpawnpoints(m_pLevelLoader->GetCrateSpawnpoints());
	m_pCrateManager->SpawnAllCrates();

	//4. Enemy manager
	m_pEnemyManager = new EnemyManager();
	AddChild(m_pEnemyManager);
	m_pEnemyManager->Initialize(gameContext);
	m_pEnemyManager->SetSpawnpoints(m_pLevelLoader->GetEnemySpawnpoints());
	m_pEnemyManager->SetTarget(m_pPlayer);

	//5. Grid
	XMFLOAT3 ws = m_pLevelLoader->GetPlayableArea();
	//Offset the grid size so the enemyspawns are inside of the grid
	m_pGrid = new Grid(XMFLOAT3(0.0f, 0.0f, 0.0f), ws.x + 40.0f, ws.z + 40.0f, 10.0f);
	m_pGrid->CreateGrid(this);

	//6. Pathfinding
	Pathfinding::GetInstance()->Initialize(m_pGrid);

	//7. Camera
	InitializeCamera();

	//Sound
	SoundManager* pSoundManager = SoundManager::GetInstance();
	m_pBackgroundMusic = pSoundManager->LoadSound("./Resources/Sounds/BackgroundMusic.mp3", FMOD_2D | FMOD_CREATESTREAM);
	FMOD_RESULT fr = pSoundManager->GetSystem()->playSound(m_pBackgroundMusic, nullptr, false, nullptr);
	m_pBackgroundMusic->setMode(FMOD_LOOP_NORMAL);
	if (fr != FMOD_OK)
		Logger::LogWarning(L"ExamScene::Initialize() > Playing background music failed!");

	pSoundManager->LoadSound("./Resources/Sounds/Enemies/ZombieDeath (1).wav", FMOD_2D);
	pSoundManager->LoadSound("./Resources/Sounds/Enemies/ZombieDeath (2).wav", FMOD_2D);
	pSoundManager->LoadSound("./Resources/Sounds/Enemies/ZombieDeath (3).wav", FMOD_2D);
}

void ExamScene::InitializePlayer()
{
	m_pPlayer = new Player();
	m_pPlayer->GetTransform()->Translate(m_pLevelLoader->GetPlayerStart());
	m_pPlayer->SetBounds(m_pLevelLoader->GetPlayableArea());
	AddChild(m_pPlayer);
}

void ExamScene::InitializeCamera()
{
	m_pCamera = new TopDownCamera();
	m_pCamera->SetTarget(m_pPlayer);
	m_pCamera->SetProperties(150.0f, 4.0f);
	m_pCamera->CameraRotate(50.0f);
	AddChild(m_pCamera);
	m_pCamera->GetComponent<CameraComponent>()->UseOrthographicProjection();
	m_pCamera->GetComponent<CameraComponent>()->SetOrthoSize(105.0f);
	SetActiveCamera(m_pCamera->GetComponent<CameraComponent>());

	//Set the bounds of the camera
	XMFLOAT3 ws = m_pLevelLoader->GetPlayableArea();
	RECT bounds;
	bounds.left = static_cast<LONG>(-ws.x / 2.0f);
	bounds.top = static_cast<LONG>(ws.z / 2.0f);
	bounds.right = bounds.left * -1;
	bounds.bottom = bounds.top * -1;
	m_pCamera->SetWorldBounds(bounds);
}

void ExamScene::LoadMaterials(const GameContext& gameContext)
{
	m_InitLightPos = { -80,60,-40 };
	gameContext.pShadowMapper->SetLight(m_InitLightPos, { 0.740129888f, -0.597205281f, 0.309117377f });

	SelfIlluminatedMaterial* pSelfIllumMaterial;
	SkinnedDiffuseMaterial_Shadow* pSkinnedShadowMaterial;
	DiffuseMaterial_Shadow* pShadowMaterial;

	pSkinnedShadowMaterial = new SkinnedDiffuseMaterial_Shadow();
	pSkinnedShadowMaterial->SetDiffuseTexture(L"./Resources/Textures/Game/Rambo.dds");
	pSkinnedShadowMaterial->SetLightDirection(gameContext.pShadowMapper->GetLightDirection());
	gameContext.pMaterialManager->AddMaterial(pSkinnedShadowMaterial, MaterialIndex::mPlayer);

	pSkinnedShadowMaterial = new SkinnedDiffuseMaterial_Shadow();
	pSkinnedShadowMaterial->SetDiffuseTexture(L"./Resources/Textures/Game/Zombie.dds");
	pSkinnedShadowMaterial->SetLightDirection(gameContext.pShadowMapper->GetLightDirection());
	gameContext.pMaterialManager->AddMaterial(pSkinnedShadowMaterial, MaterialIndex::mZombie);

	pSkinnedShadowMaterial = new SkinnedDiffuseMaterial_Shadow();
	pSkinnedShadowMaterial->SetDiffuseTexture(L"./Resources/Textures/Game/Devil.dds");
	pSkinnedShadowMaterial->SetLightDirection(gameContext.pShadowMapper->GetLightDirection());
	gameContext.pMaterialManager->AddMaterial(pSkinnedShadowMaterial, MaterialIndex::mDevil);

	pShadowMaterial = new DiffuseMaterial_Shadow();
	pShadowMaterial->SetDiffuseTexture(L"./Resources/Textures/Game/Swatch.png");
	pShadowMaterial->SetLightDirection(gameContext.pShadowMapper->GetLightDirection());
	gameContext.pMaterialManager->AddMaterial(pShadowMaterial, MaterialIndex::mSwatch);

	pShadowMaterial = new DiffuseMaterial_Shadow();
	pShadowMaterial->SetDiffuseTexture(L"./Resources/Textures/Game/Ground.png");
	pShadowMaterial->SetLightDirection(gameContext.pShadowMapper->GetLightDirection());
	gameContext.pMaterialManager->AddMaterial(pShadowMaterial, MaterialIndex::mGround);

	pSelfIllumMaterial = new SelfIlluminatedMaterial();
	pSelfIllumMaterial->SetDiffuseTexture(L"./Resources/Textures/Game/MuzzleFlash.png");
	gameContext.pMaterialManager->AddMaterial(pSelfIllumMaterial, MaterialIndex::mMuzzleFlash);

	pShadowMaterial = new DiffuseMaterial_Shadow();
	pShadowMaterial->SetDiffuseTexture(L"./Resources/Textures/Game/Bloodspat.png");
	gameContext.pMaterialManager->AddMaterial(pShadowMaterial, MaterialIndex::mBloodSpat);

	pSelfIllumMaterial = new SelfIlluminatedMaterial();
	pSelfIllumMaterial->SetDiffuseTexture(L"./Resources/Textures/Game/Explosion.png");
	gameContext.pMaterialManager->AddMaterial(pSelfIllumMaterial, MaterialIndex::mExplosion);

#ifdef EDGE_DETECTION
	//Post Processing
	PostEdgeDetection* pPostEdgeDetection = new PostEdgeDetection();
	pPostEdgeDetection->SetThreshold(0.00001f);
	gameContext.pMaterialManager->AddMaterial_PP(pPostEdgeDetection, MaterialIndex_PP::ppEdgeDetection);
	AddPostProcessingMaterial(MaterialIndex_PP::ppEdgeDetection);
#endif
	PostVignette* pPostVignette = new PostVignette();
	pPostVignette->SetColor((XMFLOAT4)Colors::Red);
	pPostVignette->SetRadius(0.8f);
	pPostVignette->SetSmoothness(0.5f);
	gameContext.pMaterialManager->AddMaterial_PP(pPostVignette, MaterialIndex_PP::ppVignette);
	AddPostProcessingMaterial(MaterialIndex_PP::ppVignette);

	PostVignette* pFlash = new PostVignette();
	pFlash->SetColor((XMFLOAT4)Colors::White);
	pFlash->SetRadius(0.0f);
	pFlash->SetSmoothness(0.0f);
	gameContext.pMaterialManager->AddMaterial_PP(pFlash, MaterialIndex_PP::ppFlash);
	AddPostProcessingMaterial(MaterialIndex_PP::ppFlash);

	PostGrayscale* pPostGrayScale = new PostGrayscale();
	gameContext.pMaterialManager->AddMaterial_PP(pPostGrayScale, MaterialIndex_PP::ppGrayscale);
}

void ExamScene::Update(const GameContext& gameContext)
{
#ifdef DEBUG_AI
	m_pGrid->Debug();
#endif

	m_PlayTime += gameContext.pGameTime->GetElapsed();

	PostVignette* pFlash = static_cast<PostVignette*>(gameContext.pMaterialManager->GetMaterial_PP(MaterialIndex_PP::ppFlash));
	float smoothness = pFlash->GetSmoothness() - gameContext.pGameTime->GetElapsed();
	Clamp(smoothness, 1.0f, 0.0f);
	pFlash->SetSmoothness(smoothness);

	//Update the light's position
	XMFLOAT3 lp = m_pCamera->GetTransform()->GetWorldPosition();
	lp.x += m_InitLightPos.x - 15.0f;
	lp.z += m_InitLightPos.z + m_pCamera->GetZOffset();
	lp.y = m_InitLightPos.y;
	gameContext.pShadowMapper->SetLight(lp);

	//If player dies call death scene
	if (m_pPlayer->IsDead())
	{
		if(m_DeathSequenceCompleted == false)
		{
			AddPostProcessingMaterial(MaterialIndex_PP::ppGrayscale);
			m_DeathSequenceCompleted = true;
		}
		PostGrayscale* pGrayScale = static_cast<PostGrayscale*>(gameContext.pMaterialManager->GetMaterial_PP(MaterialIndex_PP::ppGrayscale));
		float strength = MathHelp::InverseLerp(0.0f, 2.0f, m_DeathScreenDelayTimer);
		Clamp<float>(strength, 1.0f, 0.0f);
		pGrayScale->SetStrength(strength);
		m_DeathScreenDelayTimer += gameContext.pGameTime->GetElapsed();
		if (m_DeathScreenDelayTimer >= 4.0f && m_RegisterHighscore)
		{
#ifdef ONLINE_HIGHSCORES
				OnlineLeaderboard leaderboards;
				leaderboards.UploadScore(m_pPlayer->GetScore(), m_PlayTime);
#else
				OfflineLeaderboard leaderboards;
				if (leaderboards.RegisterHighscore(m_Score, m_PlayTime))
					Logger::LogInfo(L"Player::Die() > Highscore registered");
				else
					Logger::LogWarning(L"Player::Die() > Highscore registration failed!");
#endif	

				DeathScene* pDeathScene = new DeathScene();
				pDeathScene->SetScore(m_pPlayer->GetScore());
				SceneManager::GetInstance()->AddGameScene(pDeathScene);
				SceneManager::GetInstance()->SetActiveGameScene(L"DeathScene");
		}
	}
}

void ExamScene::Draw(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}