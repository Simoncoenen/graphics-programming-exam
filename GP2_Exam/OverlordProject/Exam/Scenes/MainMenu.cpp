//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"
#pragma region
#include "MainMenu.h"
#include "Scenegraph\GameObject.h"

#include <Scenegraph/SceneManager.h>
#include <Base/OverlordGame.h>
#include <Prefabs/FixedCamera.h>
#include <Content/ContentManager.h>

#include "ExamScene.h"

//UI
#include <Graphics/TextRenderer.h>
#include <Exam/UI/Button.h>
#include <Exam/UI/MenuSelector.h>
#include <Graphics/TextureData.h>
#include <Exam/UI/ButtonManager.h>
#include <Graphics/SpriteRenderer.h>

#define ONLINE_HIGHSCORES
#include <Exam/Managers/Leaderboards/HighscoreEntry.h>
#include <iomanip>
#include <Base/SoundManager.h>
#ifdef ONLINE_HIGHSCORES
#include <Exam/Managers/Leaderboards/OnlineLeaderboard.h>
#else
#include <Exam/Managers/Leaderboards/OfflineLeaderboard.h>
#endif

#pragma endregion Includes

MainMenu::MainMenu(void):
	GameScene(L"MainMenu")
{
}

MainMenu::~MainMenu(void)
{
	SafeDelete(m_pMainButtonManager);
	SafeDelete(m_pLeaderboardsButtonManager);
	SafeDelete(m_pLevelSelector);
}

void MainMenu::Initialize(const GameContext& gameContext)
{
	m_WindowSettings = &OverlordGame::GetGameSettings().Window;
	m_ScreenMiddle = XMFLOAT2(m_WindowSettings->Width / 2.0f, m_WindowSettings->Height / 2.0f);

	//Camera
	FixedCamera* pCamera = new FixedCamera();
	AddChild(pCamera);
	SetActiveCamera(pCamera->GetComponent<CameraComponent>());

	//Input
	InitializeInput(gameContext);

	//Sounds
	m_pBackgroundMusic = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/MenuLoop.mp3", FMOD_2D | FMOD_CREATESTREAM);
	SoundManager::GetInstance()->GetSystem()->playSound(m_pBackgroundMusic, nullptr, false, nullptr);
	//m_pBackgroundChannel->setMode(FMOD_LOOP_NORMAL);
	m_pBackgroundMusic->setMode(FMOD_LOOP_NORMAL);

	m_pButtonClickSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/ButtonClick.wav", FMOD_2D);
	m_pButtonHoverSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/ButtonHover.wav", FMOD_2D);

	//Fonts
	m_pSmallFont = ContentManager::Load<SpriteFont>(L"./Resources/Fonts/Gills_50.fnt");
	m_pTinyFont = ContentManager::Load<SpriteFont>(L"./Resources/Fonts/Eras_30.fnt");

	m_pMenuBackground = ContentManager::Load<TextureData>(L"./Resources/Textures/Game/UI/StartScreen.png");

	//Buttons
	CreateButtons();

	//Leaderboards
	#ifdef ONLINE_HIGHSCORES
	OnlineLeaderboard leaderboard;
	#else
	OfflineLeaderboard leaderboard;
	#endif
	m_HighscoreEntries = leaderboard.GetEntries(8);
}

void MainMenu::Update(const GameContext& gameContext)
{
	switch (m_CurrentWindow)
	{
	case CurrentWindow::MAIN:
		m_pMainButtonManager->Update(gameContext);
		m_pLevelSelector->Update(gameContext);

		//Keyboard/Controller input
		if (gameContext.pInput->IsActionTriggered(START))
			StartGame();
		else if (gameContext.pInput->IsActionTriggered(EXIT))
			PostQuitMessage(0);
		else if (gameContext.pInput->IsActionTriggered(LEFT))
			m_pLevelSelector->Previous();
		else if (gameContext.pInput->IsActionTriggered(RIGHT))
			m_pLevelSelector->Next();
		break;
	case CurrentWindow::HIGHSCORES:
		m_pLeaderboardsButtonManager->Update(gameContext);
		break;
	default:
		return;
	}
}

void MainMenu::Draw(const GameContext& gameContext)
{
	//Scale background image with window size
	XMFLOAT2 dim = m_pMenuBackground->GetDimension();
	XMFLOAT2 scale(m_WindowSettings->Width / dim.x, m_WindowSettings->Height / dim.y);
	SpriteRenderer::GetInstance()->Draw(m_pMenuBackground, XMFLOAT2(0.0f, 0.0f), (XMFLOAT4)Colors::White, XMFLOAT2(0.0f, 0.0f), scale);

	switch (m_CurrentWindow)
	{
	case CurrentWindow::MAIN:
	{
		//Draw text on buttons
		TextRenderer::GetInstance()->DrawText(m_pSmallFont, L"Play", XMFLOAT2(m_ScreenMiddle.x - 50.0f, m_ScreenMiddle.y - 25.0f), XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f));
		TextRenderer::GetInstance()->DrawText(m_pSmallFont, L"Highscores", XMFLOAT2(m_ScreenMiddle.x - 120.0f, m_ScreenMiddle.y + 140.0f), XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f));
		TextRenderer::GetInstance()->DrawText(m_pSmallFont, L"Exit", XMFLOAT2(m_ScreenMiddle.x - 50.0f, m_ScreenMiddle.y + 220.0f), XMFLOAT4(0.8f, 0.8f, 0.8f, 1.0f));

		m_pMainButtonManager->Draw();
		m_pLevelSelector->Draw();
		break;
	}
	case CurrentWindow::HIGHSCORES:
	{
		//Draw the buttons
		m_pLeaderboardsButtonManager->Draw();
		TextRenderer::GetInstance()->DrawText(m_pSmallFont, L"<", XMFLOAT2(100.0f, m_ScreenMiddle.y * 2 - 150.0f));
		TextRenderer::GetInstance()->DrawText(m_pSmallFont, L"Refresh", XMFLOAT2(m_ScreenMiddle.x - 90.0f, m_ScreenMiddle.y * 2 - 70.0f));

		//Draw all the highscores
		float startY = m_ScreenMiddle.y - 50.0f;
		float spacing = 30.0f;

		TextRenderer::GetInstance()->DrawText(m_pTinyFont, L"Name", XMFLOAT2(m_ScreenMiddle.x - 440.0f, startY));
		TextRenderer::GetInstance()->DrawText(m_pTinyFont, L"Score", XMFLOAT2(m_ScreenMiddle.x - 250.0f, startY));
		TextRenderer::GetInstance()->DrawText(m_pTinyFont, L"Date", XMFLOAT2(m_ScreenMiddle.x - 40.0f, startY));
		TextRenderer::GetInstance()->DrawText(m_pTinyFont, L"Time", XMFLOAT2(m_ScreenMiddle.x + 160.0f, startY));
		TextRenderer::GetInstance()->DrawText(m_pTinyFont, L"Playtime", XMFLOAT2(m_ScreenMiddle.x + 360.0f, startY));

		if(m_HighscoreEntries.size() == 0)
			TextRenderer::GetInstance()->DrawText(m_pTinyFont, L"No highscores yet.", XMFLOAT2(m_ScreenMiddle.x - 100.0f, startY + spacing * 2.0f));

		startY += spacing;
		for (size_t i = 0; i < m_HighscoreEntries.size(); i++)
		{
			wstringstream ss;
			ss << i + 1 << ". " << m_HighscoreEntries[i].Name;
			TextRenderer::GetInstance()->DrawText(m_pTinyFont, ss.str(), XMFLOAT2(m_ScreenMiddle.x - 440.0f, startY + i * spacing));

			ss = wstringstream();
			ss << setw(8) << setfill(L'0') << m_HighscoreEntries[i].Score;
			TextRenderer::GetInstance()->DrawText(m_pTinyFont, ss.str(), XMFLOAT2(m_ScreenMiddle.x - 250.0f, startY + i * spacing));

			ss = wstringstream();
			ss << m_HighscoreEntries[i].Date;
			TextRenderer::GetInstance()->DrawText(m_pTinyFont, ss.str(), XMFLOAT2(m_ScreenMiddle.x - 40.0f, startY + i * spacing));

			ss = wstringstream();
			ss << m_HighscoreEntries[i].Time;
			TextRenderer::GetInstance()->DrawText(m_pTinyFont, ss.str(), XMFLOAT2(m_ScreenMiddle.x + 160.0f, startY + i * spacing));

			ss = wstringstream();

			int seconds = m_HighscoreEntries[i].PlayTime % 60;
			int minutes = (m_HighscoreEntries[i].PlayTime / 60) % 60;
			int hours = (m_HighscoreEntries[i].PlayTime) / 60 / 60;
			ss << setw(2) << setfill(L'0') << hours << L":";
			ss << setw(2) << minutes << L":";
			ss << setw(2) << seconds;
			wstring str(ss.str());
			TextRenderer::GetInstance()->DrawText(m_pTinyFont, ss.str(), XMFLOAT2(m_ScreenMiddle.x + 360.0f, startY + i * spacing));
		}
		break;
	}
	default:
		return;
	}
	UNREFERENCED_PARAMETER(gameContext);
}

void MainMenu::CreateButtons()
{
	//Regular buttons
	TextureData* pDefaultButton = ContentManager::Load<TextureData>(L"./Resources/Textures/Game/UI/DefaultButton.png");
	TextureData* pHoverButton = ContentManager::Load<TextureData>(L"./Resources/Textures/Game/UI/HoverButton.png");

	m_pMainButtonManager = new ButtonManager();
	m_pMainButtonManager->AddButton(new Button(m_ScreenMiddle, [this]() {StartGame(); }, pDefaultButton, pHoverButton, m_pButtonHoverSound, m_pButtonClickSound));
	m_pMainButtonManager->AddButton(new Button(XMFLOAT2(m_ScreenMiddle.x, m_ScreenMiddle.y + 160.0f), [this]() {m_CurrentWindow = CurrentWindow::HIGHSCORES; }, pDefaultButton, pHoverButton, m_pButtonHoverSound, m_pButtonClickSound));
	m_pMainButtonManager->AddButton(new Button(XMFLOAT2(m_ScreenMiddle.x, m_ScreenMiddle.y + 240.0f), [this]() {PostQuitMessage(0); }, pDefaultButton, pHoverButton, m_pButtonHoverSound, m_pButtonClickSound));

	//Selector buttons
	TextureData* pCycleDefault = ContentManager::Load<TextureData>(L"./Resources/Textures/Game/UI/CycleButtonDefault.png");
	TextureData* pCycleHover = ContentManager::Load<TextureData>(L"./Resources/Textures/Game/UI/CycleButtonHover.png");
	
	m_pLevelSelector = new MenuSelector(XMFLOAT2(m_ScreenMiddle.x, m_ScreenMiddle.y + 80.0f), pCycleDefault, pCycleHover, pDefaultButton, m_pSmallFont, m_pButtonHoverSound, m_pButtonClickSound);
	
	//Add the levels to the selector
	wifstream levelNames(L"./Resources/Levels/Levels.txt");
	if (levelNames.fail())
		Logger::LogError(L"MainMenu::CreateButtons. './Resources/Levels/Levels.txt' not found!");
	wstring levelName;
	while(getline(levelNames, levelName))
		m_pLevelSelector->AddData(levelName);
	levelNames.close();

	//HIGHSCORES SCREEN

	//Highscore back button
	m_pLeaderboardsButtonManager = new ButtonManager();
	m_pLeaderboardsButtonManager->AddButton(new Button(XMFLOAT2(110.0f, m_ScreenMiddle.y * 2 - 120.0f), [this]() {m_CurrentWindow = CurrentWindow::MAIN; }, pCycleDefault, pCycleHover, m_pButtonHoverSound, m_pButtonClickSound));
	m_pLeaderboardsButtonManager->AddButton(new Button(XMFLOAT2(m_ScreenMiddle.x, m_ScreenMiddle.y * 2 - 50.0f), [this]()
	{
#ifdef ONLINE_HIGHSCORES
		OnlineLeaderboard lb; m_HighscoreEntries = lb.GetEntries(8);
#endif
	}, pDefaultButton, pHoverButton, m_pButtonHoverSound, m_pButtonClickSound));
}

void MainMenu::InitializeInput(const GameContext& gameContext)
{
	gameContext.pInput->AddInputAction(InputAction(InputKey::LEFT, Pressed, VK_LEFT, -1, XINPUT_GAMEPAD_X));
	gameContext.pInput->AddInputAction(InputAction(InputKey::RIGHT, Pressed, VK_RIGHT, -1, XINPUT_GAMEPAD_B));
	gameContext.pInput->AddInputAction(InputAction(InputKey::EXIT, Pressed, VK_ESCAPE, -1, XINPUT_GAMEPAD_Y));
	gameContext.pInput->AddInputAction(InputAction(InputKey::START, Pressed, VK_RETURN, -1, XINPUT_GAMEPAD_A));
}

void MainMenu::StartGame()
{
	SoundManager::GetInstance()->GetSystem()->playSound(m_pBackgroundMusic, nullptr, true, nullptr);
	ExamScene* pScene = new ExamScene();
	//Load the selected level
	pScene->SetLevel(m_pLevelSelector->GetSelected());
	SceneManager::GetInstance()->AddGameScene(pScene);
	SceneManager::GetInstance()->SetActiveGameScene(L"ExamScene");
}
