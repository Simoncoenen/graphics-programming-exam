//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"
#pragma region
#include "DeathScene.h"
#include "Scenegraph\GameObject.h"

#include <Base/OverlordGame.h>
#include <Prefabs/FixedCamera.h>
#include <Content/ContentManager.h>

#include "ExamScene.h"

//UI
#include <Graphics/TextRenderer.h>
#include <Exam/UI/Button.h>
#include <Graphics/TextureData.h>
#include <Graphics/SpriteRenderer.h>
#include <iomanip>
#include <Base/SoundManager.h>

#pragma endregion Includes

DeathScene::DeathScene(void):
	GameScene(L"DeathScene")
{
}

DeathScene::~DeathScene(void)
{
	SafeDelete(m_pBackButton);
}

void DeathScene::SetScore(const int score)
{
	m_Score = score;
}

void DeathScene::Initialize(const GameContext& gameContext)
{
	m_WindowSettings = &OverlordGame::GetGameSettings().Window;
	m_ScreenMiddle = XMFLOAT2(m_WindowSettings->Width / 2.0f, m_WindowSettings->Height / 2.0f);

	//Camera
	FixedCamera* pCamera = new FixedCamera();
	AddChild(pCamera);
	SetActiveCamera(pCamera->GetComponent<CameraComponent>());

	//Input
	InitializeInput(gameContext);

	CreateButtons();

	//Fonts
	m_pSmallFont = ContentManager::Load<SpriteFont>(L"./Resources/Fonts/Gills_50.fnt");
	m_pBigFont = ContentManager::Load<SpriteFont>(L"./Resources/Fonts/Gills_120.fnt");
	m_pTinyFont = ContentManager::Load<SpriteFont>(L"./Resources/Fonts/Eras_30.fnt");

	m_pMenuBackground = ContentManager::Load<TextureData>(L"./Resources/Textures/Game/UI/StartScreen.png");

	//Sounds
	m_pDefeatSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/Defeat.mp3", FMOD_2D);
	SoundManager::GetInstance()->GetSystem()->playSound(m_pDefeatSound, nullptr, false, nullptr);
}

void DeathScene::Update(const GameContext& gameContext)
{
	m_pBackButton->Update(gameContext);
}

void DeathScene::Draw(const GameContext& gameContext)
{
	//Scale background image with window size
	XMFLOAT2 dim = m_pMenuBackground->GetDimension();
	XMFLOAT2 scale(m_WindowSettings->Width / dim.x, m_WindowSettings->Height / dim.y);
	SpriteRenderer::GetInstance()->Draw(m_pMenuBackground, XMFLOAT2(0.0f, 0.0f), (XMFLOAT4)Colors::White, XMFLOAT2(0.0f, 0.0f), scale);

	m_pBackButton->Draw();
	TextRenderer::GetInstance()->DrawText(m_pSmallFont, L"Exit Game", XMFLOAT2(m_ScreenMiddle.x - 120.0f, m_ScreenMiddle.y * 2 - 125.0f));

	TextRenderer::GetInstance()->DrawText(m_pSmallFont, L"Player died.", XMFLOAT2(m_ScreenMiddle.x - 140.0f, m_ScreenMiddle.y - 50.0f));
	TextRenderer::GetInstance()->DrawText(m_pSmallFont, L"Score:", XMFLOAT2(m_ScreenMiddle.x - 70.0f, m_ScreenMiddle.y));
	wstringstream ss;
	ss << setw(8) << setfill(L'0') << m_Score;
	TextRenderer::GetInstance()->DrawText(m_pBigFont, ss.str(), XMFLOAT2(m_ScreenMiddle.x - ss.str().length() * 35.0f, m_ScreenMiddle.y + 70.0f));


	UNREFERENCED_PARAMETER(gameContext);
}

void DeathScene::CreateButtons()
{
	FMOD::Sound* pHoverSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/ButtonHover.wav", FMOD_2D);
	FMOD::Sound* pClickSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/ButtonClick.wav", FMOD_2D);

	//Regular buttons
	TextureData* pDefaultButton = ContentManager::Load<TextureData>(L"./Resources/Textures/Game/UI/DefaultButton.png");
	TextureData* pHoverButton = ContentManager::Load<TextureData>(L"./Resources/Textures/Game/UI/HoverButton.png");

	m_pBackButton = new Button(XMFLOAT2(m_ScreenMiddle.x, m_ScreenMiddle.y * 2 - 100.0f), []() {PostQuitMessage(0);}, pDefaultButton, pHoverButton, pHoverSound, pClickSound);
}

void DeathScene::InitializeInput(const GameContext& gameContext)
{
	gameContext.pInput->AddInputAction(InputAction(InputKey::RETURN, Pressed, VK_LEFT, -1, XINPUT_GAMEPAD_A));
}