#include "stdafx.h"
#include "LevelLoader.h"
#include <Scenegraph/GameScene.h>
#include <Scenegraph/GameObject.h>
#include <Components/Components.h>
#include <Content/ContentManager.h>
#include <Physx/PhysxManager.h>

#include <Exam/Helpers/Enumerations.h>
#include <Exam/Prefabs/Objects/FakeWall.h>
#include <Exam/Prefabs/Objects/Barrel.h>
#include <Exam/Prefabs/Objects/ClothObject.h>
#include <Graphics/MeshFilter.h>

#define CLOTH

using json = nlohmann::json;

LevelLoader::LevelLoader():
	m_pGameScene(nullptr),
	m_pDefaultMaterial(nullptr)
{}

LevelLoader::~LevelLoader()
{
	SafeDelete(m_pMasterMeshFilter);
}

void LevelLoader::Initialize(GameScene* pGameScene)
{
	PxPhysics* pPhysx = PhysxManager::GetInstance()->GetPhysics();
	m_pDefaultMaterial = pPhysx->createMaterial(0.5f, 0.5f, 0.1f);
	m_pGameScene = pGameScene;
}

void LevelLoader::LoadLevel(const std::wstring& filePath)
{
	Timer timer;
	timer.Start(L"LevelLoader::LoadLevel > Loading level");

	ifstream file;
	file.open(filePath);
	if (file.fail())
		Logger::LogFormat(LogLevel::Error, L"LevelLoader: Could not open file '%s'. Exiting game.", filePath.c_str());
	Logger::LogFormat(LogLevel::Info, L"LevelLoader: file '%s' opened.", filePath.c_str());

	json levelData = json::parse(file);

	SetCrateSpawns(levelData);
	SetEnemySpawns(levelData);
	CreateGround(levelData);
	CreateObjects(levelData);
	SetPlayerStart(levelData);

	m_pMasterObject = new GameObject();
	m_pMasterObject->AddComponent(new RigidBodyComponent(true));
	m_pMasterMeshFilter = new MeshFilter();
	m_pMasterMeshFilter->m_HasElement = ILSemantic::POSITION | ILSemantic::NORMAL | ILSemantic::TEXCOORD;
	CreateStructures(levelData);
	ModelComponent* pModel = new ModelComponent(m_pMasterMeshFilter);
	pModel->SetMaterial(MaterialIndex::mSwatch);

	m_pMasterObject->AddComponent(pModel);
	m_pGameScene->AddChild(m_pMasterObject);

	timer.Stop(L"LevelLoader::LoadLevel > Loading level");
}

void LevelLoader::CreateStructures(nlohmann::json& levelData)
{
	Logger::LogInfo(L"LevelLoader::CreateStructures > Loading structures:");
	int savedByBatching = 0;
	for (auto structure : levelData["structures"])
	{
		string path = structure["filepath"].get<string>();
		wstring unicodePath = wstring(path.begin(), path.end());
		Logger::LogFormat(LogLevel::Info, L"LevelLoader::CreateStructures > Structures with filepath: '%s'...", unicodePath.c_str());
		for (auto positions : structure["positions"])
		{
			std::vector<float> position = positions.get<std::vector<float>>();

			//Add the mesh filter
			MeshFilter* mesh = ContentManager::Load<MeshFilter>(unicodePath);
			XMFLOAT3 offset(position[0], position[1], position[2]);
			m_pMasterMeshFilter->AddMeshFilter(mesh, offset);

			//Collider
			wstringstream ss;
			ss << unicodePath.substr(0, unicodePath.find(L".ovm")) << L".ovpc";
			PxConvexMesh* pConvexMesh = ContentManager::Load<PxConvexMesh>(ss.str());
			shared_ptr<PxGeometry> geom(new PxConvexMeshGeometry(pConvexMesh));
			ColliderComponent* pCollider = new ColliderComponent(geom, *m_pDefaultMaterial, PxTransform(ToPxVec3(offset)));
			m_pMasterObject->AddComponent(pCollider);

			++savedByBatching;
		}
	}
	Logger::LogFormat(LogLevel::Info, L"LevelLoader::CreateStructures > Complete. Saved by batching: %i", savedByBatching);
}

void LevelLoader::CreateGround(nlohmann::json& levelData)
{
	Logger::LogInfo(L"LevelLoader::CreateGround > Creating ground...");
	json block = levelData["ground"];
	string assetPath = block["filepath"].get<string>();
	vector<float> position = block["position"].get <vector<float>>();
	vector<float> playableArea = block["playablearea"].get<vector<float>>();
	m_PlayableArea = XMFLOAT3(playableArea[0], playableArea[1], playableArea[2]);
	GameObject* pGround = new GameObject();
	ModelComponent* pModel = new ModelComponent(wstring(assetPath.begin(), assetPath.end()));
	pModel->SetMaterial(MaterialIndex::mGround);
	pGround->AddComponent(pModel);
	pGround->AddComponent(new RigidBodyComponent(true));
	std::shared_ptr<PxGeometry> groundGeometry(new PxPlaneGeometry());
	pGround->AddComponent(new ColliderComponent(groundGeometry, *PhysxManager::GetInstance()->GetDefaultMaterial(), PxTransform(PxQuat(PxPiDivTwo, PxVec3(0.0f, 0.0f, 1.0f)))));
	pGround->GetTransform()->Scale(1.5f, 1.0f, 1.5f);
	pGround->GetTransform()->Translate(static_cast<XMFLOAT3>(position.data()));
	m_pGameScene->AddChild(pGround);
}

void LevelLoader::CreateObjects(nlohmann::json& levelData) const
{
	Logger::LogInfo(L"LevelLoader::CreateObjects > Creating objects...");

	if (levelData["fakewallspawns"].is_null())
		Logger::LogInfo(L"LevelLoader::CreateObjects > No fake wall data found!");
	else
	{
		Logger::LogInfo(L"LevelLoader::CreateObjects > Creating/spawning fake walls...");
		for (auto spawns : levelData["fakewallspawns"])
		{
	
			std::vector<float> position = spawns.get<std::vector<float>>();
			XMFLOAT3 pos(position[0], position[1], position[2]);
			FakeWall* pFakeWall = new FakeWall();
			pFakeWall->GetTransform()->Translate(pos);
			m_pGameScene->AddChild(pFakeWall);
		}
	}

	if(levelData["barrelspawns"].is_null())
		Logger::LogInfo(L"LevelLoader::CreateObjects > No barrel data found!");
	else
	{
		Logger::LogInfo(L"LevelLoader::CreateObjects > Creating/spawning barrels...");
		for (auto spawns : levelData["barrelspawns"])
		{
			std::vector<float> position = spawns.get<std::vector<float>>();
			XMFLOAT3 pos(position[0], position[1], position[2]);
			Barrel* pBarrel = new Barrel();
			pBarrel->GetTransform()->Translate(pos);
			m_pGameScene->AddChild(pBarrel);
		}
	}

#ifdef CLOTH
	if (levelData["cloth"].is_null())
		Logger::LogInfo(L"LevelLoader::CreateObjects > No cloth objects found!");
	else
	{
		Logger::LogInfo(L"LevelLoader::CreateObjects > Creating cloth objects...");
		for (auto cloth : levelData["cloth"])
		{
			std::vector<float> position = cloth["position"].get<std::vector<float>>();
			float width = cloth["size"][0].get<float>();
			float height = cloth["size"][1].get<float>();

			std::vector<float> planeNormal = cloth["planeNormal"].get<std::vector<float>>();
			std::vector<float> force = cloth["force"].get<std::vector<float>>();

			XMFLOAT3 pos(position[0], position[1], position[2]);
			ClothObject* pCloth = new ClothObject(width, height, 1.0f);
			pCloth->GetTransform()->Translate(pos);
			m_pGameScene->AddChild(pCloth);
			pCloth->SetExternalAcceleration(PxVec3(force[0], force[1], force[2]));
			pCloth->GetCloth()->addCollisionPlane(PxClothCollisionPlane(PxVec3(planeNormal[0], planeNormal[1], planeNormal[2]), planeNormal[3]));
			pCloth->GetCloth()->addCollisionConvex(1 << 0);

			pCloth->SetTexture(ContentManager::Load<TextureData>(L"./Resources/Textures/Game/Drape.png"));
		}
	}
#endif
}

void LevelLoader::SetCrateSpawns(nlohmann::json& levelData)
{
	if (levelData["cratespawns"].is_null())
	{
		Logger::LogInfo(L"LevelLoader::SetCrateSpawns > No crate spawn data found!...");
		return;
	}
	Logger::LogInfo(L"LevelLoader::SetCrateSpawns > Setting crate spawns...");

	for(auto spawns : levelData["cratespawns"])
	{
		std::vector<float> position = spawns.get<std::vector<float>>();
		m_CrateSpawnpoints.push_back(XMFLOAT3(position[0], position[1], position[2]));
	}
}

void LevelLoader::SetEnemySpawns(nlohmann::json& levelData)
{
	if(levelData["enemyspawns"].is_null())
	{
		Logger::LogInfo(L"LevelLoader::SetEnemySpawns > No enemy spawn data found!...");
		return;
	}
	Logger::LogInfo(L"LevelLoader::SetEnemySpawns > Setting enemy spawns...");

	for (auto spawns : levelData["enemyspawns"])
	{
		std::vector<float> position = spawns.get<std::vector<float>>();
		m_EnemySpawnpoints.push_back(XMFLOAT3(position[0], position[1], position[2]));
	}
}

void LevelLoader::SetPlayerStart(nlohmann::json& levelData)
{
	if (levelData["playerstart"].is_null())
	{
		Logger::LogWarning(L"LevelLoader::SetPlayerStart > No playerstart data found!");
		return;
	}
	vector<float> playerStart = levelData["playerstart"].get<vector<float>>();
	m_PlayerStart = XMFLOAT3(playerStart[0], playerStart[1], playerStart[2]);
	Logger::LogFormat(LogLevel::Info, L"LevelLoader::SetPlayerStart > Player start is: [%f, %f, %f]", m_PlayerStart.x, m_PlayerStart.y, m_PlayerStart.z);
}