#pragma once

class HighscoreEntry;

class OfflineLeaderboard
{
public:
	OfflineLeaderboard();
	~OfflineLeaderboard();

	bool RegisterHighscore(int score, float playTime);
	bool RegisterHighscore(const wstring& name, int score, float playTime);
	vector<HighscoreEntry> GetEntries(int amount = 10);

private:
	HighscoreEntry ParseHighscoreLine(const wstring& line) const;
	wstring FormatName(const wstring& name) const;
};

