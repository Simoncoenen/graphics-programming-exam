#pragma once

class HighscoreEntry;

#pragma region
class OnlineLeaderboard
{
public:
	OnlineLeaderboard();
	~OnlineLeaderboard();

	void UploadScore(int score, float m_PlayTime) const;
	void UploadScore(const string& name, int score, float m_PlayTime) const;
	vector<HighscoreEntry> GetEntries(int amount = 10);
	void ClearScores();

private:
	string HttpGet(string host, string path) const;
	HighscoreEntry FormatToEntry(const wstring& line) const;
	wstring FormatDate(const wstring& line) const;
	wstring FormatTime(const wstring& line, const wstring &meridiem = L"AM") const;
	string FormatName(const string& name) const;

	//Constants
	static const int BUFFER_SIZE = 512;
	static const string HOST;
	static const string PUBLIC_LINK;
	static const string PRIVATE_LINK;
};
#pragma endregion OnlineLeaderboard

#pragma region
class SocketError : public runtime_error
{
public:
	SocketError(const wstring& context, const int errorCode) :
		runtime_error("")
	{
		m_Message = ErrorMessage(context, errorCode);
	}

	const wstring& GetMessageW() const { return m_Message; }

private:
	std::wstring ErrorMessage(const wstring& context, const int errorCode) const;
	wstring m_Message;
};
#pragma endregion SocketError