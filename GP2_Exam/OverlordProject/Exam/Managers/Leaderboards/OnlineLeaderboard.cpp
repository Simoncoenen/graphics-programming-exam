#include "stdafx.h"

#include "OnlineLeaderboard.h"
#include "HighscoreEntry.h"

#include <winsock2.h>
#include <ws2tcpip.h>
// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")

#include <array>
#include <iomanip>

const string OnlineLeaderboard::HOST = "dreamlo.com";
//http://dreamlo.com/lb/5740a9576e51b6162838cd1b/pipe
const string OnlineLeaderboard::PUBLIC_LINK = "5740a9576e51b6162838cd1b";
//http://dreamlo.com/lb/JK0-REy270mBnp78TztrAArYhElbjkUUqXhdcHPnPrEw
const string OnlineLeaderboard::PRIVATE_LINK = "JK0-REy270mBnp78TztrAArYhElbjkUUqXhdcHPnPrEw";

OnlineLeaderboard::OnlineLeaderboard()
{}

OnlineLeaderboard::~OnlineLeaderboard()
{}

void OnlineLeaderboard::UploadScore(int score, float m_PlayTime) const
{
	//Get the username of the current user
	wchar_t username[50 + 1];
	DWORD username_len = 50 + 1;
	GetUserName(username, &username_len);
	string name = string(username, username + username_len - 1);
	UploadScore(name, score, m_PlayTime);
}

void OnlineLeaderboard::UploadScore(const string& name, int score, float m_PlayTime) const
{
	//Format the name
	string newName = FormatName(name);

	Logger::LogInfo(L"OnlineLeaderboard::UploadScore > Adding new score...");
	Timer timer;
	timer.Start(L"OnlineLeaderboard::UploadScore()");

	//Make path
	stringstream path;
	path << "/lb/" << PRIVATE_LINK << "/add/" << newName << "/" << score << "/" << (int)trunc(m_PlayTime);

	//Make a request to the server
	try
	{
		string response = HttpGet(HOST, path.str());
	}
	catch (SocketError &error)
	{
		Logger::LogWarning(error.GetMessageW());
	}

	timer.Stop(L"OnlineLeaderboard::UploadScore()");
	Logger::LogInfo(L"OnlineLeaderboard::UploadScore() > Successful");
}

vector<HighscoreEntry> OnlineLeaderboard::GetEntries(int amount)
{
	Logger::LogInfo(L"OnlineLeaderboard::GetEntries > Getting scores...");
	Timer timer;
	timer.Start(L"OnlineLeaderboard::GetEntries()");

	//Make path
	stringstream path;
	path << "/lb/" << PUBLIC_LINK << "/pipe/" << amount;

	vector<HighscoreEntry> entries;

	//Make a request to the server
	string response;
	try
	{
		response = HttpGet(HOST, path.str());
	}
	catch (SocketError &error)
	{
		Logger::LogWarning(error.GetMessageW());
		return entries;
	}

	//Get all the lines with entries
	stringstream ss(response);
	string line;

	bool getEntry = false;
	while (getline(ss, line))
	{
		if (getEntry == false && line == "\r")
		{
			getEntry = true;
			continue;
		}
		if (getEntry)
		{
			HighscoreEntry entry = FormatToEntry(wstring(line.begin(), line.end()));
			entries.push_back(entry);
		}
	}
	timer.Stop(L"OnlineLeaderboard::GetEntries()");
	Logger::LogInfo(L"OnlineLeaderboard::GetEntries() > Successful");
	return entries;
}

void OnlineLeaderboard::ClearScores()
{
	Timer timer;
	timer.Start(L"OnlineLeaderboard::ClearScores()");

	//Make path
	stringstream path;
	path << "/lb/" << PRIVATE_LINK << "/clear";

	//Make request to the server
	try
	{
		string response = HttpGet(HOST, path.str());
	}
	catch (SocketError &error)
	{
		Logger::LogWarning(error.GetMessageW());
	}
	timer.Stop(L"OnlineLeaderboard::ClearScores()");
	Logger::LogInfo(L"OnlineLeaderboard::ClearScores() > Successful");
}

string OnlineLeaderboard::HttpGet(string host, string path) const
{
	WSADATA wsaData;
	int result;

	// Initialize Winsock
	result = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (result != 0)
		throw SocketError(L"WSAStartUp", result);

	// Resolve the server address and port
	addrinfo * pAddrInfo;
	result = getaddrinfo(host.c_str(), "80", 0, &pAddrInfo);
	if (result != 0)
		throw SocketError(L"addrinfo", result);

	//Create the socket
	SOCKET sock = socket(pAddrInfo->ai_family, pAddrInfo->ai_socktype, pAddrInfo->ai_protocol);
	if (sock == INVALID_SOCKET)
		throw SocketError(L"Socket", WSAGetLastError());

	// Connect to server.
	result = connect(sock, pAddrInfo->ai_addr, pAddrInfo->ai_addrlen);
	if (result != 0)
		throw SocketError(L"Connect", WSAGetLastError());

	const string request = "GET " + path + " HTTP/1.1\nHost: " + host + "\n\n";

	// Send an initial buffer
	result = send(sock, request.c_str(), request.size(), 0);
	if (result == SOCKET_ERROR)
		throw SocketError(L"Send", WSAGetLastError());

	// shutdown the connection since no more data will be sent
	result = shutdown(sock, SD_SEND);
	if (result == SOCKET_ERROR)
		throw SocketError(L"Close send connection", WSAGetLastError());

	// Receive until the peer closes the connection
	string response;

	char buffer[BUFFER_SIZE];
	int bytesRecv = 0;

	for (;;)
	{
		result = recv(sock, buffer, sizeof(buffer), 0);
		if (result == SOCKET_ERROR)
			throw SocketError(L"Recv", WSAGetLastError());
		if (result == 0)
			break;
		response += string(buffer, result);
		Logger::LogFormat(LogLevel::Info, L"OnlineLeaderboard::HttpGet() > Bytes received: %i", result);
		bytesRecv += result;
	}
	Logger::LogFormat(LogLevel::Info, L"OnlineLeaderboard::HttpGet() > Recv Completed. Total bytes received: %i", bytesRecv);

	// cleanup
	result = closesocket(sock);
	if (result == SOCKET_ERROR)
		throw SocketError(L"Closesocket", WSAGetLastError());

	result = WSACleanup();
	if (result == SOCKET_ERROR)
		throw SocketError(L"WSACleanup", WSAGetLastError());

	freeaddrinfo(pAddrInfo);

	Logger::LogFormat(LogLevel::Info, L"OnlineLeaderboard::HttpGet() > Cleanup successful", bytesRecv);

	return response;
}

HighscoreEntry OnlineLeaderboard::FormatToEntry(const wstring& line) const
{
	HighscoreEntry entry;

	//Put line in a string stream so we can split the line up per '|' character (pipe delimited)
	wstringstream entryStream(wstring(line.begin(), line.end()));
	wstring entryValue;
	vector<wstring> entryComponents;
	while (getline(entryStream, entryValue, L'|'))
		entryComponents.push_back(entryValue);

	//The name is the first value
	entry.Name = entryComponents[0];
	//Score is the second value
	entry.Score = stoi(entryComponents[1]);

	entry.PlayTime = static_cast<int>(stof(entryComponents[2]));

	//Date & Time (Date and time are in the fourth value together)
	wstringstream timeAndDate(entryComponents[4]);
	wstring part;
	vector<wstring> timeAndDateValues;
	while (getline(timeAndDate, part, L' '))
		timeAndDateValues.push_back(part);

	//Date
	entry.Date = FormatDate(timeAndDateValues[0]);
	//Time
	entry.Time = FormatTime(timeAndDateValues[1], timeAndDateValues[2]);
	return entry;
}

wstring OnlineLeaderboard::FormatDate(const wstring& line) const
{
	wstringstream stream(line);
	wstring value;
	vector<int> dateData;
	while (getline(stream, value, L'/'))
		dateData.push_back(stoi(value));
	stream = wstringstream();
	stream 
		<< setfill(L'0') << setw(2) << dateData[1] << L"/"
		<< setw(2) << dateData[0] << L"/"
		<< dateData[2];
	return stream.str();
}

wstring OnlineLeaderboard::FormatTime(const wstring& line, const wstring &meridiem) const
{
	wstringstream stream(line);
	wstring value;
	vector<int> timeData;
	while (getline(stream, value, L':'))
		timeData.push_back(stoi(value));
	stream = wstringstream();
	//Convert to GMT+2 24h format
	int add = (meridiem == L"PM") ? 14 : 2;
	stream
		<< setfill(L'0') << setw(2) << timeData[0] % 12 + add << L":"
		<< setw(2) << timeData[1] << L":" 
		<< setw(2) << timeData[2];
	return stream.str();
}

string OnlineLeaderboard::FormatName(const string& name) const
{
	if (name.size() < 2)
	{
		Logger::LogWarning(L"OnlineLeaderboard::FormatName > Name too short!");
		return name;
	}
	stringstream nameStream;
	for (size_t i = 0; i < name.size(); i++)
	{
		if (name[i] == ' ')
			continue;
		if (i == 0)
			nameStream << static_cast<char>(toupper(name[i]));
		else
			nameStream << name[i];
	}
	return nameStream.str();
}

wstring SocketError::ErrorMessage(const wstring & context, const int errorCode) const
{
	wchar_t buf[1024];
	FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, errorCode, 0, buf, sizeof(buf), 0);
	wchar_t * newLine = wcsrchr(buf, '\r');
	if (newLine) *newLine = '\0';
	wstringstream stream;
	stream << L"Socket error in " << context << L" (" << errorCode << L"): " << buf;
	return stream.str();
}