#include <Scenegraph/GameObject.h>

class Pathfinding;
class BehaviourEnemy;
class Node;
class Player;
class SpriteFont;

class EnemyManager : public GameObject
{
public:
	EnemyManager();
	~EnemyManager();

	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
	void Draw(const GameContext& gamecontext);
	void SetTarget(Player* pTarget) { m_pTarget = pTarget; }
	void SetSpawnpoints(std::vector<XMFLOAT3> spawnpoints) { m_Spawnpoints = spawnpoints; }
	void SetEnemyPath(BehaviourEnemy* pEnemy);
private:
	enum EnemyType : int
	{
		ZOMBIE = 0,
		DEVIL = 1,
	};

	void UpdatePaths(const GameContext& gameContext);
	void UpdateSpawns(const GameContext& gameContext);
	void AddEnemy(const XMFLOAT3& position, EnemyType type);
	bool RemoveEnemy(BehaviourEnemy* pEnemy);

	//Pathfinding
	Pathfinding* m_pPathfinding = nullptr;
	map<Node*, vector<Node*>> m_Paths;
	Node* m_pLastTargetNode = nullptr;
	Player*	m_pTarget = nullptr;

	//Enemy spawning
	vector<XMFLOAT3> m_Spawnpoints;

	int m_EnemyAmount = 0;

	int m_MaxZombieAmount = 0;
	int m_CurrentZombieAmount = 0;
	float m_ZombieSpawnTimer = 0.0f;
	float m_ZombieSpawnInterval = 1.0f;

	int m_MaxDevilAmount = 1;
	int	m_CurrentDevilAmout = 0;
	float m_DevilSpawnTimer = 0.0f;
	float m_DevilSpawnInterval = 5.0f;

	bool m_WaveCleared = true;
	float m_WavePauseTimer = 0.0f;
	int m_CurrentWave = 1;
	SpriteFont* m_pWaveTextFont = nullptr;

	vector<BehaviourEnemy*> m_Enemies;

	//Sounds
	float m_GroanTimer = 0.0f;
	float m_GroanInterval = 0.0f;
	int m_GroanIndex = 0;
	vector<FMOD::Sound*> m_GroanSounds;
	FMOD::Sound* m_pZombieIncomingSound = nullptr;
};

