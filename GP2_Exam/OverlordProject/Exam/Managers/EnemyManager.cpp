#include "stdafx.h"
#include "EnemyManager.h"

#include <Content/ContentManager.h>
#include <Base/OverlordGame.h>
#include <Graphics/TextRenderer.h>
#include <Components/Components.h>

#include <Exam/Helpers/MathHelp.h>

#include <Exam/AI/Pathfinding/Pathfinding.h>
#include <Exam/AI/Enemies/BehaviourZombie.h>
#include <Exam/AI/Enemies/BehaviourDevil.h>
#include <Exam/Prefabs/Player.h>
#include <Base/SoundManager.h>

EnemyManager::EnemyManager()
{}

EnemyManager::~EnemyManager()
{}

void EnemyManager::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	m_pPathfinding = Pathfinding::GetInstance();
	m_pWaveTextFont = ContentManager::Load<SpriteFont>(L"./Resources/Fonts/Gills_50.fnt");

	//Sounds
	m_GroanSounds.push_back(SoundManager::GetInstance()->LoadSound("./Resources/Sounds/Enemies/Zombie_Groan (1).mp3", FMOD_2D));
	m_GroanSounds.push_back(SoundManager::GetInstance()->LoadSound("./Resources/Sounds/Enemies/Zombie_Groan (2).mp3", FMOD_2D));
	m_GroanSounds.push_back(SoundManager::GetInstance()->LoadSound("./Resources/Sounds/Enemies/Zombie_Groan (3).mp3", FMOD_2D));
	m_GroanSounds.push_back(SoundManager::GetInstance()->LoadSound("./Resources/Sounds/Enemies/Zombie_Groan (4).mp3", FMOD_2D));

	m_GroanInterval = randF(3.0f, 8.0f);
	m_GroanIndex = rand() % m_GroanSounds.size();

	m_pZombieIncomingSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/Enemies/ZombiesIncoming.mp3", FMOD_2D);
}

void EnemyManager::Update(const GameContext& gameContext)
{
	for (size_t i = 0; i < m_Enemies.size(); i++)
		if (m_Enemies[i]->IsDead())
			RemoveEnemy(m_Enemies[i]);

	if (m_EnemyAmount <= 0)
		m_WaveCleared = true;
	if(m_WaveCleared)
	{
		m_WavePauseTimer += gameContext.pGameTime->GetElapsed();
		if (m_WavePauseTimer < 5.0f)
			return;
		m_WavePauseTimer = 0.0f;
		++m_CurrentWave;
		m_WaveCleared = false;
		m_CurrentZombieAmount = 0;
		m_CurrentDevilAmout = 0;

		if (m_CurrentWave % 5 == 0)
			++m_MaxDevilAmount;
		m_MaxZombieAmount = m_CurrentWave * 2;

		m_EnemyAmount = m_MaxDevilAmount + m_MaxZombieAmount;
		SoundManager::GetInstance()->GetSystem()->playSound(m_pZombieIncomingSound, nullptr, false, nullptr);
	}

	UpdateSpawns(gameContext);
	UpdatePaths(gameContext);

	//Sounds
	m_GroanTimer += gameContext.pGameTime->GetElapsed();
	if(m_GroanTimer >= m_GroanInterval)
	{
		SoundManager::GetInstance()->GetSystem()->playSound(m_GroanSounds[m_GroanIndex], nullptr, false, nullptr);
		m_GroanInterval = randF(3.0f, 8.0f);
		m_GroanIndex = rand() % m_GroanSounds.size();
		m_GroanTimer = 0.0f;
	}
}

void EnemyManager::Draw(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	if (m_WaveCleared)
	{
		wstringstream ss;
		ss << "Wave " << m_CurrentWave;
		auto ws = OverlordGame::GetGameSettings().Window;
		TextRenderer::GetInstance()->DrawText(m_pWaveTextFont, ss.str(), XMFLOAT2(ws.Width / 2.0f - 80.0f, 120.0f));
	}
}

void EnemyManager::UpdatePaths(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	XMFLOAT3 targetPos = m_pTarget->GetTransform()->GetWorldPosition();
	Node* pTargetNode = m_pPathfinding->NodeFromWorldPoints(targetPos);

	//Don't need to calculate path if the player hasn't moved
	if (pTargetNode != m_pLastTargetNode)
	{
		m_pLastTargetNode = pTargetNode;

		//Clear all the paths
		m_Paths.clear();

		//Sort enemies by distance so its more efficient to get the path
		//time: +- 0.05ms
		sort(m_Enemies.begin(), m_Enemies.end(), [targetPos](BehaviourEnemy* a, BehaviourEnemy* b)
		{
			float distA = MathHelp::DistanceXZ(a->GetTransform()->GetWorldPosition(), targetPos);
			float distB = MathHelp::DistanceXZ(b->GetTransform()->GetWorldPosition(), targetPos);
			return distA > distB;
		});

		//Set the path for all the enemies
		for (size_t i = 0; i < m_Enemies.size(); i++)
		{
			if (m_Enemies[i] != nullptr)
				SetEnemyPath(m_Enemies[i]);
		}
	}
}

void EnemyManager::UpdateSpawns(const GameContext& gameContext)
{
	float deltaTime = gameContext.pGameTime->GetElapsed();
	m_DevilSpawnTimer += deltaTime;
	m_ZombieSpawnTimer += deltaTime;

	//Spawn devil(s)
	if (m_CurrentDevilAmout < m_MaxDevilAmount)
	{
		if (m_DevilSpawnTimer >= m_DevilSpawnInterval)
		{
			AddEnemy(m_Spawnpoints[rand() % m_Spawnpoints.size()], EnemyType::DEVIL);
			m_DevilSpawnTimer = 0.0f;
			m_DevilSpawnInterval = randF(8.0f, 15.0f);
			++m_CurrentDevilAmout;
		}
	}

	//Spawn zombie(s)
	if (m_CurrentZombieAmount < m_MaxZombieAmount)
	{
		if (m_ZombieSpawnTimer >= m_ZombieSpawnInterval)
		{
			AddEnemy(m_Spawnpoints[rand() % (m_Spawnpoints.size() - 1)], EnemyType::ZOMBIE);
			m_ZombieSpawnTimer = 0.0f;
			m_ZombieSpawnInterval = randF(1.0f, 3.0f);
			++m_CurrentZombieAmount;
		}
	}
}

void EnemyManager::SetEnemyPath(BehaviourEnemy* pEnemy)
{
	XMFLOAT3 enemyPos = pEnemy->GetTransform()->GetWorldPosition();

	//time: +- 0.014ms
	Node* pEnemyNode = m_pPathfinding->NodeFromWorldPoints(enemyPos);

	//Optimization:
	//Manager holds a map of paths linked to a node
	//If path already exists, there is no need to recalculate the path
	for (pair<Node*, vector<Node*>> enemyPath : m_Paths)
	{
		for (size_t pathIndex = 0; pathIndex < enemyPath.second.size(); pathIndex++)
		{
			Node* pPosNode = enemyPath.second[pathIndex];
			if(pEnemyNode == pPosNode)
			{
				pEnemy->SetPath(enemyPath.second, pathIndex);
				return;
			}
		}
	}
	//time: 5-20ms
	m_Paths[pEnemyNode] = m_pPathfinding->FindPath(enemyPos, m_pTarget->GetTransform()->GetWorldPosition());
	pEnemy->SetPath(m_Paths[pEnemyNode], 0);
}

void EnemyManager::AddEnemy(const XMFLOAT3& position, EnemyType type)
{
	BehaviourEnemy* pEnemy;
	switch (type)
	{
	case EnemyManager::ZOMBIE:
		pEnemy = new BehaviourZombie();
		break;
	case EnemyManager::DEVIL:
		pEnemy = new BehaviourDevil();
		break;
	default:
		Logger::LogWarning(L"EnemyManager: Bad enemy type!");
		return;
	}
	pEnemy->GetTransform()->Translate(position);
	AddChild(pEnemy);
	pEnemy->SetTarget(m_pTarget);
	SetEnemyPath(pEnemy);
	m_Enemies.push_back(pEnemy);
}

bool EnemyManager::RemoveEnemy(BehaviourEnemy* pEnemy)
{
	--m_EnemyAmount;
	m_Enemies.erase(find(m_Enemies.begin(), m_Enemies.end(), pEnemy));
	RemoveChild(pEnemy, true);
	return true;
}