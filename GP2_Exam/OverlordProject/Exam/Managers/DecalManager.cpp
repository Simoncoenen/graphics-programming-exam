#include "stdafx.h"
#include "DecalManager.h"
#include <Components/Components.h>
#include <Scenegraph/GameScene.h>
#include <Scenegraph/GameObject.h>

#include "Exam/Helpers/MathHelp.h"
#include <Exam/Helpers/Enumerations.h>

DecalManager::DecalManager() : 
	m_MaxSize(100),
	m_HeightCount(0),
	m_BeginHeight(0.0005f)
{

}

DecalManager::~DecalManager()
{
}

void DecalManager::Add(const XMFLOAT3& position, const Type type, GameScene* pScene)
{
	//Is the a decal close?
	float range = 2.5f;
	for (size_t i = 0; i < m_Decals.size(); i++)
	{
		if(m_Decals[i] != nullptr)
			if(MathHelp::DistanceXZ(position, m_Decals[i]->GetTransform()->GetWorldPosition()) < range)
				return;
	}

	if (static_cast<int>(m_Decals.size()) >= m_MaxSize)
	{
		pScene->RemoveChild(m_Decals[0]);
		m_Decals.pop_front();
	}
	m_HeightCount = (m_HeightCount + 1) % m_MaxSize;

	GameObject* pDecal = CreateDecal(type, position);
	m_Decals.push_back(pDecal);
	pScene->AddChild(pDecal);
}

GameObject* DecalManager::CreateDecal(const Type type, const XMFLOAT3& position)
{
	GameObject* pDecal = new GameObject();
	ModelComponent* pModel = new ModelComponent(L"./Resources/Meshes/Objects/Decal.ovm");
	float scale = 1.0f;

	switch (type)
	{
	case Type::BLOOD:
		pModel->SetMaterial(MaterialIndex::mBloodSpat);
		pDecal->GetTransform()->Rotate(0.0f, static_cast<float>(rand() % 360), 0.0f);
		scale = static_cast<float>(rand() % 50 + 80) / 100.0f;
		pDecal->GetTransform()->Scale(scale, scale, scale);
		break;
	case Type::EXPLOSION:
		pModel->SetMaterial(MaterialIndex::mExplosion);
		pDecal->GetTransform()->Rotate(0.0f, static_cast<float>(rand() % 360), 0.0f);
		scale = static_cast<float>(rand() % 100 + 160) / 100.0f;
		pDecal->GetTransform()->Scale(scale, scale, scale);
		break;
	default:
		break;
	}
	pDecal->GetTransform()->Translate(position.x, (m_HeightCount + 1) * m_BeginHeight, position.z);
	pDecal->GetTransform()->Scale(scale, scale, scale);
	pDecal->AddComponent(pModel);
	return pDecal;
}
