#include "stdafx.h"
#include "CrateManager.h"
#include <Components/Components.h>
#include <Content/ContentManager.h>
#include <Exam/Prefabs/Player.h>

#include <Exam/Prefabs/Objects/Crate.h>

CrateManager::CrateManager()
{}

CrateManager::~CrateManager()
{}

void CrateManager::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	m_RespawnTime = randF(m_MinimumRespawnTime, m_MaximumRespawnTime);
}

void CrateManager::SpawnAllCrates()
{
	for (size_t i = 0; i < m_Spawnpoints.size(); i++)
	{
		Crate* pCrate = new Crate();
		pCrate->GetTransform()->Translate(m_Spawnpoints[i]);
		AddChild(pCrate);
		m_Crates.push_back(pCrate);
	}
}

void CrateManager::Update(const GameContext& gameContext)
{
	for (size_t i = 0; i < m_Crates.size(); i++)
	{
		if(m_Crates[i] != nullptr && m_Crates[i]->IsUsed())
		{
			RemoveChild(m_Crates[i]);
			m_Crates[i] = nullptr;
		}
	}

	if (find(m_Crates.begin(), m_Crates.end(), nullptr) != m_Crates.end())
	{
		float deltaTime = gameContext.pGameTime->GetElapsed();
		m_RespawnTimer += deltaTime;

		if (m_RespawnTimer >= m_RespawnTime)
		{
			m_RespawnTimer = 0.0f;
			int spawnIdx = rand() % m_Spawnpoints.size();
			while (m_Crates[spawnIdx] != nullptr)
				spawnIdx = rand() % m_Spawnpoints.size();

			//Spawn a crate
			Crate* pCrate = new Crate();
			pCrate->GetTransform()->Translate(m_Spawnpoints[spawnIdx]);
			m_Crates[spawnIdx] = pCrate;
			AddChild(pCrate);

			m_RespawnTime = randF(m_MinimumRespawnTime, m_MaximumRespawnTime);
		}
	}
}

void CrateManager::RemoveCrate(GameObject* pCrate)
{
	auto it = find(m_Crates.begin(), m_Crates.end(), pCrate);
	if(it != m_Crates.end())
	{
		RemoveChild(pCrate);
		*it = nullptr;
	}
}