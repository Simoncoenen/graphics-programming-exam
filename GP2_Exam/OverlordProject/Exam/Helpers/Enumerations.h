enum MaterialIndex
{
	mPlayer =			0,
	mZombie =			1,
	mDevil =			2,
	mSwatch =			3,
	mGround =			4,
	mMuzzleFlash =		5,
	mBloodSpat =		6,
	mExplosion =		7,
};

enum MaterialIndex_PP
{
	ppEdgeDetection =	0,
	ppVignette		=	1,
	ppFlash			=	2,
	ppMenuVignette  =	3,
	ppGrayscale		=	4,
};

enum CollisionGroup : UINT32
{
	cStructure =	(1 << 0),
	cPlayer =		(1 << 1),
	cEnemy =		(1 << 2),
	cTrigger =		(1 << 3),
	cDestructible =	(1 << 4),
};