#include "stdafx.h"
#include "Timer.h"

Timer* Timer::m_pInstance = nullptr;

Timer::Timer()
{
	QueryPerformanceFrequency(&m_Freq);
}

Timer::~Timer()
{
}

void Timer::Start(const wstring& id)
{
	QueryPerformanceCounter(&m_Times[id]);
}

void Timer::Stop(const wstring& id)
{
#ifdef _DEBUG
	if(m_Times.find(id) == m_Times.end())
	{
		wcout << L"[INFO]\t  Timer: ID: " << id << L" not found!" << endl;
		return;
	}
#endif

	LARGE_INTEGER end;
	QueryPerformanceCounter(&end);
	float time = static_cast<float>(end.QuadPart - m_Times[id].QuadPart) * 1000.0f / m_Freq.QuadPart;
	wstringstream stream;
	stream << L"Timer::Stop > ID: " << id << L",  Ended: " << time << L" ms.";
	Logger::LogInfo(stream.str());
}

Timer* Timer::GetInstance()
{
	if (m_pInstance == nullptr)
		m_pInstance = new Timer();
	return m_pInstance;
}

void Timer::DestroyInstance()
{
	if (m_pInstance != nullptr)
	{
		delete m_pInstance;
		m_pInstance = nullptr;
	}
}
