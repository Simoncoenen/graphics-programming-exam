#pragma once
class Timer
{
public:
	Timer();
	~Timer();

	void Start(const wstring& id);
	void Stop(const wstring& id);

	static Timer* GetInstance();
	static void DestroyInstance();
private:
	LARGE_INTEGER m_Freq;
	map<wstring, LARGE_INTEGER> m_Times;

	static Timer* m_pInstance;
};

