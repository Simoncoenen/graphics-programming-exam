#include "stdafx.h"

struct MathHelp
{
	static XMVECTOR CreateLookRotation(XMVECTOR vDirection, XMVECTOR upDir)
	{
		XMVector3Normalize(-vDirection);
		XMVECTOR xmRight = XMVector3Cross(upDir, -vDirection);
		XMVECTOR xmUp = XMVector3Cross(xmRight, vDirection);
		XMFLOAT3 forward, up, right;
		XMStoreFloat3(&forward, -vDirection);
		XMStoreFloat3(&up, xmRight);
		XMStoreFloat3(&right, xmUp);

		XMFLOAT3X3 matrix;
		matrix._11 = up.x;
		matrix._12 = up.y;
		matrix._13 = up.z;
		matrix._21 = right.x;
		matrix._22 = right.y;
		matrix._23 = right.z;
		matrix._31 = forward.x;
		matrix._32 = forward.y;
		matrix._33 = forward.z;

		XMVECTOR quat, scale, trans;

		XMMATRIX m = XMLoadFloat3x3(&matrix);

		XMMatrixDecompose(&scale, &quat, &trans, m);
		return quat;
	}

	static float DistanceXZ(const XMFLOAT3& a, const XMFLOAT3& b)
	{
		return sqrt(pow(a.x - b.x, 2) + pow(a.z - b.z, 2));
	}

	static float Lerp(const float a, const float b, const float value)
	{
		return (1 - value)*a + value * b;
	}

	static float InverseLerp(const float min, const float max, const float value)
	{
		return (value - min) / (max - min);
	}
};