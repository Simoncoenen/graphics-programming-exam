#pragma once
class TextureData;
class Button;

class ButtonManager
{
public:
	ButtonManager();
	~ButtonManager();

	void Update(const GameContext& gameContext);
	void AddButton(Button* pBtn);
	void Draw();
private:
	vector<Button*> m_Buttons;
};