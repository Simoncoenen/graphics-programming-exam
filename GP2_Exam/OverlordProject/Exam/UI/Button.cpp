#include "stdafx.h"
#include "Button.h"
#include <Graphics/TextureData.h>
#include <Graphics/SpriteRenderer.h>
#include <Base/SoundManager.h>

Button::Button(const XMFLOAT2& pos, function<void(void)> callBack, TextureData* pDefaultTexture, TextureData* pHoverTexture, FMOD::Sound* pHoverSound, FMOD::Sound* pClickSound) :
	m_Callback(callBack),
	m_Position(pos),
	m_Color(static_cast<XMFLOAT4>(Colors::White)),
	m_pDefaultTexture(pDefaultTexture),
	m_pHoverTexture(pHoverTexture),
	m_pHoverSound(pHoverSound),
	m_pClickSound(pClickSound)
{
}

void Button::Update(const GameContext& gameContext)
{
	POINT mousePos = gameContext.pInput->GetMousePosition();
	if (IsOnButton(mousePos))
	{
		if (m_IsOnButton == false)
		{
			if(m_pHoverSound)
				SoundManager::GetInstance()->GetSystem()->playSound(m_pHoverSound, nullptr, false, nullptr);
			m_IsOnButton = true;
		}

		if (gameContext.pInput->IsMouseButtonDown(VK_LBUTTON))
		{
			if (m_IsPressed == false)
			{
				if (m_pClickSound)
					SoundManager::GetInstance()->GetSystem()->playSound(m_pClickSound, nullptr, false, nullptr);
				m_Callback();
				m_IsPressed = true;
			}
		}
		else
			m_IsPressed = false;
	}
	else
		m_IsOnButton = false;
}

void Button::Draw()
{
	XMFLOAT2 size = m_pDefaultTexture->GetDimension();
	XMFLOAT2 pos;
	pos.x = m_Position.x - size.x / 2.0f;
	pos.y = m_Position.y - size.y / 2.0f;

	SpriteRenderer::GetInstance()->Draw(m_IsOnButton ? m_pHoverTexture : m_pDefaultTexture, pos, (XMFLOAT4)Colors::White, XMFLOAT2(0, 0), XMFLOAT2(1.0f, 1.0f), 0.0f, 0.0f);
}

bool Button::IsOnButton(const POINT& point) const
{
	XMFLOAT2 size = m_pDefaultTexture->GetDimension();

	if (point.x < m_Position.x - size.x / 2.0f || point.x > m_Position.x + size.x / 2.0f)
		return false;
	if (point.y < m_Position.y - size.y / 2.0f || point.y > m_Position.y + size.y / 2.0f)
		return false;
	return true;
}