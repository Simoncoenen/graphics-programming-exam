#pragma once
#include <Components/BaseComponent.h>

class Player;
class SpriteFont;
class TextureData;

class HUDComponent : public BaseComponent
{
public:
	HUDComponent();
	virtual ~HUDComponent(void);
	void ShowInfo(const wstring& info);

protected:
	
	virtual void Update(const GameContext& context);
	virtual void Draw(const GameContext& context);
	virtual void Initialize(const GameContext& gameContext);

private:
	float m_InfoTimer = 0.0f;
	wstring m_Info;

	friend class GameScene;
	int m_WindowWidth = 0;
	int m_WindowHeight = 0;
	Player* m_pPlayer;
	SpriteFont* m_WeaponFont = nullptr;
	SpriteFont* m_ScoreFont = nullptr;
	SpriteFont* m_MultiplierFont = nullptr;

	TextureData* m_pComboMeter = nullptr;
	XMFLOAT2 m_MeterSize;
	TextureData* m_pComboDot = nullptr;
	XMFLOAT2 m_ComboMeterPosition;
	XMFLOAT2 m_ComboDotPosition;

	TextureData* m_pHealthBar = nullptr;

	int m_Combo = 0;

private:
	// -------------------------
	// Disabling default copy constructor and default 
	// assignment operator.
	// -------------------------
	HUDComponent( const HUDComponent &obj);
	HUDComponent& operator=( const HUDComponent& obj);
};

