//Precompiled Header [ALWAYS ON TOP IN CPP]
#include "stdafx.h"
#include "HUDComponent.h"
#include <Exam/Prefabs/Player.h>
#include <Graphics/TextRenderer.h>
#include <Base/OverlordGame.h>
#include <Content/ContentManager.h>
#include <iomanip>
#include <Components/Components.h>
#include <Graphics/TextureData.h>
#include <Graphics/SpriteRenderer.h>

//#define FPS

HUDComponent::HUDComponent():
	m_pPlayer(nullptr)
{}


HUDComponent::~HUDComponent(void)
{}

void HUDComponent::ShowInfo(const wstring& info)
{
	m_Info = info;
	m_InfoTimer = 2.0f;
}

void HUDComponent::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	m_pPlayer = static_cast<Player*>(GetGameObject());

	m_WindowWidth = OverlordGame::GetGameSettings().Window.Width;
	m_WindowHeight = OverlordGame::GetGameSettings().Window.Height;

	m_WeaponFont = ContentManager::Load<SpriteFont>(L"./Resources/Fonts/Eras_30.fnt");
	m_ScoreFont = ContentManager::Load<SpriteFont>(L"./Resources/Fonts/Eras_80.fnt");

	m_pComboDot = ContentManager::Load<TextureData>(L"./Resources/Textures/Game/UI/ComboDot.png");
	m_pComboMeter = ContentManager::Load<TextureData>(L"./Resources/Textures/Game/UI/ComboMeter.png");
	m_MeterSize = m_pComboMeter->GetDimension();
	m_ComboMeterPosition = XMFLOAT2(m_WindowWidth / 2.0f + 135.0f, 40.0f);

	m_pHealthBar = ContentManager::Load<TextureData>(L"./Resources/Textures/Game/UI/Red.png");
}

void HUDComponent::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	float angle = -XM_PIDIV2 + m_pPlayer->GetComboPercent() * (XM_PI + XM_PIDIV2);
	m_ComboDotPosition.x = cos(angle) * m_MeterSize.x / 2.0f + 30.0f + m_ComboMeterPosition.x;
	m_ComboDotPosition.y = sin(angle) * m_MeterSize.y / 2.0f + 30.0f + m_ComboMeterPosition.y;
}

void HUDComponent::Draw(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	
	//Score
	wstringstream score;
	score << setw(8) << setfill(L'0') << m_pPlayer->GetScore();
	TextRenderer::GetInstance()->DrawText(m_ScoreFont, score.str(), XMFLOAT2(m_WindowWidth / 2.0f - 200.0f, 10.0f), (XMFLOAT4)Colors::White);

	//Combo
	wstringstream combo;
	combo << m_pPlayer->GetCombo() << L'X';
	TextRenderer::GetInstance()->DrawText(m_WeaponFont, combo.str(), XMFLOAT2(m_ComboMeterPosition.x + 15.0f, m_ComboMeterPosition.y + 25.0f));
	SpriteRenderer::GetInstance()->Draw(m_pComboMeter, m_ComboMeterPosition);
	SpriteRenderer::GetInstance()->Draw(m_pComboDot, m_ComboDotPosition);

	float healthPercent = static_cast<float>(m_pPlayer->GetHealth());
	if (healthPercent > 0)
	{
		XMFLOAT2 pos = gameContext.pCamera->WorldToScreenPoint(m_pPlayer->GetTransform()->GetWorldPosition());

		//Show weapon + ammo
		int ammo = m_pPlayer->GetAmmo();
		wstring wpn = m_pPlayer->GetCurrentWeapon();
		wstringstream weapon;
		weapon << wpn;
		if (wpn != L"Pistol")
			weapon << ": " << ammo;
		TextRenderer::GetInstance()->DrawText(m_WeaponFont, weapon.str(), XMFLOAT2(pos.x - weapon.str().length() * 6.0f, pos.y + 45.0f), (XMFLOAT4)Colors::Black);

		//Health bar
		float width = 80.0f;
		float height = 20.0f;
		float edge = 3.0f;
		SpriteRenderer::GetInstance()->Draw(m_pHealthBar, XMFLOAT2(pos.x - edge - 40.0f, pos.y - 80.0f - edge), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT2(0.0f, 0.0f), XMFLOAT2(width, height));
		SpriteRenderer::GetInstance()->Draw(m_pHealthBar, XMFLOAT2(pos.x - 40.0f, pos.y - 80.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f), XMFLOAT2(0.0f, 0.0f), XMFLOAT2((healthPercent * width) - 6.0f, height - edge * 2.0f));
	}

	if(m_InfoTimer > 0)
	{
		m_InfoTimer -= gameContext.pGameTime->GetElapsed();
		TextRenderer::GetInstance()->DrawText(m_WeaponFont, m_Info, XMFLOAT2(m_WindowWidth / 2.0f - m_Info.length() * 6.0f, m_WindowHeight - 100.0f), (XMFLOAT4)Colors::White);
	}

#if defined(_DEBUG) || defined(FPS)
	wstringstream ss;
	ss << "FPS: " << 1.0f / gameContext.pGameTime->GetElapsed() << " | " << gameContext.pGameTime->GetElapsed() << " ms";
	TextRenderer::GetInstance()->DrawText(m_WeaponFont, ss.str(), XMFLOAT2(0.0f, 0.0f));
#endif
	if(m_pPlayer->IsGodModeEnabled())
		TextRenderer::GetInstance()->DrawText(m_WeaponFont, L"God Mode enabled", XMFLOAT2(0.0f, 30.0f));
}