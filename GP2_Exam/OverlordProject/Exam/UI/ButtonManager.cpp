#include "stdafx.h"
#include "ButtonManager.h"
#include "Button.h"

ButtonManager::ButtonManager()
{
}

void ButtonManager::Update(const GameContext& gameContext)
{
	for (Button* b : m_Buttons)
		b->Update(gameContext);
}

void ButtonManager::AddButton(Button* pBtn)
{
	Button* pButton = pBtn;
	m_Buttons.push_back(pButton);
}

void ButtonManager::Draw()
{
	for (Button* b : m_Buttons)
		b->Draw();
}

ButtonManager::~ButtonManager()
{
	for (size_t i = 0; i < m_Buttons.size(); i++)
	{
		if(m_Buttons[i] != nullptr)
		{
			delete m_Buttons[i];
			m_Buttons[i] = nullptr;
		}
	}
}