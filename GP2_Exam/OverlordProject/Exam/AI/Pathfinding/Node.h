class Node
{
public:
	Node(XMFLOAT3 worldPosition, bool walkable, int gridX, int gridZ);
	~Node();

	bool operator>(Node other);

	const XMFLOAT3& GetPosition() const { return m_WorldPosition; }
	const bool& IsWalkable() const { return m_Walkable; }
	int GetFCost() const { return m_gCost + m_hCost; }
	int& GetHCost() { return m_hCost; }
	int& GetGCost() { return m_gCost; }
	Node* GetParent() const { return m_pParent; }
	const int& GetX() const { return m_GridX; }
	const int& GetZ() const { return m_GridZ; }
	int& HeapIndex() { return m_HeapIndex; }
	GameObject* GetOccupyingObject() const { return m_pOccupyingObject; }

	void SetParent(Node* pNode) { m_pParent = pNode; }
	void SetOccupyingObject(GameObject* pObject) { m_pOccupyingObject = pObject; }
	void SetWalkable(bool walkable) { m_Walkable = walkable; }

private:
	Node* m_pParent = nullptr;
	XMFLOAT3 m_WorldPosition;
	int m_GridX;
	int m_GridZ;
	int m_gCost = 0;
	int m_hCost = 0;
	int m_HeapIndex = 0;

	bool m_Walkable;
	GameObject* m_pOccupyingObject = nullptr;
};

class NodeHeap
{
public:
	NodeHeap(int maxHeapSize);

	void Add(Node* item);
	Node* RemoveFirst();
	bool Contains(Node* item);
	const int& Size() const;

private:

	void UpdateItem(Node* item);
	void SortDown(Node* item);
	void SortUp(Node* item);
	void Swap(Node* itemA, Node* itemB);
	std::vector<Node*> m_Items;
	int m_CurrentItemCount = 0;
};