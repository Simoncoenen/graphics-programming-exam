#include "stdafx.h"
#include "Grid.h"
#include <Scenegraph/GameScene.h>
#include <Prefabs/QuadPrefab.h>
#include <Components/Components.h>
#include <Physx/PhysxProxy.h>
#include "Node.h"
#include <Exam/Helpers/Enumerations.h>

Grid::Grid(XMFLOAT3 center, float width, float depth, float nodeSize) :
	m_pGameScene(nullptr),
	m_Center(center),
	m_GridWidth(width),
	m_GridDepth(depth),
	m_NodeSize(nodeSize)
{
	m_NodeRadius = m_NodeSize / 2.0f;
	m_GridSizeX = static_cast<int>(width / m_NodeSize);
	m_GridSizeZ = static_cast<int>(depth / m_NodeSize);
}

Grid::~Grid()
{
	for(Node* n : m_Grid)
		if (n != nullptr)
			delete n;
}

void Grid::CreateGrid(GameScene* pGameScene)
{
	Timer a;
	m_pGameScene = pGameScene;
	m_Grid.resize(m_GridSizeX * m_GridSizeZ);
	XMFLOAT3 worldBottomLeft(m_Center.x - m_GridWidth / 2.0f, m_Center.y, m_Center.z - m_GridDepth / 2.0f);

	for (int z = 0; z < m_GridSizeZ; z++)
	{
		for (int x = 0; x < m_GridSizeX; x++)
		{
			int idx = x + z * m_GridSizeX;
			XMFLOAT3 worldPos(worldBottomLeft.x + (x * m_NodeSize + m_NodeRadius), worldBottomLeft.y, worldBottomLeft.z + (z * m_NodeSize + m_NodeRadius));

			GameObject* pOccupyingObject = NodeCheck(worldPos);
			bool walkable = false;
			if (pOccupyingObject == nullptr)
				walkable = true;
			m_Grid[idx] = new Node(worldPos, walkable, x, z);
			m_Grid[idx]->SetOccupyingObject(pOccupyingObject);
		}
	}
}

Node* Grid::NodeFromWorldPoints(const XMFLOAT3& worldPosition)
{
	int x = static_cast<int>((m_GridWidth / 2.0f + worldPosition.x) / m_NodeSize);
	int z = static_cast<int>((m_GridDepth / 2.0f + worldPosition.z) / m_NodeSize);
	Clamp(x, m_GridSizeX - 1, 0);
	Clamp(z, m_GridSizeZ - 1, 0);
	int idx = x + z * m_GridSizeX;

	return m_Grid[idx];
}

std::vector<Node*> Grid::GetNeighbours(Node* pNode)
{
	vector<Node*> neighbours;

	for (int x = -1; x <= 1; x++)
	{
		for (int z = -1; z <= 1; z++)
		{
			if (x == 0 && z == 0)
				continue;

			int currentX = pNode->GetX();
			int currentZ = pNode->GetZ();

			int checkX = currentX + x;
			int checkZ = currentZ + z;
			if (checkX >= 0 && checkX < m_GridSizeX && checkZ >= 0 && checkZ < m_GridSizeZ)
			{
				Node* nNode = m_Grid[checkX + checkZ * m_GridSizeX];
				if (x != 0 && z != 0)
				{
					int cornerCheckX = currentX + x + currentZ * m_GridSizeX;
					int cornerCheckZ = currentX + (currentZ + z) * m_GridSizeX;
					Clamp(cornerCheckX, static_cast<int>(m_Grid.size()) - 1, 0);
					Clamp(cornerCheckZ, static_cast<int>(m_Grid.size()) - 1, 0);

					if (m_Grid[cornerCheckX]->IsWalkable() == false || m_Grid[cornerCheckZ]->IsWalkable() == false)
						continue;
				}
				neighbours.push_back(nNode);
			}
		}
	}
	return neighbours;
}

void Grid::Debug()
{
	if (m_DebugNodes.size() == 0)
	{
		m_DebugNodes.resize(m_Grid.size(), nullptr);
		for (size_t i = 0; i < m_DebugNodes.size(); i++)
		{
			if (m_DebugNodes[i] == nullptr)
			{
				m_DebugNodes[i] = new QuadPrefab(m_NodeSize - 0.3f, m_NodeSize - 0.3f);
				m_pGameScene->AddChild(m_DebugNodes[i]);
			}
		}
	}

	for (size_t i = 0; i < m_DebugNodes.size(); i++)
	{
		bool walkable = m_Grid[i]->IsWalkable();
		if (walkable)
			m_DebugNodes[i]->SetColor(XMFLOAT4(0.0f, 1.0f, 0.0f, 0.2f));
		else
			m_DebugNodes[i]->SetColor(XMFLOAT4(1.0f, 0.0f, 0.0f, 0.2f));

		XMFLOAT3 pos = m_Grid[i]->GetPosition();
		pos.y += 0.1f;
		m_DebugNodes[i]->GetTransform()->Translate(pos);
	}
}

GameObject* Grid::NodeCheck(const XMFLOAT3& position) const
{
	PhysxProxy *pProxy = m_pGameScene->GetPhysxProxy();
	PxSweepBuffer hit;
	PxQueryFilterData filterData;
	filterData.data.word0 = CollisionGroup::cStructure | CollisionGroup::cDestructible;
	if (pProxy->Sweep(PxSphereGeometry(m_NodeRadius - 1.0f), PxTransform(ToPxVec3(position) + PxVec3(0.0f, 8.0f, 0.0f)), PxVec3(1.0f, 0.0f, 0.0f), 0.0f, hit, PxHitFlag::eDEFAULT ,filterData))
	{
		auto actor = hit.block.actor;
		if (actor != nullptr)
		{
			auto userData = actor->userData;
			if (userData != nullptr)
			{
				return reinterpret_cast<BaseComponent*>(userData)->GetGameObject();
			}
		}
	}
	return nullptr;
}
