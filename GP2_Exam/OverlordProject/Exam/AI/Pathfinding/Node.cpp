#include "stdafx.h"
#include "Node.h"

Node::Node(XMFLOAT3 worldPosition, bool walkable, int gridX, int gridZ) :
	m_pParent(nullptr),
	m_WorldPosition(worldPosition),
	m_GridX(gridX),
	m_GridZ(gridZ),
	m_Walkable(walkable)
{}

Node::~Node()
{}

bool Node::operator>(Node other)
{
	if (GetFCost() == other.GetFCost())
		return GetHCost() > other.GetHCost();
	return GetFCost() < other.GetFCost();
}

NodeHeap::NodeHeap(int maxHeapSize)
{
	m_Items.resize(maxHeapSize, nullptr);
}

void NodeHeap::Add(Node* item)
{
	item->HeapIndex() = m_CurrentItemCount;
	m_Items[m_CurrentItemCount] = item;
	SortUp(item);
	++m_CurrentItemCount;
}

Node* NodeHeap::RemoveFirst()
{
	Node* item = m_Items[0];
	--m_CurrentItemCount;
	m_Items[0] = m_Items[m_CurrentItemCount];
	m_Items[0]->HeapIndex() = 0;
	SortDown(m_Items[0]);
	return item;
}

bool NodeHeap::Contains(Node* item)
{
	return m_Items[item->HeapIndex()] == item;
}

const int& NodeHeap::Size() const
{
	return m_CurrentItemCount;
}

void NodeHeap::UpdateItem(Node* item)
{
	SortUp(item);
}

void NodeHeap::SortDown(Node* item)
{
	for (;;)
	{
		int childIdxLeft = item->HeapIndex() * 2 + 1;
		int childIdxRight = item->HeapIndex() * 2 + 2;

		if (childIdxLeft < m_CurrentItemCount)
		{
			int swapIdx = childIdxLeft;
			if (childIdxRight < m_CurrentItemCount)
			{
				if (*m_Items[childIdxRight] > *m_Items[childIdxLeft])
					swapIdx = childIdxRight;
			}

			if (*m_Items[swapIdx] > *item)
				Swap(m_Items[swapIdx], item);
			else
				return;
		}
		else
			return;
	}
}

void NodeHeap::SortUp(Node* item)
{
	int parentIdx = (item->HeapIndex() - 1) / 2;
	for (;;)
	{
		Node* parentItem = m_Items[parentIdx];
		if (*item > *parentItem)
			Swap(item, parentItem);
		else
			break;
		parentIdx = (item->HeapIndex() - 1) / 2;
	}
}

void NodeHeap::Swap(Node* itemA, Node* itemB)
{
	m_Items[itemA->HeapIndex()] = itemB;
	m_Items[itemB->HeapIndex()] = itemA;
	int itemAidx = itemA->HeapIndex();
	itemA->HeapIndex() = itemB->HeapIndex();
	itemB->HeapIndex() = itemAidx;
}
