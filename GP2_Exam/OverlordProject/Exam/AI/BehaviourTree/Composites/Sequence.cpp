#include "stdafx.h"
#include "Composites.h"

BehaviourState Sequence::Execute()
{
	for (BehaviourNode* child : m_ChildrenBehaviours)
	{
		m_CurrentState = child->Execute();
		switch (m_CurrentState)
		{
		case BehaviourState::FAILURE:
			return m_CurrentState;
		case BehaviourState::SUCCESS:
			continue;
		case BehaviourState::RUNNING:
			return m_CurrentState;
		default:
			continue;
		}
	}
	return m_CurrentState = BehaviourState::SUCCESS;
}