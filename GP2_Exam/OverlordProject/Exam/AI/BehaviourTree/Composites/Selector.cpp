#include "stdafx.h"
#include "Composites.h"

BehaviourState Selector::Execute()
{
	for (BehaviourNode* child : m_ChildrenBehaviours)
	{
		m_CurrentState = child->Execute();
		switch (m_CurrentState)
		{
		case BehaviourState::FAILURE:
			continue;
		case BehaviourState::SUCCESS:
			return m_CurrentState;
		case BehaviourState::RUNNING:
			return m_CurrentState;
		default:
			continue;
		}
	}
	return m_CurrentState = BehaviourState::FAILURE;
}
