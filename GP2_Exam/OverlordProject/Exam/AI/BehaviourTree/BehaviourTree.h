#pragma once
#include <Scenegraph/GameObject.h>
#include "Constants.h"
class BehaviourNode;

class BehaviourTree : public GameObject
{
public:
	BehaviourTree(){}
	virtual ~BehaviourTree(){}

	virtual void Initialize(const GameContext& gameContext) { UNREFERENCED_PARAMETER(gameContext); }
	virtual void Update(const GameContext& gamecontext);

	BehaviourState Execute();

	void SetState(const BehaviourState state) { m_CurrentState = state; }
	void SetComposite(BehaviourNode* composite) { m_CurrentComposite = composite; }

protected:
	float m_DeltaTime = 0.0f;

private:

	BehaviourState m_CurrentState = BehaviourState::RUNNING;
	BehaviourNode* m_CurrentComposite = nullptr;
};