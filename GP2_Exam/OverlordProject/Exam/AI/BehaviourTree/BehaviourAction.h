#include "Composites/Composites.h"

class BehaviourAction : public BehaviourNode
{
public:
	BehaviourAction(std::function<BehaviourState(void)> action):m_Action(action)
	{}
	~BehaviourAction(){}

	BehaviourState Execute()
	{
		if (m_Action == nullptr)
			return BehaviourState::FAILURE;
		return m_CurrentState = m_Action();
	}
private:
	std::function<BehaviourState(void)> m_Action;
};