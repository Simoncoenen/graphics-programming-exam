#include "stdafx.h"
#include "BehaviourTree.h"
#include "./Composites/Composites.h"

void BehaviourTree::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	m_DeltaTime = gameContext.pGameTime->GetElapsed();
	Execute();
}

BehaviourState BehaviourTree::Execute()
{
	if (m_CurrentComposite == nullptr)
		return BehaviourState::FAILURE;
	return m_CurrentState = m_CurrentComposite->Execute();
}
