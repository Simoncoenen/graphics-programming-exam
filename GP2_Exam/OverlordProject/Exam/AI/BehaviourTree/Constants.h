#pragma once
enum class BehaviourState
{
	RUNNING,
	SUCCESS,
	FAILURE,
};