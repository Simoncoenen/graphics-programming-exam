#include "stdafx.h"
#include "BehaviourEnemy.h"
#include <Components/Components.h>
#include <Exam/Helpers/MathHelp.h>
#include <Exam/AI/Pathfinding/Node.h>
#include <Exam/AI/BehaviourTree/Composites/Composites.h>
#include <Graphics/ModelAnimator.h>
#include <Exam/Prefabs/Player.h>
#include <Exam/Helpers/Enumerations.h>
#include <Physx/PhysxManager.h>
#include <Exam/Managers/DecalManager.h>
#include <Prefabs/QuadPrefab.h>
#include <Scenegraph/GameScene.h>
#include <Base/SoundManager.h>

BehaviourEnemy::~BehaviourEnemy()
{
	SafeDelete(m_Root);
}

void BehaviourEnemy::Initialize(const GameContext& gameContext)
{
	SetTag(L"Enemy");
	m_pController = new ControllerComponent(PhysxManager::GetInstance()->GetDefaultMaterial(), m_Radius, m_Height);
	m_pController->SetCollisionGroup(CollisionGroup::cEnemy);
	AddComponent(m_pController);

	Init(gameContext);
}

void BehaviourEnemy::PostInitialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	m_pAnimator = m_pModelComponent->GetAnimator();
}

void BehaviourEnemy::Update(const GameContext& gameContext)
{
	if(m_Active)
		BehaviourTree::Update(gameContext);
}

void BehaviourEnemy::SetPath(std::vector<Node*> path, const int pathIndex)
{
	m_CurrentPathIndex = pathIndex;
	m_Path = path;
}

void BehaviourEnemy::Damage(int damage)
{
	m_Health -= damage;
	DecalManager::GetInstance()->Add(GetTransform()->GetWorldPosition(), DecalManager::Type::BLOOD, GetScene());
}

BehaviourState BehaviourEnemy::MoveToTarget()
{
	if (m_pAnimator != nullptr)
	{
		float speed = m_pController->GetVelocity();
		m_pAnimator->SetAnimation(L"Walk");
		m_pAnimator->Play();
		float percentage = speed / m_MoveSpeed * 40;
		Clamp(percentage, 1.0f, 0.001f);
		m_pAnimator->SetAnimationSpeed(percentage);
	}

	//Move in forward direction
	XMFLOAT3 moveDirection = GetTransform()->GetForward();
	moveDirection.x *= m_MoveSpeed * m_DeltaTime * -1;
	moveDirection.y *= m_MoveSpeed * m_DeltaTime * -1;
	moveDirection.z *= m_MoveSpeed * m_DeltaTime * -1;
	m_pController->Move(moveDirection);

	return BehaviourState::SUCCESS;
}

BehaviourState BehaviourEnemy::LookAtTarget()
{
	//If the enemy has no path, just return
	if (m_Path.size() == 0)
	{
		Logger::LogInfo(L"Enemy: Path size is 0.");
		return BehaviourState::FAILURE;
	}

	//Calculate the look direction
	XMFLOAT3 enemyPosition = GetTransform()->GetWorldPosition();

	//LookTarget is the next waypoint
	XMFLOAT3 lookTarget = m_Path[m_CurrentPathIndex]->GetPosition();

	//If the enemy is close to a waypoint and there is a next one, increase the waypoint index
	if (MathHelp::DistanceXZ(enemyPosition, lookTarget) <= 5.5f && m_CurrentPathIndex < static_cast<int>(m_Path.size()) - 1)
		++m_CurrentPathIndex;

	XMFLOAT3 lookDirection;
	lookDirection.x = lookTarget.x - enemyPosition.x;
	lookDirection.y = 0.0f;
	lookDirection.z = lookTarget.z - enemyPosition.z;

	//Calculate the rotation to the target (quaternion)
	XMFLOAT3 up(0.0f, 1.0f, 0.0f);
	XMVECTOR xmTargetRotation = MathHelp::CreateLookRotation(XMLoadFloat3(&lookDirection), XMLoadFloat3(&up));
	XMFLOAT4 currentRotation = GetTransform()->GetRotation();
	XMVECTOR xmCurrentRotation = XMLoadFloat4(&currentRotation);
	//Slerp towards target
	GetTransform()->Rotate(XMQuaternionSlerp(xmCurrentRotation, xmTargetRotation, m_SlerpValue * m_DeltaTime));

	return BehaviourState::SUCCESS;
}

BehaviourState BehaviourEnemy::IsInRange()
{
	float distance = MathHelp::DistanceXZ(GetTransform()->GetWorldPosition(), m_pTarget->GetTransform()->GetWorldPosition());
	if (distance <= m_AttackRange)
			return BehaviourState::SUCCESS;
	return BehaviourState::FAILURE;
}

BehaviourState BehaviourEnemy::Die()
{
	if(m_Health <= 0)
	{
		//Check if already died so we play sound only once
		if (m_Died == false)
		{
			m_pTarget->AddKill();
			int randomSound = rand() % 2 + 1;
			stringstream stream;
			stream << "./Resources/Sounds/Enemies/ZombieDeath (" << randomSound << ").wav";
			FMOD::Sound* pSound = SoundManager::GetInstance()->LoadSound(stream.str(), FMOD_2D);
			SoundManager::GetInstance()->GetSystem()->playSound(pSound, nullptr, false, nullptr);
			RemoveComponent(m_pController);
			SafeDelete(m_pController);
			m_pAnimator->SetAnimation(L"Death");
			m_pAnimator->SetLoop(false);
			m_pAnimator->Play();
			m_Died = true;
		}
    	m_DeathTimer += m_DeltaTime;
		if (m_DeathTimer >= 2.0f)
		{
			m_IsDead = true;
			return BehaviourState::SUCCESS;
		}
		return BehaviourState::RUNNING;
	}
	return BehaviourState::FAILURE;
}

void BehaviourEnemy::Debug()
{
	if(m_DebugInitialized == false)
	{
		m_DebugColor = XMFLOAT4(randF(0.f, 1.f), randF(0.f, 1.f), randF(0.f, 1.f), 1.f);
		m_DebugInitialized = true;
	}

	size_t pathSize = m_Path.size();
	if(pathSize > m_DebugQuads.size())
		m_DebugQuads.resize(pathSize);

	for (size_t i = 0; i < m_DebugQuads.size(); i++)
	{
		if (i >= pathSize)
		{
			m_DebugQuads[i]->GetTransform()->Translate(0.0f, -50.0f, 0.0f);
			continue;
		}

		if (m_DebugQuads[i] == nullptr)
		{
			m_DebugQuads[i] = new QuadPrefab(3.0f, 3.0f);
			GetScene()->AddChild(m_DebugQuads[i]);
			m_DebugQuads[i]->SetColor(m_DebugColor);
		}
		XMFLOAT3 pos = m_Path[i]->GetPosition();
		pos.y += 0.2f;
		m_DebugQuads[i]->GetTransform()->Translate(pos);
	}
}
