#include "stdafx.h"
#include "DevilFireball.h"
#include <Components/Components.h>
#include <Content/ContentManager.h>
#include <Scenegraph/GameScene.h>
#include <Physx/PhysxManager.h>
#include <Exam/Helpers/Enumerations.h>

#include <Exam/Prefabs/Player.h>
#include <Exam/Prefabs/Objects/Barrel.h>
#include <Exam/Prefabs/Objects/FakeWall.h>

DevilFireball::DevilFireball()
{}

DevilFireball::~DevilFireball()
{}

void DevilFireball::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	//Create the object
	ModelComponent* pModelComponent = new ModelComponent(L"./Resources/Meshes/Objects/DevilFireball.ovm");
	pModelComponent->SetMaterial(MaterialIndex::mSwatch);
	AddComponent(pModelComponent);
	RigidBodyComponent* pRigidbody = new RigidBodyComponent(false);
	AddComponent(pRigidbody);
	shared_ptr<PxGeometry>geom(new PxSphereGeometry(2.0f));
	ColliderComponent* pCollider = new ColliderComponent(geom, *PhysxManager::GetInstance()->GetDefaultMaterial());
	AddComponent(pCollider);

	pRigidbody->SetCollisionGroup(CollisionGroup::cTrigger);
	pRigidbody->SetCollisionIgnoreGroups(CollisionGroup::cEnemy | CollisionGroup::cTrigger);

	//Set the trigger effects
	SetOnTriggerCallBack
	(
		[this](GameObject* trigger, GameObject* receiver, TriggerAction action) 
		{
			UNREFERENCED_PARAMETER(trigger);
			if(action == TriggerAction::ENTER)
			{
				//Destroy if receiver is not another enemy
				//Deal damage to the player
				if (receiver->GetTag() == L"Player")
					static_cast<Player*>(receiver)->ReceiveDamage(10);
				if (receiver->GetTag() == L"Barrel")
					static_cast<Barrel*>(receiver)->Explode();
				if (receiver->GetTag() == L"Fakewall")
					static_cast<FakeWall*>(receiver)->Damage(10);
				Destroy();
			}
		}
	);
	pCollider->EnableTrigger(true);	
	
}

void DevilFireball::PostInitialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	GetComponent<RigidBodyComponent>()->GetPxRigidBody()->setActorFlag(PxActorFlag::eDISABLE_GRAVITY, true);
}

void DevilFireball::Update(const GameContext& gameContext)
{
	m_LifeTimer += gameContext.pGameTime->GetElapsed();
	if (m_LifeTimer >= m_LifeTime)
		Destroy();
}

void DevilFireball::ApplyForce(const PxVec3& force)
{
	GetComponent<RigidBodyComponent>()->AddForce(force, PxForceMode::eIMPULSE);
}