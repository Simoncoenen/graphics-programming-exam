#include "stdafx.h"
#include "BehaviourDevil.h"
#include <Exam/Helpers/Enumerations.h>
#include <Components/Components.h>
#include <Scenegraph/GameScene.h>
#include <Physx/PhysxProxy.h>
#include <Exam/Helpers/MathHelp.h>

#include "DevilFireball.h"
#include "../BehaviourTree/BehaviourAction.h"
#include <Graphics/ModelAnimator.h>
#include <Exam/Prefabs/Player.h>

BehaviourDevil::BehaviourDevil()
{}

BehaviourDevil::~BehaviourDevil()
{}

void BehaviourDevil::Init(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	m_AttackRange = 60.0f;

	//Seperate gameobject from mesh
	GameObject* pMeshObject = new GameObject();
	m_pModelComponent = new ModelComponent(L"./Resources/Meshes/Characters/Devil_Anim.ovm");
	m_pModelComponent->SetMaterial(MaterialIndex::mDevil);
	pMeshObject->AddComponent(m_pModelComponent);
	pMeshObject->GetTransform()->Scale(0.9f, 0.9f, 0.9f);
	pMeshObject->GetTransform()->Translate(0.0f, -m_Height, 0.0f);
	AddChild(pMeshObject);

	//Behaviour Tree
	m_Root = new Selector();

	BehaviourAction* action = new BehaviourAction(bind(&BehaviourDevil::Die, this));
	m_Root->AddChild(action);

	Sequence* pSequence = new Sequence();
	pSequence->AddChild(new BehaviourAction(bind(&BehaviourDevil::IsInRange, this)));
	pSequence->AddChild(new BehaviourAction(bind(&BehaviourDevil::LookAtPlayer, this)));
	pSequence->AddChild(new BehaviourAction(bind(&BehaviourDevil::Attack, this)));
	m_Root->AddChild(pSequence);

	Sequence* pSequence2 = new Sequence();

	pSequence2->AddChild(new BehaviourAction(bind(&BehaviourDevil::MoveToTarget, this)));
	pSequence2->AddChild(new BehaviourAction(bind(&BehaviourDevil::LookAtTarget, this)));
	m_Root->AddChild(pSequence2);
	SetComposite(m_Root);

	//Fireball spawn
	m_pFireBallSpawnSocket = new GameObject();
	m_pFireBallSpawnSocket->GetTransform()->Translate(m_FireBallSpawnPosition);
	AddChild(m_pFireBallSpawnSocket);

	m_DeathAnimationTime = 0.35f;
}

BehaviourState BehaviourDevil::Attack()
{
	m_pAnimator->SetAnimation(L"Attack");
	m_pAnimator->Play();
	if (m_pAnimator->AtTheEnd())
	{
		//Spawn a fireball
		DevilFireball* pFireBall = new DevilFireball();
		pFireBall->GetTransform()->Translate(m_pFireBallSpawnSocket->GetTransform()->GetWorldPosition());
		GetScene()->AddChild(pFireBall);
		pFireBall->ApplyForce(-ToPxVec3(GetTransform()->GetForward()) * 30.0f);
	}
	return BehaviourState::RUNNING;
}

BehaviourState BehaviourDevil::LookAtPlayer()
{
	//Calculate the look direction
	XMFLOAT3 enemyPosition = GetTransform()->GetWorldPosition();
	XMFLOAT3 targetPosition = m_pTarget->GetTransform()->GetWorldPosition();

	XMFLOAT3 lookDirection;
	lookDirection.x = targetPosition.x - enemyPosition.x;
	lookDirection.y = 0.0f;
	lookDirection.z = targetPosition.z - enemyPosition.z;

	//Calculate the rotation to the target (quaternion)
	XMFLOAT3 up(0.0f, 1.0f, 0.0f);
	XMVECTOR xmTargetRotation = MathHelp::CreateLookRotation(XMLoadFloat3(&lookDirection), XMLoadFloat3(&up));
	XMFLOAT4 currentRotation = GetTransform()->GetRotation();
	XMVECTOR xmCurrentRotation = XMLoadFloat4(&currentRotation);
	//Slerp towards target
	GetTransform()->Rotate(XMQuaternionSlerp(xmCurrentRotation, xmTargetRotation, m_SlerpValue * m_DeltaTime));

	PxVec3 dir = PxVec3(targetPosition.x - enemyPosition.x, targetPosition.y - enemyPosition.y, targetPosition.z - enemyPosition.z);
	dir.normalize();
	PxRaycastBuffer hit;
	PxQueryFilterData filterData;

	filterData.data.word0 = CollisionGroup::cStructure | CollisionGroup::cPlayer;
	if (GetScene()->GetPhysxProxy()->Raycast(ToPxVec3(enemyPosition), dir, PX_MAX_F32, hit, PxHitFlag::eDEFAULT, filterData))
	{
		PxRigidActor* actor = hit.block.actor;
		if (actor != nullptr)
		{
			auto userData = actor->userData;
			if (userData != nullptr)
			{
				GameObject* hitObject = reinterpret_cast<BaseComponent*>(userData)->GetGameObject();
				if (hitObject == m_pTarget)
					return BehaviourState::SUCCESS;
			}
		}
	}
	return BehaviourState::FAILURE;
}
