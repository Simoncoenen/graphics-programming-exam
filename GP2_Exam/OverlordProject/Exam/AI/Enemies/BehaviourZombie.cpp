#include "stdafx.h"
#include "BehaviourZombie.h"
#include "../BehaviourTree/BehaviourAction.h"
#include <Components/Components.h>
#include <Exam/Helpers/Enumerations.h>
#include <Graphics/ModelAnimator.h>
#include <Exam/Prefabs/Player.h>

BehaviourZombie::BehaviourZombie()
{
}


BehaviourZombie::~BehaviourZombie()
{
}

void BehaviourZombie::Init(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	//Seperate gameobject from mesh
	GameObject* pMeshObject = new GameObject();
	m_pModelComponent = new ModelComponent(L"./Resources/Meshes/Characters/Zombie_Anim.ovm");
	m_pModelComponent->SetMaterial(MaterialIndex::mZombie);
	pMeshObject->AddComponent(m_pModelComponent);
	pMeshObject->GetTransform()->Scale(0.9f, 0.9f, 0.9f);
	pMeshObject->GetTransform()->Translate(0.0f, -m_Height, 0.0f);
	AddChild(pMeshObject);

	//Behaviour Tree
	m_Root = new Selector();

	BehaviourAction* action = new BehaviourAction(bind(&BehaviourZombie::Die, this));
	m_Root->AddChild(action);

	Sequence* pSequence2 = new Sequence();
	pSequence2->AddChild(new BehaviourAction(bind(&BehaviourZombie::IsInRange, this)));
	pSequence2->AddChild(new BehaviourAction(bind(&BehaviourZombie::IsInFront, this)));
	pSequence2->AddChild(new BehaviourAction(bind(&BehaviourZombie::Attack, this)));
	m_Root->AddChild(pSequence2);

	Sequence* pSequence3 = new Sequence();

	pSequence3->AddChild(new BehaviourAction(bind(&BehaviourZombie::MoveToTarget, this)));
	pSequence3->AddChild(new BehaviourAction(bind(&BehaviourZombie::LookAtTarget, this)));
	m_Root->AddChild(pSequence3);
	SetComposite(m_Root);

	m_DeathAnimationTime = 0.5f;
}

BehaviourState BehaviourZombie::Attack()
{
	m_pAnimator->SetAnimation(L"Attack");
	m_pAnimator->Play();
	if(m_pAnimator->AtTheEnd())
	{
		m_pTarget->ReceiveDamage(m_Damage);
	}
	return BehaviourState::RUNNING;
}

BehaviourState BehaviourZombie::IsInFront()
{
	XMFLOAT3 fwd = GetTransform()->GetForward();
	XMFLOAT3 pos = GetTransform()->GetWorldPosition();
	XMFLOAT3 targetPos = m_pTarget->GetTransform()->GetWorldPosition();
	XMFLOAT3 dir;
	dir.x = targetPos.x - pos.x;
	dir.y = targetPos.y - pos.y;
	dir.z = targetPos.z - pos.z;
	XMVECTOR xmDir = XMLoadFloat3(&dir);
	xmDir = XMVector3Normalize(xmDir);
	XMVECTOR xmFwd = XMLoadFloat3(&fwd);
	XMVECTOR xmDot = XMVector3Dot(xmDir, xmFwd);
	float dot;
	XMStoreFloat(&dot, xmDot);

	if (dot + 1 < 0.1f)
		return BehaviourState::SUCCESS;

	return BehaviourState::FAILURE;
}