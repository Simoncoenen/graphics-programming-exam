#pragma once
#include "../BehaviourTree/BehaviourTree.h"

class BehaviourNode;
class Node;
class ControllerComponent;
class ModelAnimator;
class ModelComponent;
class Player;
class QuadPrefab;

class BehaviourEnemy : public BehaviourTree
{
public:
	BehaviourEnemy(){}
	~BehaviourEnemy();

	void Initialize(const GameContext& gameContext);
	void PostInitialize(const GameContext& gameContext);
	virtual void Init(const GameContext& gameContext) = 0;
	void Update(const GameContext& gameContext);

	void SetPath(std::vector<Node*> path, const int pathIndex);
	void SetTarget(Player* pTarget) { m_pTarget = pTarget; }
	void Damage(int damage);

	void SetActive(const bool active = true) { m_Active = active; }
	bool IsDead() const { return m_IsDead; }

	void Debug();

protected:
	//Behaviour Tree
	BehaviourNode* m_Root = nullptr;
	BehaviourState MoveToTarget();
	BehaviourState LookAtTarget();
	BehaviourState IsInRange();
	virtual BehaviourState Attack() = 0;
	BehaviourState Die();

	//Animations
	ModelAnimator* m_pAnimator = nullptr;
	ModelComponent* m_pModelComponent = nullptr;

	//Properties
	float m_MoveSpeed = 7.0f;
	float m_SlerpValue = 5.0f;
	bool m_IsDead = false;
	int m_Health = 10;
	float m_DeathTimer = 0.0f;
	float m_DeathAnimationTime = 0.0f;
	bool m_Active = true;

	//Attacking
	float m_AttackRange = 11.0f;
	float m_AttackTimer = 0.0f;
	float m_AttackRate = 1.0f;
	int m_Damage = 20;

	//Components
	ControllerComponent* m_pController = nullptr;
	float m_Height = 7.0f;
	float m_Radius = 4.0f;
	bool m_IsAttacking = false;

	//Pathfinding
	Player* m_pTarget = nullptr;
	vector<Node*> m_Path;
	int m_CurrentPathIndex = 0;

	//Debugging
	vector<QuadPrefab*> m_DebugQuads;
	bool m_DebugInitialized = false;
	XMFLOAT4 m_DebugColor;

	bool m_Died = false;
};

