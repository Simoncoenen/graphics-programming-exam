#pragma once
#include "BehaviourEnemy.h"

class BehaviourZombie :	public BehaviourEnemy
{
public:
	BehaviourZombie();
	~BehaviourZombie();

	void Init(const GameContext& gameContext);

private:
	BehaviourState IsInFront();
	BehaviourState Attack();
};

