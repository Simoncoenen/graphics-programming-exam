#pragma once
#include "BehaviourEnemy.h"

class BehaviourDevil :	public BehaviourEnemy
{
public:
	BehaviourDevil();
	~BehaviourDevil();

	void Init(const GameContext& gameContext);

private:
	BehaviourState Attack();
	BehaviourState LookAtPlayer();

	GameObject* m_pFireBallSpawnSocket = nullptr;
	XMFLOAT3 m_FireBallSpawnPosition = { 0.0f, 3.0f, -7.0f };
};

