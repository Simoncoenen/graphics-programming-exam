#pragma once
#include <Scenegraph\GameObject.h>
class DevilFireball : public GameObject
{
public:
	DevilFireball();
	~DevilFireball();

	void Initialize(const GameContext& gameContext);
	void PostInitialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
	void ApplyForce(const PxVec3& force);

private:
	float m_LifeTime = 2.0f;
	float m_LifeTimer = 0.0f;
};

