#include "./Scenegraph/GameObject.h"

class TopDownCamera : public GameObject
{
public:
	TopDownCamera();
	~TopDownCamera();

	//Delete copy- and assignment constructor
	TopDownCamera& operator=(const TopDownCamera &other) = delete;
	TopDownCamera(const TopDownCamera& other) = delete;

	void Initialize(const GameContext &gameContext);
	void Update(const GameContext& gameContext);

	void SetTarget(GameObject* pTarget) { m_pTarget = pTarget; }
	void SetProperties(const float height, const float speed);
	void SetWorldBounds(const RECT &bounds) { m_WorldBounds = bounds; m_LimitsEnabled = true; }
	void EnableLimits(const bool enabled) { m_LimitsEnabled = enabled; }
	void CameraRotate(const float x, const float y = 0.0f, const float z = 0.0f);
	float GetZOffset() const { return m_OffsetZ; }

private:
	CameraComponent* m_pCameraComponent = nullptr;
	GameObject* m_pTarget = nullptr;

	bool m_LimitsEnabled = false;
	RECT m_WorldBounds;
	float m_Height = 100.0f;
	float m_Speed = 20.0f;
	float m_OffsetZ = 0.0f;
	float m_Angle = 0.0f;
};