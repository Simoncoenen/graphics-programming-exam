#include "stdafx.h"
#include "TopDownCamera.h"
#include <Components/TransformComponent.h>
#include <Base/OverlordGame.h>

TopDownCamera::TopDownCamera()
{

}

TopDownCamera::~TopDownCamera()
{
}

void TopDownCamera::Initialize(const GameContext &gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	m_pCameraComponent = new CameraComponent();
	AddComponent(m_pCameraComponent);
}

void TopDownCamera::Update(const GameContext& gameContext)
{
	XMFLOAT3 targetPos = m_pTarget->GetTransform()->GetWorldPosition();
	if (m_LimitsEnabled)
	{
		GameSettings settings = OverlordGame::GetGameSettings();
		float orthoSize = m_pCameraComponent->GetOrthoSize() / 2.0f;
		float vertExtent = orthoSize / sin(m_Angle);
		float horzExtent = orthoSize * settings.Window.AspectRatio;

		Clamp(targetPos.z, static_cast<float>(m_WorldBounds.top) - vertExtent, static_cast<float>(m_WorldBounds.bottom) + vertExtent);
		Clamp(targetPos.x, static_cast<float>(m_WorldBounds.right) - horzExtent, static_cast<float>(m_WorldBounds.left) + horzExtent);
	}

	targetPos.y += m_Height;
	targetPos.z -= m_OffsetZ;

	XMFLOAT3 currentPos = GetTransform()->GetWorldPosition();
	XMVECTOR t = XMLoadFloat3(&targetPos);
	XMVECTOR p = XMLoadFloat3(&currentPos);
	XMVECTOR dir = t - p;
	XMVector3Normalize(dir);
	dir *= m_Speed * gameContext.pGameTime->GetElapsed();
	dir += p;
	XMFLOAT3 moveDirection;
	XMStoreFloat3(&moveDirection, dir);
	GetTransform()->Translate(moveDirection);
}

void TopDownCamera::SetProperties(const float height, const float speed)
{
	m_Height = height;
	m_Speed = speed;
}

void TopDownCamera::CameraRotate(const float x, const float y, const float z)
{
	m_Angle = x * PxPi / 180.0f;
	GetTransform()->Rotate(x, y, z);
	//Offset the camera so that the player is always in the middle of the screen
	m_OffsetZ = m_Height * tan((90 - x) * (XM_PI / 180.0f));
}