#include "stdafx.h"
#include "UziWeapon.h"
#include "./Components/Components.h"
#include <Exam/Helpers/Enumerations.h>
#include <Exam/Prefabs/Weapons/MuzzleFlash.h>
#include <Base/SoundManager.h>

UziWeapon::UziWeapon() :
	Weapon()
{
}

UziWeapon::~UziWeapon()
{
}

void UziWeapon::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	m_pModelComponent = new ModelComponent(L"./Resources/Meshes/Weapons/Uzi.ovm");
	m_pModelComponent->SetMaterial(MaterialIndex::mSwatch);
	AddComponent(m_pModelComponent);

	m_pMuzzleFlash = new MuzzleFlash();
	m_pMuzzleFlash->GetTransform()->Translate(XMFLOAT3(0.0f, 1.1f, -3.5f));
	AddChild(m_pMuzzleFlash);

	//Sounds
	m_pSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/Weapons/uzi.wav", FMOD_2D);
}

void UziWeapon::Shoot(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	if (m_FireTimer > m_FireRate)
	{
		SoundManager::GetInstance()->GetSystem()->playSound(m_pSound, nullptr, false, &m_pChannel);

		m_pMuzzleFlash->Activate();

		m_FireTimer = 0.0f;
		--m_Ammo;

		PxVec3 weaponPos = ToPxVec3(GetTransform()->GetWorldPosition());
		PxVec3 rayStart = weaponPos;
		PxVec3 rayDir = -ToPxVec3(GetTransform()->GetForward());

		GameObject* hitObject = GetHit(rayStart, rayDir, m_Range);
		if (hitObject != nullptr)
			DealDamage(hitObject);
	}
}