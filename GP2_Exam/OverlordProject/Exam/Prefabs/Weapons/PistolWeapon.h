#pragma once
#include "Weapon.h"

class PistolWeapon final : public Weapon
{
public:
	PistolWeapon();
	~PistolWeapon();

	void Initialize(const GameContext& gameContext);
	void Shoot(const GameContext& gameContext);
};