#include "stdafx.h"
#include "FakeWallWeapon.h"
#include <Scenegraph/GameScene.h>
#include <Exam/Prefabs/Objects/FakeWall.h>
#include <Components/Components.h>
#include <Exam/AI/Pathfinding/Pathfinding.h>
#include <Exam/AI/Pathfinding/Node.h>
#include <Exam/Helpers/MathHelp.h>
#include <Base/SoundManager.h>

FakeWallWeapon::FakeWallWeapon()
{
}

void FakeWallWeapon::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	//Sounds
	m_pSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/ItemPlace.mp3", FMOD_2D);
}

void FakeWallWeapon::Shoot(const GameContext& gameContext)
{
	if(m_FireTimer >= m_FireRate)
	{
		//Check if the target node is currently in use
		Node* pTargetNode = Pathfinding::GetInstance()->NodeFromWorldPoints(m_pSpawnSocket->GetTransform()->GetWorldPosition());
		if (pTargetNode->GetOccupyingObject() != nullptr)
			return;

		--m_Ammo;
		m_FireTimer = 0.0f;

		//Check if the node is in range
		if (MathHelp::DistanceXZ(pTargetNode->GetPosition(), GetTransform()->GetWorldPosition()) > m_Range)
			return;

		//Drop a wall
		FakeWall* pFakeWall = new FakeWall();
		pFakeWall->GetTransform()->Translate(pTargetNode->GetPosition());
		pFakeWall->Initialize(gameContext);
		GetScene()->AddChild(pFakeWall);
		pTargetNode->SetOccupyingObject(pFakeWall);
		pTargetNode->SetWalkable(false);

		SoundManager::GetInstance()->GetSystem()->playSound(m_pSound, nullptr, false, &m_pChannel);
	}
}

FakeWallWeapon::~FakeWallWeapon()
{
}
