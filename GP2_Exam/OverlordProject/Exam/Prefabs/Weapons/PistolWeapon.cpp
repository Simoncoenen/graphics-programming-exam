#include "stdafx.h"
#include "PistolWeapon.h"
#include "./Components/Components.h"
#include <Exam/Helpers/Enumerations.h>
#include <Exam/Prefabs/Weapons/MuzzleFlash.h>
#include <Base/SoundManager.h>

PistolWeapon::PistolWeapon():
	Weapon()
{
}

PistolWeapon::~PistolWeapon()
{
}

void PistolWeapon::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	m_pModelComponent = new ModelComponent(L"./Resources/Meshes/Weapons/Pistol.ovm");
	m_pModelComponent->SetMaterial(MaterialIndex::mSwatch);
	AddComponent(m_pModelComponent);

	m_pMuzzleFlash = new MuzzleFlash();
	m_pMuzzleFlash->GetTransform()->Translate(XMFLOAT3(0.0f, 1.0f, -2.2f));
	AddChild(m_pMuzzleFlash);

	//Sounds
	m_pSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/Weapons/pistol.wav", FMOD_2D);
}

void PistolWeapon::Shoot(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	if(m_FireTimer > m_FireRate)
	{
		SoundManager::GetInstance()->GetSystem()->playSound(m_pSound, nullptr, false, &m_pChannel);

		m_pMuzzleFlash->Activate();
		m_FireTimer = 0.0f;

		PxVec3 weaponPos = ToPxVec3(GetTransform()->GetWorldPosition());
		PxVec3 rayStart = weaponPos;
		PxVec3 rayDir = -ToPxVec3(GetTransform()->GetForward());

		GameObject* hitObject = GetHit(rayStart, rayDir, m_Range);
		if (hitObject != nullptr)
			DealDamage(hitObject);
	}
}