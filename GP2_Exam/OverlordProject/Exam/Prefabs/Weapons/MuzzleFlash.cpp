#include "stdafx.h"
#include "MuzzleFlash.h"
#include <Components/Components.h>
#include <Exam/Helpers/Enumerations.h>

MuzzleFlash::MuzzleFlash():
	m_VisibilityTime(0.1f),
	m_Timer(m_VisibilityTime),
	m_pModelComponent(nullptr)
{
}

MuzzleFlash::~MuzzleFlash()
{
}

void MuzzleFlash::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	m_pModelComponent = new ModelComponent(L"./Resources/Meshes/Weapons/MuzzleFlash.ovm");
	m_pModelComponent->SetMaterial(MaterialIndex::mMuzzleFlash);
	m_pModelComponent->CastShadows(false);
	AddComponent(m_pModelComponent);
}

void MuzzleFlash::Update(const GameContext& gameContext)
{
	m_Timer += gameContext.pGameTime->GetElapsed();
	if(m_Timer >= m_VisibilityTime)
		m_pModelComponent->SetDraw(false);
	else
		m_pModelComponent->SetDraw(true);
}

void MuzzleFlash::Activate()
{
	m_Timer = 0.0f;
	GetTransform()->Rotate(0.0f, 0.0f, static_cast<float>(rand() % 360));
}