#include "Weapon.h"

class BarrelWeapon :public Weapon
{
public:
	BarrelWeapon();
	~BarrelWeapon();

	void Initialize(const GameContext& gameContext);
	void Shoot(const GameContext& gameContext);
};