#include "stdafx.h"
#include "ShotgunWeapon.h"
#include "./Components/Components.h"
#include <Exam/Helpers/Enumerations.h>
#include <Exam/Prefabs/Weapons/MuzzleFlash.h>
#include <Base/SoundManager.h>

ShotgunWeapon::ShotgunWeapon() :
	Weapon()
{
}

ShotgunWeapon::~ShotgunWeapon()
{
}

void ShotgunWeapon::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	m_pModelComponent = new ModelComponent(L"./Resources/Meshes/Weapons/Shotgun.ovm");
	m_pModelComponent->SetMaterial(MaterialIndex::mSwatch);
	AddComponent(m_pModelComponent);

	m_pMuzzleFlash = new MuzzleFlash();
	m_pMuzzleFlash->GetTransform()->Translate(XMFLOAT3(0.0f, 1.0f, -4.6f));
	m_pMuzzleFlash->GetTransform()->Scale(1.4f, 1.4f, 1.4f);
	AddChild(m_pMuzzleFlash);

	//Sounds
	m_pSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/Weapons/shotgun.wav", FMOD_2D);
}

void ShotgunWeapon::SetSpread(const float spread)
{
	m_Spread = spread;
}

void ShotgunWeapon::Shoot(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	if (m_FireTimer > m_FireRate)
	{
		SoundManager::GetInstance()->GetSystem()->playSound(m_pSound, nullptr, false, &m_pChannel);

		m_pMuzzleFlash->Activate();

		m_FireTimer = 0.0f;
		--m_Ammo;

		PxVec3 weaponPos = ToPxVec3(GetTransform()->GetWorldPosition());
		PxVec3 rayStart = weaponPos;

		//Position initialization
		for (int i = 0; i < m_Shots; i++)
		{
			XMFLOAT3 randomDirection = GetTransform()->GetForward();
			XMVECTOR xmDirection = -XMLoadFloat3(&randomDirection);
			XMMATRIX randomMatrix = XMMatrixRotationRollPitchYaw(randF(-m_Spread, m_Spread), randF(-m_Spread, m_Spread), randF(-m_Spread, m_Spread));
			xmDirection = XMVector3Transform(xmDirection, randomMatrix);
			XMStoreFloat3(&randomDirection, xmDirection);
			PxVec3 rayDir = ToPxVec3(randomDirection).getNormalized();

			GameObject* hitObject = GetHit(rayStart, rayDir, m_Range);
			if (hitObject != nullptr)
				DealDamage(hitObject);
		}
	}
}