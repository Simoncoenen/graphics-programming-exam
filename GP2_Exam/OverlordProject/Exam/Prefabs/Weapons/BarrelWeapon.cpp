#include "stdafx.h"
#include "BarrelWeapon.h"
#include <Scenegraph/GameScene.h>
#include <Exam/Prefabs/Objects/Barrel.h>
#include <Components/Components.h>
#include <Exam/AI/Pathfinding/Pathfinding.h>
#include <Exam/AI/Pathfinding/Node.h>
#include <Exam/Helpers/MathHelp.h>
#include <Base/SoundManager.h>

BarrelWeapon::BarrelWeapon()
{}

BarrelWeapon::~BarrelWeapon()
{}

void BarrelWeapon::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	//Sounds
	m_pSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/ItemPlace.mp3", FMOD_2D);
}

void BarrelWeapon::Shoot(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	if(m_FireTimer >= m_FireRate)
	{
		//Check if the target node is currently in use
		Node* pTargetNode = Pathfinding::GetInstance()->NodeFromWorldPoints(m_pSpawnSocket->GetTransform()->GetWorldPosition());
		if (pTargetNode->GetOccupyingObject() != nullptr)
			return;

		--m_Ammo;
		m_FireTimer = 0.0f;

		//Check if the node is in range
		if (MathHelp::DistanceXZ(pTargetNode->GetPosition(), GetTransform()->GetWorldPosition()) > m_Range)
			return;

		//Drop a barrel
		Barrel* pBarrel = new Barrel();
		pBarrel->GetTransform()->Translate(pTargetNode->GetPosition());
		GetScene()->AddChild(pBarrel);
		pTargetNode->SetOccupyingObject(pBarrel);
		pTargetNode->SetWalkable(false);

		SoundManager::GetInstance()->GetSystem()->playSound(m_pSound, nullptr, false, &m_pChannel);
	}
}