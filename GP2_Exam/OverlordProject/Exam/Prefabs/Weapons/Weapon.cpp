#include "stdafx.h"
#include "Weapon.h"
#include "./Components/Components.h"
#include "./Physx/PhysxProxy.h"
#include "./Scenegraph/GameScene.h"
#include <Exam/Helpers/Enumerations.h>

#include <Exam/Prefabs/Objects/FakeWall.h>
#include <Exam/Prefabs/Objects/Barrel.h>
#include <Exam/AI/Enemies/BehaviourEnemy.h>

Weapon::Weapon()
{
}

Weapon::~Weapon()
{
}

void Weapon::Update(const GameContext& gameContext)
{
	m_FireTimer += gameContext.pGameTime->GetElapsed();

	//Needs to be optimized!
	if(m_pModelComponent != nullptr)
		m_pModelComponent->SetDraw(m_Active);
}

void Weapon::SetFireRate(const float fireRate)
{
	m_FireRate = fireRate;
	m_FireTimer = m_FireRate;
}

void Weapon::AddAmmo(const int ammo)
{
	m_Ammo += ammo;
	if (m_Ammo > m_MaxAmmo)
		m_Ammo = m_MaxAmmo;
}

GameObject* Weapon::GetHit(const PxVec3& origin, const PxVec3& unitDir, const PxReal distance)
{
	PhysxProxy* pPhysxProxy = GetScene()->GetPhysxProxy();
	PxRaycastBuffer hit;
	PxQueryFilterData filterData;
	filterData.data.word0 = CollisionGroup::cEnemy | CollisionGroup::cStructure | CollisionGroup::cDestructible;
	if (pPhysxProxy->Raycast(origin, unitDir, distance, hit, PxHitFlag::eDEFAULT, filterData))
	{
		auto actor = hit.block.actor;
		if (actor != nullptr)
		{
			auto userData = actor->userData;
			if (userData != nullptr)
				return reinterpret_cast<BaseComponent*>(userData)->GetGameObject();
		}
	}
	return nullptr;
}

void Weapon::DealDamage(GameObject* pObject)
{
	wstring tag = pObject->GetTag();
	if(tag == L"Enemy")
		static_cast<BehaviourEnemy*>(pObject)->Damage(m_Damage);
	if(tag == L"Fakewall")
		static_cast<FakeWall*>(pObject)->Damage(m_Damage);
	else if(tag == L"Barrel")
		static_cast<Barrel*>(pObject)->Explode();
}

void Weapon::SetAmmo(const int ammo, const bool overrideMaxAmmo)
{
	m_Ammo = ammo;
	if (m_Ammo > m_MaxAmmo)
	{
		if (overrideMaxAmmo)
			m_MaxAmmo = ammo;
		else
			m_Ammo = m_MaxAmmo;
	}
}

void Weapon::SetActive(bool active)
{
	if(m_pModelComponent != nullptr)
		m_pModelComponent->SetDraw(active);
	m_Active = active;
}
