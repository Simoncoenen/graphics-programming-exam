#include "stdafx.h"
#include "MineWeapon.h"
#include <Scenegraph/GameScene.h>
#include <Exam/Prefabs/Objects/Mine.h>
#include <Components/Components.h>
#include <Exam/AI/Pathfinding/Pathfinding.h>
#include <Exam/AI/Pathfinding/Node.h>
#include <Exam/Helpers/MathHelp.h>

MineWeapon::MineWeapon()
{}

MineWeapon::~MineWeapon()
{}

void MineWeapon::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}

void MineWeapon::Shoot(const GameContext& gameContext)
{
	if(m_FireTimer >= m_FireRate)
	{
		//Check if the target node is currently in use
		//If so, just return
		Node* pTargetNode = Pathfinding::GetInstance()->NodeFromWorldPoints(m_pSpawnSocket->GetTransform()->GetWorldPosition());
		if (pTargetNode->GetOccupyingObject() != nullptr)
			return;

		--m_Ammo;
		m_FireTimer = 0.0f;

		//Check if the node is in range
		if (MathHelp::DistanceXZ(pTargetNode->GetPosition(), GetTransform()->GetWorldPosition()) > m_Range)
			return;

		//Drop a mine
		Mine* pMine = new Mine();
		pMine->GetTransform()->Translate(pTargetNode->GetPosition());
		pMine->Initialize(gameContext);
		GetScene()->AddChild(pMine);
		pTargetNode->SetOccupyingObject(pMine);
		pTargetNode->SetWalkable(true);
	}
}