#include "Weapon.h"

class ShotgunWeapon final : public Weapon
{
public:
	ShotgunWeapon();
	~ShotgunWeapon();

	void Initialize(const GameContext& gameContext);
	void Shoot(const GameContext& gameContext);

	void SetSpread(const float spread);

private:
	float m_Spread = 0.2f;
	int m_Shots = 4;
};