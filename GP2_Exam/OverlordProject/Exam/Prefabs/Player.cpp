#include "stdafx.h"
#include "Player.h"
#include "./Physx/PhysxManager.h"
#include <Components/Components.h>

//Weapons
#include <Exam/Prefabs/Weapons/Weapon.h>
#include <Exam/Prefabs/Weapons/PistolWeapon.h>
#include <Exam/Prefabs/Weapons/UziWeapon.h>
#include <Exam/Prefabs/Weapons/ShotgunWeapon.h>
#include <Exam/Prefabs/Weapons/BarrelWeapon.h>
#include <Exam/Prefabs/Weapons/FakeWallWeapon.h>
#include <Exam/Prefabs/Weapons/MineWeapon.h>

//Helpers
#include <Exam/Helpers/MathHelp.h>
#include <Exam/Helpers/Enumerations.h>

#include <Graphics/ModelAnimator.h>
#include <Materials/Post/PostVignette.h>

#include <Exam/UI/HUDComponent.h>
#include <Scenegraph/SceneManager.h>

#include <Exam/Scenes/DeathScene.h>

#define ONLINE_HIGHSCORES
#include <Exam/Managers/Leaderboards/HighscoreEntry.h>
#include <Base/SoundManager.h>
#ifdef ONLINE_HIGHSCORES
#include <Exam/Managers/Leaderboards/OnlineLeaderboard.h>
#else
#include <Exam/Managers/Leaderboards/OfflineLeaderboard.h>
#endif

Player::Player():m_Health(m_MaxHealth)
{}

Player::~Player()
{}

#pragma region
void Player::Initialize(const GameContext& gameContext)
{
	SetTag(L"Player");

	//PhysX
	m_pController = new ControllerComponent(PhysxManager::GetInstance()->GetDefaultMaterial(), m_Radius, m_Height, L"Player", PxCapsuleClimbingMode::eEASY);
	GetTransform()->Scale(0.9f, 0.9f, 0.9f);
	AddComponent(m_pController);
	m_pController->SetCollisionGroup(CollisionGroup::cPlayer);

	//Model + Material
	GameObject* pMeshObject = new GameObject();
	ModelComponent* pModel = new ModelComponent(L"./Resources/Meshes/Characters/Player_Anim.ovm");
	pMeshObject->AddComponent(pModel);
	pModel->SetMaterial(MaterialIndex::mPlayer);
	pMeshObject->GetTransform()->Translate(0.0f, -m_Height, 0.0f);
	AddChild(pMeshObject);

	//Input
	InitializeInputs(gameContext);

	//Initiailze all the weapons
	InitializeWeapons();

	//Post processing shader for blood on the edges
	m_pPostVignette = static_cast<PostVignette*>(gameContext.pMaterialManager->GetMaterial_PP(MaterialIndex_PP::ppVignette));

	//HUD
	m_pHudComponent = new HUDComponent();
	AddComponent(m_pHudComponent);

	//Sounds
	m_pUpgradeSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/PlayerUpgrade.mp3", FMOD_2D);
	m_pDamageSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/PlayerDamage.wav", FMOD_2D);

}

void Player::PostInitialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	m_pAnimator = GetChild<GameObject>()->GetComponent<ModelComponent>()->GetAnimator();
}

wstring Player::GetCurrentWeapon()
{
	switch (static_cast<WeaponID>(m_CurrentWeapon))
	{
	case Player::PISTOL:
		return L"Pistol";
	case Player::UZI:
		return L"Uzi";
	case Player::SHOTGUN:
		return L"Shotgun";
	case Player::BARRELS:
		return L"Barrels";
	case Player::FAKEWALLS:
		return L"Fake Walls";
	case Player::MINES:
		return L"Mines";
	}
	Logger::LogWarning(L"Player::GetCurrentWeapon()... Weapon does not exist in Enum!");
	return L"";
}

void Player::InitializeInputs(const GameContext gameContext)
{
	gameContext.pInput->AddInputAction(InputAction(PlayerInput::FORWARD, InputTriggerState::Down, VK_UP));
	gameContext.pInput->AddInputAction(InputAction(PlayerInput::BACKWARD, InputTriggerState::Down, VK_DOWN));
	gameContext.pInput->AddInputAction(InputAction(PlayerInput::LEFT, InputTriggerState::Down, VK_LEFT));
	gameContext.pInput->AddInputAction(InputAction(PlayerInput::RIGHT, InputTriggerState::Down, VK_RIGHT));
	gameContext.pInput->AddInputAction(InputAction(PlayerInput::SHOOT, InputTriggerState::Down, VK_SPACE, -1 , XINPUT_GAMEPAD_X | XINPUT_GAMEPAD_A | XINPUT_GAMEPAD_B | XINPUT_GAMEPAD_Y));
	gameContext.pInput->AddInputAction(InputAction(PlayerInput::PREVWEAPON, InputTriggerState::Pressed, 'Z', -1, XINPUT_GAMEPAD_LEFT_SHOULDER));
	gameContext.pInput->AddInputAction(InputAction(PlayerInput::NEXTWEAPON, InputTriggerState::Pressed, 'X', -1, XINPUT_GAMEPAD_RIGHT_SHOULDER));
	gameContext.pInput->AddInputAction(InputAction(PlayerInput::SELECT1, InputTriggerState::Pressed, '1'));
	gameContext.pInput->AddInputAction(InputAction(PlayerInput::SELECT2, InputTriggerState::Pressed, '2'));
	gameContext.pInput->AddInputAction(InputAction(PlayerInput::SELECT3, InputTriggerState::Pressed, '3'));
	gameContext.pInput->AddInputAction(InputAction(PlayerInput::SELECT4, InputTriggerState::Pressed, '4'));
	gameContext.pInput->AddInputAction(InputAction(PlayerInput::SELECT5, InputTriggerState::Pressed, '5'));
	gameContext.pInput->AddInputAction(InputAction(PlayerInput::SELECT6, InputTriggerState::Pressed, '6'));
	gameContext.pInput->AddInputAction(InputAction(PlayerInput::GODMODE, InputTriggerState::Pressed, 'G', -1, XINPUT_GAMEPAD_BACK));
}

void Player::InitializeWeapons()
{
	//Set the position of the weapon socket
	m_pHandSocket = new GameObject();
	m_pHandSocket->GetTransform()->Translate(m_HandPosition);
	AddChild(m_pHandSocket);

	//Set the position of the spawn socket
	m_pSpawnSocket = new GameObject();
	m_pSpawnSocket->GetTransform()->Translate(m_ObjectSpawnPosition);
	AddChild(m_pSpawnSocket);

	//Pistol
	Weapon* pWeapon = new PistolWeapon();
	pWeapon->SetRange(60.0f);
	pWeapon->SetDamage(2);
	pWeapon->SetFireRate(0.8f);
	m_WeaponsArr.push_back(pWeapon);
	
	//Uzi
	pWeapon = new UziWeapon();
	pWeapon->SetAmmo(100, true);
	pWeapon->SetFireRate(0.3f);
	pWeapon->SetDamage(2);
	pWeapon->SetRange(100.0f);
	m_WeaponsArr.push_back(pWeapon);
	
	//Shotgun
	pWeapon = new ShotgunWeapon();
	pWeapon->SetAmmo(50, true);
	pWeapon->SetFireRate(1.0f);
	pWeapon->SetDamage(3);
	pWeapon->SetRange(50.0f);
	m_WeaponsArr.push_back(pWeapon);

	//Barrel
	pWeapon = new BarrelWeapon();
	pWeapon->SetAmmo(10, true);
	pWeapon->SetFireRate(1.5f);
	pWeapon->SetDamage(8);
	pWeapon->SetBulletSpawnpoint(m_pSpawnSocket);
	pWeapon->SetRange(15.0f);
	m_WeaponsArr.push_back(pWeapon);

	//FakeWalls
	pWeapon = new FakeWallWeapon();
	pWeapon->SetAmmo(10, true);
	pWeapon->SetFireRate(1.5f);
	pWeapon->SetBulletSpawnpoint(m_pSpawnSocket);
	pWeapon->SetRange(15.0f);
	m_WeaponsArr.push_back(pWeapon);

	//Mines
	pWeapon = new MineWeapon();
	pWeapon->SetAmmo(10, true);
	pWeapon->SetFireRate(1.5f);
	pWeapon->SetDamage(8);
	pWeapon->SetBulletSpawnpoint(m_pSpawnSocket);
	pWeapon->SetRange(15.0f);
	m_WeaponsArr.push_back(pWeapon);

	for (size_t i = 0; i < m_WeaponsArr.size(); i++)
	{
		m_pHandSocket->AddChild(m_WeaponsArr[i]);
		if (i != 0)
			m_WeaponsArr[i]->SetActive(false);
	}

	//todo: All weapons are unlocked
	//m_UnlockedWeapons = m_WeaponsArr.size() - 1;
}
#pragma endregion INITIALIZATION

#pragma region
void Player::Update(const GameContext& gameContext)
{
	if (m_Dead)
		return; 

	Move(gameContext);
	UpdateWeapons(gameContext);
	VignetteUpdate(gameContext);
	ScoreUpdate(gameContext);

	if (gameContext.pInput->IsActionTriggered(PlayerInput::GODMODE))
		m_GodMode = !m_GodMode;
}

void Player::Move(const GameContext& gameContext)
{
	//Accumulate inputs and calculate resulting input to apply to the controller

	XMFLOAT3 playerPos = GetTransform()->GetWorldPosition();

	float deltaTime = gameContext.pGameTime->GetElapsed();
	PxVec3 moveDirection = PxVec3(0.0f, 0.0f, 0.0f);
	if (gameContext.pInput->IsActionTriggered(PlayerInput::FORWARD) && playerPos.z < m_Bounds.z / 2)
		moveDirection += PxVec3(0.0f, 0.0f, 1.0f);
	if (gameContext.pInput->IsActionTriggered(PlayerInput::BACKWARD) && playerPos.z > -m_Bounds.z / 2)
		moveDirection -= PxVec3(0.0f, 0.0f, 1.0f);
	if (gameContext.pInput->IsActionTriggered(PlayerInput::RIGHT) && playerPos.x < m_Bounds.x / 2)
		moveDirection += PxVec3(1.0f, 0.0f, 0.0f);
	if (gameContext.pInput->IsActionTriggered(PlayerInput::LEFT) && playerPos.x > -m_Bounds.x / 2)
		moveDirection -= PxVec3(1.0f, 0.0f, 0.0f);

	//Controller movement
	XMFLOAT2 controllerInput = gameContext.pInput->GetThumbstickPosition();
	moveDirection.x += controllerInput.x;
	moveDirection.z += controllerInput.y;
	moveDirection.x = static_cast<int>(moveDirection.x * 2) / 2.0f;
	moveDirection.z = static_cast<int>(moveDirection.z * 2) / 2.0f;
	moveDirection.normalize();

	moveDirection.x *= m_MoveSpeed * deltaTime;
	moveDirection.y *= m_MoveSpeed * deltaTime;
	moveDirection.z *= m_MoveSpeed * deltaTime;

	XMFLOAT3 up = { 0.0f, 1.0f, 0.0f };

	//Rotate towards the movement direction
	
	if (moveDirection.magnitude() != 0)
	{
		m_LastDirection = ToXMFLOAT3(moveDirection);
		m_pAnimator->Play();
	}
	else
		m_pAnimator->Pause();
	XMVECTOR rotation = MathHelp::CreateLookRotation(XMLoadFloat3(&m_LastDirection), XMLoadFloat3(&up));
	GetTransform()->Rotate(rotation);

	//Apply gravity
	moveDirection.y += m_Gravity * deltaTime;
	//Apply the movement to the controller
	m_pController->Move(moveDirection);
}

void Player::UpdateWeapons(const GameContext& gameContext)
{
	//Update the current weapon
	m_WeaponsArr[m_CurrentWeapon]->Update(gameContext);

	//WEAPON SWITCHING
	//Cycling
	if (gameContext.pInput->IsActionTriggered(PlayerInput::PREVWEAPON))
		PreviousWeapon();
	if (gameContext.pInput->IsActionTriggered(PlayerInput::NEXTWEAPON))
		NextWeapon();
	//Direct selecting
	if (gameContext.pInput->IsActionTriggered(PlayerInput::SELECT1))
		SelectWeapon(WeaponID::PISTOL);
	else if (gameContext.pInput->IsActionTriggered(PlayerInput::SELECT2))
		SelectWeapon(WeaponID::UZI);
	else if (gameContext.pInput->IsActionTriggered(PlayerInput::SELECT3))
		SelectWeapon(WeaponID::SHOTGUN);
	else if (gameContext.pInput->IsActionTriggered(PlayerInput::SELECT4))
		SelectWeapon(WeaponID::BARRELS);
	else if (gameContext.pInput->IsActionTriggered(PlayerInput::SELECT5))
		SelectWeapon(WeaponID::FAKEWALLS);
	else if (gameContext.pInput->IsActionTriggered(PlayerInput::SELECT6))
		SelectWeapon(WeaponID::MINES);

	float triggerPressure = gameContext.pInput->GetTriggerPressure(false);

	//Shooting
	if (gameContext.pInput->IsActionTriggered(PlayerInput::SHOOT) || triggerPressure > 0.2f)
	{
		//If current weapon doesn't have ammo, skip to the next one and return
		if (m_WeaponsArr[m_CurrentWeapon]->GetAmmo() <= 0 && m_UnlockedWeapons > 0)
			NextWeapon();
		else
			m_WeaponsArr[m_CurrentWeapon]->Shoot(gameContext);
	}
}

void Player::ScoreUpdate(const GameContext& gameContext)
{
	if (m_CurrentCombo > 0)
	{
		m_ComboTimer -= gameContext.pGameTime->GetElapsed() * 0.1f * (float)sqrt(m_CurrentCombo);
		if (m_ComboTimer <= 0)
		{
			--m_CurrentCombo;
			m_ComboTimer = 1.0f;
		}
	}
	if (m_ShownScore < m_Score)
	{
		m_ShownScore += static_cast<int>(gameContext.pGameTime->GetElapsed() * m_LastScore * 6);
		Clamp(m_ShownScore, m_Score, 0);
	}
}

void Player::VignetteUpdate(const GameContext& gameContext)
{
	//If low health, show the blood vignette
	if (m_Health < 30)
	{
		m_VignettePulseTimer += gameContext.pGameTime->GetElapsed();
		float pulse = (sin(m_VignettePulseTimer * 5.0f) + 1.0f) / 2.0f;
		m_pPostVignette->SetRadius(0.7f * pulse / 4.0f + 0.9f);
		m_pPostVignette->SetSmoothness(0.2f);
	}
	else
	{
		m_pPostVignette->SetRadius(0.0f);
		m_pPostVignette->SetSmoothness(0.0f);
	}
}

#pragma endregion UPDATES

void Player::NextWeapon()
{
	//Deactivate current weapon
	m_WeaponsArr[m_CurrentWeapon]->SetActive(false);

	//Check for the next available weapon
	++m_CurrentWeapon;
	if (m_CurrentWeapon > m_UnlockedWeapons)
		m_CurrentWeapon = 0;
	while (m_WeaponsArr[m_CurrentWeapon]->GetAmmo() <= 0)
	{
		if (m_CurrentWeapon > m_UnlockedWeapons)
			m_CurrentWeapon = 0;
		else
			++m_CurrentWeapon;
	}

	//Activate new weapon
	m_WeaponsArr[m_CurrentWeapon]->SetActive(true);
}

void Player::PreviousWeapon()
{
	//Deactivate current weapon
	m_WeaponsArr[m_CurrentWeapon]->SetActive(false);

	//Check for the previous available weapon
	--m_CurrentWeapon;
	if (m_CurrentWeapon < 0)
		m_CurrentWeapon = m_UnlockedWeapons;
	while (m_WeaponsArr[m_CurrentWeapon]->GetAmmo() <= 0)
		--m_CurrentWeapon;

	//Activate new weapon
	m_WeaponsArr[m_CurrentWeapon]->SetActive(true);
}

void Player::SelectWeapon(const WeaponID id)
{
	if (id > m_UnlockedWeapons)
		return;
	if (m_WeaponsArr[id]->GetAmmo() < 0)
		return;
	m_WeaponsArr[m_CurrentWeapon]->SetActive(false);
	m_CurrentWeapon = id;
	m_WeaponsArr[m_CurrentWeapon]->SetActive(true);
}

void Player::AddCrate()
{
	//If every weapon is maxed, just do nothing
	if (IsAmmoMaxed())
	{
		m_pHudComponent->ShowInfo(L"Got Health!");
		m_Health = 100;
		return;
	}

	//Look for a random weapon that isn't maxed out (or health)
	int weaponIdx = randI(0, m_UnlockedWeapons);
	while (m_WeaponsArr[weaponIdx]->IsMaxed())
		weaponIdx = randI(0, m_UnlockedWeapons);

	/*  Ammo Distribution Table
	WEAPON		AMMO	FACTOR
	uzi			50		1
	shotgun		25		0.5
	barrel		5		0.1
	walls		5		0.1
	mines		5		0.1
	*/

	int ammo = 50;
	switch (static_cast<WeaponID>(weaponIdx))
	{
	case 0:
		m_pHudComponent->ShowInfo(L"Got Health!");
		m_Health = 100;
		return;
	case UZI:
		m_pHudComponent->ShowInfo(L"Got UZI ammo!");
		ammo = static_cast<int>(ammo * 1.0f);
		break;
	case SHOTGUN:
		m_pHudComponent->ShowInfo(L"Got Shotgun ammo!");
		ammo = static_cast<int>(ammo * 0.5f);
		break;
	case BARRELS:
		m_pHudComponent->ShowInfo(L"Picked up barrels!");
		ammo = static_cast<int>(ammo * 0.5f);
		break;
	case FAKEWALLS:
		m_pHudComponent->ShowInfo(L"Picked up fake walls!");
		ammo = static_cast<int>(ammo * 0.5f);
		break;
	case MINES:
		m_pHudComponent->ShowInfo(L"Picked up mines!");
		ammo = static_cast<int>(ammo * 0.5f);
		break;
	default:
		Logger::LogWarning(L"Player::AddCrate()... : weaponIdx is out of range!");
		return;
	}
	m_WeaponsArr[weaponIdx]->AddAmmo(ammo);
}

void Player::AddKill()
{
	//Add combo
	m_CurrentCombo++;
	m_ComboTimer = 1.0f;
	m_LastScore = m_CurrentCombo * 10;
	m_Score += m_LastScore;
	if (m_CurrentCombo < m_NextGoal)
		return;

	switch (m_CurrentCombo)
	{
	case 3:
		m_pHudComponent->ShowInfo(L"Pistol fire rate increased!");
		m_WeaponsArr[0]->SetFireRate(0.5f);
		break;
	case 6:
		m_pHudComponent->ShowInfo(L"Unlocked UZI");
		m_UnlockedWeapons = 1;
		break;
	case 9:
		m_pHudComponent->ShowInfo(L"Unlocked shotgun!");
		m_UnlockedWeapons = 2;
		break;
	case 12:
		m_pHudComponent->ShowInfo(L"UZI fire rate increased!");
		m_WeaponsArr[1]->SetFireRate(0.15f);
		break;
	case 15:
		m_pHudComponent->ShowInfo(L"Shotgun damage increased!");
		m_WeaponsArr[2]->SetDamage(5);
		break;
	case 18:
		m_pHudComponent->ShowInfo(L"Unlocked barrels");
		m_UnlockedWeapons = 3;
		break;
	case 21:
		m_pHudComponent->ShowInfo(L"Unlocked fake walls");
		m_UnlockedWeapons = 4;
		break;
	case 24:
		m_pHudComponent->ShowInfo(L"Unlocked mines");
		m_UnlockedWeapons = 5;
		break;
	case 27:
		m_pHudComponent->ShowInfo(L"shotgun fire rate increased!");
		m_WeaponsArr[2]->SetFireRate(0.6f);
		break;
	default:
		return;
	}
	m_NextGoal += 3;
	SoundManager::GetInstance()->GetSystem()->playSound(m_pUpgradeSound, nullptr, false, nullptr);
}

void Player::ReceiveDamage(int damage)
{
	if (m_GodMode)
		return;

	SoundManager::GetInstance()->GetSystem()->playSound(m_pDamageSound, nullptr, false, nullptr);

	m_Health -= damage;
	if (m_Health <= 0)
		Die();
}

void Player::Die()
{
	m_Dead = true;

	for (Weapon* pWpn : m_WeaponsArr)
		pWpn->Destroy();

	m_pAnimator->SetAnimation(L"Death");
	m_pAnimator->SetLoop(false);
	m_pAnimator->Play();
	m_pController = nullptr;
}

int Player::GetAmmo() const
{
	return m_WeaponsArr[m_CurrentWeapon]->GetAmmo();
}

bool Player::IsAmmoMaxed() const
{
	for (int i = 0; i < m_UnlockedWeapons; i++)
	{
		if (m_WeaponsArr[i]->IsMaxed() == false)
			return false;
	}
	return true;
}
