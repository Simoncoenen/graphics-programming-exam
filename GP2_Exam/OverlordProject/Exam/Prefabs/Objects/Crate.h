#pragma once
#include <Scenegraph\GameObject.h>

class Crate : public GameObject
{
public:
	Crate();
	~Crate();

	void Initialize(const GameContext& gameContext);

	const bool& IsUsed() const { return m_IsUsed; }
private:
	bool m_IsUsed;

	FMOD::Sound* m_pSound = nullptr;
	FMOD::Channel* m_pChannel = nullptr;
};
