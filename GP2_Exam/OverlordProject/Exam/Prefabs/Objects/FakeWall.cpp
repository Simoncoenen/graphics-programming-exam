#include "stdafx.h"
#include "FakeWall.h"
#include <Exam/AI/Pathfinding/Node.h>
#include <Exam/AI/Pathfinding/Pathfinding.h>
#include <Components/Components.h>
#include <Content/ContentManager.h>
#include <Physx/PhysxManager.h>
#include <Exam/Helpers/Enumerations.h>

FakeWall::FakeWall()
{}

FakeWall::~FakeWall()
{}

void FakeWall::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	SetTag(L"Fakewall");

	ModelComponent* pModel = new ModelComponent(L"./Resources/Meshes/Structure/1x1_wall.ovm");
	pModel->SetMaterial(MaterialIndex::mSwatch);

	RigidBodyComponent* pRigidbody = new RigidBodyComponent(true);
	pRigidbody->SetCollisionGroup(CollisionGroup::cDestructible);

	PxConvexMesh* mesh = ContentManager::Load<PxConvexMesh>(L"./Resources/Meshes/Structure/1x1_wall.ovpc");
	shared_ptr<PxGeometry> geometry(new PxConvexMeshGeometry(mesh));
	ColliderComponent* pCollider = new ColliderComponent(geometry, *PhysxManager::GetInstance()->GetDefaultMaterial());

	AddComponent(pModel);
	AddComponent(pRigidbody);
	AddComponent(pCollider);
}

void FakeWall::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}

void FakeWall::Damage(int damage)
{
	m_Health -= damage;
	if (m_Health <= 0)
	{
		//Free the node
		Node* pObjectNode = Pathfinding::GetInstance()->NodeFromWorldPoints(GetTransform()->GetWorldPosition());
		pObjectNode->SetOccupyingObject(nullptr);
		pObjectNode->SetWalkable(true);

		Destroy();
	}
}
