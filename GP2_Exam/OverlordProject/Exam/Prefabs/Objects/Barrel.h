#include <Scenegraph/GameObject.h>

class Barrel : public GameObject
{
public:
	Barrel();
	~Barrel();

	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
	void Explode() { m_Triggered = true; }
private:
	void DealDamage();

	float m_ExplosionDelay = 0.3f;
	float m_ExplosionTimer = 0.0f;
	float m_ExplosionRadius = 30.0f;
	int m_Damage = 30;
	bool m_Triggered = false;

	FMOD::Sound* m_pSound = nullptr;
	FMOD::Channel* m_pChannel = nullptr;
};

