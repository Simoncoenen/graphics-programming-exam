#include "stdafx.h"
#include "Barrel.h"
#include <Components/Components.h>
#include <Content/ContentManager.h>
#include <Physx/PhysxManager.h>
#include <Scenegraph/GameScene.h>
#include <Physx/PhysxProxy.h>
#include <Base/OverlordGame.h>

#include <Exam/AI/Pathfinding/Node.h>
#include <Exam/AI/Pathfinding/Pathfinding.h>

#include <Base/SoundManager.h>
#include <Exam/Managers/DecalManager.h>
#include <Exam/Helpers/Enumerations.h>
#include <Materials/Post/PostVignette.h>

#include <Exam/AI/Enemies/BehaviourEnemy.h>
#include <Exam/Prefabs/Player.h>

#include "Explosion.h"

Barrel::Barrel()
{}

Barrel::~Barrel()
{}

void Barrel::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	SetTag(L"Barrel");

	PxPhysics* physX = PhysxManager::GetInstance()->GetPhysics();
	PxMaterial* barrelMaterial = physX->createMaterial(1.0f, 1.0f, 0.0f);

	ModelComponent* pModelComponent = new ModelComponent(L"./Resources/Meshes/Objects/Barrel.ovm");
	pModelComponent->SetMaterial(MaterialIndex::mSwatch);
	AddComponent(pModelComponent);

	RigidBodyComponent* pRigidbodyComponent = new RigidBodyComponent(true);
	pRigidbodyComponent->SetCollisionGroup(CollisionGroup::cDestructible);
	AddComponent(pRigidbodyComponent);

	PxConvexMesh* mesh = ContentManager::Load<PxConvexMesh>(L"./Resources/Meshes/Objects/Barrel.ovpc");
	shared_ptr<PxGeometry> pBarrelGeometry(new PxConvexMeshGeometry(mesh));
	ColliderComponent* pColliderComponent = new ColliderComponent(pBarrelGeometry, *barrelMaterial, PxTransform::createIdentity());
	AddComponent(pColliderComponent);

	m_pSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/Explosion.mp3", FMOD_2D);
}

void Barrel::Update(const GameContext& gameContext)
{
	if(m_Triggered)
	{
		m_ExplosionTimer += gameContext.pGameTime->GetElapsed();
		if(m_ExplosionTimer >= m_ExplosionDelay)
		{
			//Clear node object
			Node* pObjectNode = Pathfinding::GetInstance()->NodeFromWorldPoints(GetTransform()->GetWorldPosition());
			pObjectNode->SetOccupyingObject(nullptr);
			pObjectNode->SetWalkable(true);

			DealDamage();

			//Place decal
			DecalManager::GetInstance()->Add(GetTransform()->GetWorldPosition(), DecalManager::Type::EXPLOSION, GetScene());

			//Flash post processing
			PostVignette* pFlash = static_cast<PostVignette*>(gameContext.pMaterialManager->GetMaterial_PP(MaterialIndex_PP::ppFlash));
			XMFLOAT2 screenPos = gameContext.pCamera->WorldToScreenPoint(GetTransform()->GetWorldPosition());
			auto ws = OverlordGame::GetGameSettings().Window;
			screenPos.x /= ws.Width;
			screenPos.y /= ws.Height;
			pFlash->SetCenter(screenPos);
			pFlash->SetSmoothness(1.0f);

			//Play sound
			SoundManager::GetInstance()->GetSystem()->playSound(m_pSound, nullptr, false, &m_pChannel);

			//Spawn Explosion
			Explosion* pExplosion = new Explosion(L"./Resources/Textures/Smoke.png", 5.0f);
			pExplosion->GetTransform()->Translate(GetTransform()->GetWorldPosition());
			GetScene()->AddChild(pExplosion);

			//Destroy object
			Destroy();
		}
	}
}

void Barrel::DealDamage()
{
	//Deal damage in a radius
	const PxU32 bufferSize = 256;        // [in] size of 'hitBuffer'
	PxSweepHit hitBuffer[bufferSize];  // [out] User provided buffer for results
	PxSweepBuffer buffer(hitBuffer, bufferSize); // [out] Blocking and touching hits will be stored here
	PhysxProxy* pProxy = GetScene()->GetPhysxProxy();
	PxTransform pose = PxTransform(ToPxVec3(GetTransform()->GetWorldPosition()));
	PxQueryFilterData filterData;
	filterData.data.word0 = CollisionGroup::cPlayer | CollisionGroup::cEnemy | CollisionGroup::cDestructible;

	if (pProxy->Sweep(PxSphereGeometry(m_ExplosionRadius), pose, PxVec3(1, 0, 0), 0.0f, buffer, PxHitFlag::eDEFAULT, filterData))
	{
		for (PxU32 i = 0; i < buffer.nbTouches; i++)
		{
			PxRigidActor* actor = buffer.touches[i].actor;
			if (actor != nullptr)
			{
				auto userData = actor->userData;
				if (userData != nullptr)
				{
					GameObject* hitObj = reinterpret_cast<BaseComponent*>(userData)->GetGameObject();
					if (hitObj->GetTag() == L"Player")
						reinterpret_cast<Player*>(hitObj)->ReceiveDamage(m_Damage);
					else if (hitObj->GetTag() == L"Enemy")
						reinterpret_cast<BehaviourEnemy*>(hitObj)->Damage(m_Damage);
					else if (hitObj->GetTag() == L"Barrel")
						reinterpret_cast<Barrel*>(hitObj)->Explode();
				}
			}
		}
	}
}
