#include "stdafx.h"
#include "ClothObject.h"
#include <Components/Components.h>
#include <Content/ContentManager.h>
#include <Physx/PhysxManager.h>
#include <Scenegraph/GameScene.h>
#include <Physx/PhysxProxy.h>
#include <Graphics/TextureData.h>

ID3DX11EffectMatrixVariable* ClothObject::m_pWorldVar = nullptr;
ID3DX11EffectMatrixVariable* ClothObject::m_pWvpVar = nullptr;
ID3DX11EffectShaderResourceVariable* ClothObject::m_pTextureVariable = nullptr;

ClothObject::ClothObject(const float width, const float height, float vertexDensity):
	m_Width(width),
	m_Height(height),
	m_VertexDensity(vertexDensity)
{
	m_VerticesX = int(m_Width / m_VertexDensity);
	m_VerticesY = int(m_Height / m_VertexDensity);
	m_Vertices.resize((m_VerticesX + 1) * (m_VerticesY + 1));
	m_ClothParticles.resize(m_Vertices.size());
}

ClothObject::~ClothObject()
{
	SafeRelease(m_pInputLayout);
	SafeRelease(m_pVertexBuffer);
	SafeRelease(m_pIndexBuffer);
}

void ClothObject::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	LoadEffect(gameContext);
	CreateVertices();
	CreateIndices();
	CreateCloth();
	CreateBuffers(gameContext);
}

void ClothObject::PostInitialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
}

void ClothObject::LoadEffect(const GameContext& gameContext)
{
	m_pEffect = ContentManager::Load<ID3DX11Effect>(L"./Resources/Effects/PosNormTex3D.fx");
	m_pTechnique = m_pEffect->GetTechniqueByIndex(0);

	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	UINT numElements = sizeof(layout) / sizeof(layout[0]);

	D3DX11_PASS_DESC PassDesc;
	m_pTechnique->GetPassByIndex(0)->GetDesc(&PassDesc);
	auto result = gameContext.pDevice->CreateInputLayout(layout, numElements, PassDesc.pIAInputSignature, PassDesc.IAInputSignatureSize, &m_pInputLayout);
	Logger::LogHResult(result, L"MeshDrawComponent::LoadEffect(...)");

	if (!m_pWorldVar)
		m_pWorldVar = m_pEffect->GetVariableBySemantic("World")->AsMatrix();

	if (!m_pWvpVar)
		m_pWvpVar = m_pEffect->GetVariableBySemantic("WorldViewProjection")->AsMatrix();

	if(!m_pTextureVariable)
		m_pTextureVariable = m_pEffect->GetVariableByName("gDiffuseMap")->AsShaderResource();
	if (!m_pTextureVariable->IsValid())
		Logger::LogWarning(L"ClothObject::LoadEffect > Variable 'gDiffuseMap' invalid!");
}

void ClothObject::CreateVertices()
{
	int verticesX = m_VerticesX + 1;
	for (int y = 0; y <= m_VerticesY; y++)
	{
		for (int x = 0; x <= m_VerticesX; x++)
		{
			int idx = x + y * verticesX;
			m_Vertices[idx].Position = XMFLOAT3(x * m_VertexDensity - m_Width / 2.0f, -y * m_VertexDensity - m_Height / 2.0f, 0.0f);
			m_Vertices[idx].Normal = XMFLOAT3(0.0f, 0.0f, -1.0f);
			m_Vertices[idx].TexCoord = XMFLOAT2((float)x / m_VerticesX, (float)y / m_VerticesY);

			//Cloth
			PxClothParticle p;
			p.pos = ToPxVec3(m_Vertices[idx].Position);
			p.invWeight = 1.0;
			if (y == 0)
				p.invWeight = 0.0f;
			m_ClothParticles[idx] = p;
		}
	}
}

void ClothObject::CreateIndices()
{
	int verticesX = m_VerticesX + 1;
	for (int y = 0; y < m_VerticesY; y++)
	{
		for (int x = 0; x < m_VerticesX; x++)
		{
			int idx = x + y * verticesX;
			m_Indices.push_back(idx);
			m_Indices.push_back(idx + 1);
			m_Indices.push_back(idx + verticesX + 1);

			m_Indices.push_back(idx);
			m_Indices.push_back(idx + verticesX + 1);
			m_Indices.push_back(idx + verticesX);
		}
	}
}

void ClothObject::CreateCloth()
{
	PxClothMeshDesc meshDesc;

	meshDesc.points.data = m_ClothParticles.data();
	meshDesc.points.count = m_ClothParticles.size();
	meshDesc.points.stride = sizeof(PxClothParticle);

	meshDesc.invMasses.data = &m_ClothParticles.data()->invWeight;
	meshDesc.invMasses.count = m_ClothParticles.size();
	meshDesc.invMasses.stride = sizeof(PxClothParticle);

	meshDesc.triangles.count = m_VerticesX * m_VerticesY * 2;
	meshDesc.triangles.data = m_Indices.data();
	meshDesc.triangles.stride = 3 * sizeof(DWORD);

	PxPhysics* physX = PhysxManager::GetInstance()->GetPhysics();
	PxScene* scene = GetScene()->GetPhysxProxy()->GetPhysxScene();
	PxClothFabric* fabric = PxClothFabricCreate(*physX, meshDesc, PxVec3(0.0f, -9.81f, 0.0f));

	PxTransform pose;
	pose.p = ToPxVec3(GetTransform()->GetWorldPosition());
	pose.q = ToPxQuat(GetTransform()->GetRotation());
	m_pCloth = physX->createCloth(pose, *fabric, m_ClothParticles.data(), PxClothFlags());

	m_pCloth->setStretchConfig(PxClothFabricPhaseType::eVERTICAL, PxClothStretchConfig(1.0f));
	m_pCloth->setStretchConfig(PxClothFabricPhaseType::eHORIZONTAL, PxClothStretchConfig(0.9f));
	m_pCloth->setStretchConfig(PxClothFabricPhaseType::eSHEARING, PxClothStretchConfig(0.75f));
	m_pCloth->setStretchConfig(PxClothFabricPhaseType::eBENDING, PxClothStretchConfig(0.5f));

	PxClothStretchConfig stretchConfig;
	stretchConfig.stiffness = 0.8f;
	stretchConfig.stiffnessMultiplier = 0.5f;
	stretchConfig.compressionLimit = 1.0f;
	stretchConfig.stretchLimit = 1.0f;
	m_pCloth->setStretchConfig(PxClothFabricPhaseType::eINVALID, stretchConfig);
	m_pCloth->setDragCoefficient(1.0f);
	scene->addActor(*m_pCloth);
	m_pCloth->setSelfCollisionDistance(0.1f);
	m_pCloth->setSolverFrequency(120);

	m_pCloth->setInertiaScale(1.0f);

	fabric->release();
}

void ClothObject::CreateBuffers(const GameContext& gameContext)
{
	//Vertex buffer
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.ByteWidth = sizeof(VertexPosNormTex) * m_Vertices.size();
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bd.MiscFlags = 0;
	//bd.StructureByteStride = sizeof(VertexPosNormTex);
	bd.Usage = D3D11_USAGE_DYNAMIC;

	//Create the vertex buffer
	auto hr = gameContext.pDevice->CreateBuffer(&bd, nullptr, &m_pVertexBuffer);
	Logger::LogHResult(hr, L"ClothObject::CreateBuffers() ...");

	//Index buffer
	ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));
	bd.ByteWidth = sizeof(DWORD) * m_Indices.size();
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	//bd.StructureByteStride = sizeof(DWORD);
	bd.Usage = D3D11_USAGE_IMMUTABLE;

	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = m_Indices.data();

	//Create the vertex buffer
	hr = gameContext.pDevice->CreateBuffer(&bd, &initData, &m_pIndexBuffer);
	Logger::LogHResult(hr, L"ClothObject::CreateBuffers() ...");
}

void ClothObject::Update(const GameContext& gameContext)
{
	m_pCloth->setExternalAcceleration(m_ExternalAcceleration);

	if (m_Vertices.size() == 0)
		return;

	D3D11_MAPPED_SUBRESOURCE mappedResource;
	gameContext.pDeviceContext->Map(m_pVertexBuffer, 0, D3D11_MAP_WRITE_NO_OVERWRITE, 0, &mappedResource);

	//Change the vertices
	PxClothParticleData* pData = m_pCloth->lockParticleData();
	PxClothParticle* pParticle = pData->particles;

	for (size_t i = 0; i < m_Vertices.size(); i++)
	{
		m_Vertices[i].Position = ToXMFLOAT3(pParticle[i].pos);
	}

	for (size_t i = 0; i < m_Indices.size(); i += 3)
	{
		PxVec3 p1 = pParticle[m_Indices[i]].pos;
		PxVec3 p2 = pParticle[m_Indices[i + 1]].pos;
		PxVec3 p3 = pParticle[m_Indices[i + 2]].pos;
		PxVec3 n = (p2 - p1).cross(p3 - p1);
		n = n.getNormalized();
		XMFLOAT3 normal = ToXMFLOAT3(n);
		m_Vertices[m_Indices[i]].Normal = normal;
		m_Vertices[m_Indices[i + 1]].Normal = normal;
		m_Vertices[m_Indices[i + 2]].Normal = normal;
	}

	memcpy(mappedResource.pData, m_Vertices.data(), sizeof(VertexPosNormTex) * m_Vertices.size());
	gameContext.pDeviceContext->Unmap(m_pVertexBuffer, 0);
}

void ClothObject::Draw(const GameContext& gameContext)
{
	if (m_Vertices.size() == 0)
		return;

	//Set shader variables
	auto world = XMLoadFloat4x4(&GetTransform()->GetWorld());
	auto viewProjection = XMLoadFloat4x4(&gameContext.pCamera->GetViewProjection());

	m_pWorldVar->SetMatrix(reinterpret_cast<float*>(&world));
	XMMATRIX wvp = world*viewProjection;
	m_pWvpVar->SetMatrix(reinterpret_cast<float*>(&wvp));

	m_pTextureVariable->SetResource(m_pTexture->GetShaderResourceView());

	//Configure Input Assembler
	gameContext.pDeviceContext->IASetInputLayout(m_pInputLayout);

	gameContext.pDeviceContext->IASetIndexBuffer(m_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	gameContext.pDeviceContext->IASetPrimitiveTopology(D3D10_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	UINT strides = sizeof(VertexPosNormTex);
	UINT offset = 0;
	gameContext.pDeviceContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &strides, &offset);

	//Draw
	D3DX11_TECHNIQUE_DESC techDesc;
	m_pTechnique->GetDesc(&techDesc);
	for (size_t i = 0; i < techDesc.Passes; i++)
	{
		m_pTechnique->GetPassByIndex(i)->Apply(0, gameContext.pDeviceContext);
		gameContext.pDeviceContext->DrawIndexed(m_Indices.size(), 0, 0);
	}
}

void ClothObject::SetTexture(TextureData* pTexture)
{
	m_pTexture = pTexture;
}