#include "stdafx.h"
#include "Mine.h"
#include <Components/Components.h>
#include <Physx/PhysxManager.h>
#include <Content/ContentManager.h>
#include <Physx/PhysxProxy.h>
#include <SceneGraph/GameScene.h>
#include <Base/SoundManager.h>
#include <Base/OverlordGame.h>

#include <Exam/AI/Pathfinding/Node.h>
#include <Exam/AI/Pathfinding/Pathfinding.h>
#include <Exam/Helpers/Enumerations.h>
#include <Exam/Managers/DecalManager.h>

#include <Materials/Post/PostVignette.h>

#include <Exam/Prefabs/Player.h>
#include "Barrel.h"
#include <Exam/AI/Enemies/BehaviourEnemy.h>
#include "Explosion.h"

Mine::Mine()
{}

Mine::~Mine()
{}

void Mine::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	ModelComponent* pModel = new ModelComponent(L"./Resources/Meshes/Objects/Mine.ovm");
	pModel->SetMaterial(MaterialIndex::mSwatch);
	RigidBodyComponent* pRigidbody = new RigidBodyComponent(true);
	shared_ptr<PxGeometry> geometry(new PxBoxGeometry(3.0f, 2.0f, 3.0f));
	ColliderComponent* pCollider = new ColliderComponent(geometry, *PhysxManager::GetInstance()->GetDefaultMaterial());

	AddComponent(pModel);
	AddComponent(pRigidbody);
	AddComponent(pCollider);

	m_pTriggerSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/MineTrigger.wav", FMOD_2D);

	pRigidbody->SetCollisionGroup(CollisionGroup::cEnemy);
	SetOnTriggerCallBack
	(
		[this](GameObject* trigger, GameObject* receiver, TriggerAction action)
		{
			UNREFERENCED_PARAMETER(trigger);
			if (m_Triggered)
				return;
			if (action == TriggerAction::ENTER)
			{
				if (receiver->GetTag() == L"Enemy")
				{
					SoundManager::GetInstance()->GetSystem()->playSound(m_pTriggerSound, nullptr, false, &m_pChannel);
					m_Triggered = true;
				}
			}
		}
	);
	pCollider->EnableTrigger(true);

	m_pExplosionSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/Explosion.mp3", FMOD_2D);
}

void Mine::Update(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);
	if(m_Triggered)
	{
		m_ExplostionTimer += gameContext.pGameTime->GetElapsed();
		if(m_ExplostionTimer >= m_ExplosionDelay)
		{
			//Clear node object
			Node* pObjectNode = Pathfinding::GetInstance()->NodeFromWorldPoints(GetTransform()->GetWorldPosition());
			pObjectNode->SetOccupyingObject(nullptr);
			pObjectNode->SetWalkable(true);

			DealDamage();

			//Place decal
			DecalManager::GetInstance()->Add(GetTransform()->GetWorldPosition(), DecalManager::Type::EXPLOSION, GetScene());
			PostVignette* pFlash = static_cast<PostVignette*>(gameContext.pMaterialManager->GetMaterial_PP(MaterialIndex_PP::ppFlash));
			pFlash->SetSmoothness(1.0f);

			//Play sound
			SoundManager::GetInstance()->GetSystem()->playSound(m_pExplosionSound, nullptr, false, &m_pChannel);

			//Flash effect
			XMFLOAT2 screenPos = gameContext.pCamera->WorldToScreenPoint(GetTransform()->GetWorldPosition());
			auto ws = OverlordGame::GetGameSettings().Window;
			screenPos.x /= ws.Width;
			screenPos.y /= ws.Height;
			pFlash->SetCenter(screenPos);
			pFlash->SetSmoothness(1.0f);

			//Spawn Explosion
			Explosion* pExplosion = new Explosion(L"./Resources/Textures/Smoke.png", 4.0f);
			pExplosion->GetTransform()->Translate(GetTransform()->GetWorldPosition());
			GetScene()->AddChild(pExplosion);

			Destroy();
		}
	}
}

void Mine::Explode()
{
	//Free the node
	Node* pObjectNode = Pathfinding::GetInstance()->NodeFromWorldPoints(GetTransform()->GetWorldPosition());
	pObjectNode->SetOccupyingObject(nullptr);

	Destroy();
}

void Mine::DealDamage()
{
	const PxU32 bufferSize = 256;        // [in] size of 'hitBuffer'
	PxSweepHit hitBuffer[bufferSize];  // [out] User provided buffer for results
	PxSweepBuffer buffer(hitBuffer, bufferSize); // [out] Blocking and touching hits will be stored here
	PhysxProxy* pProxy = GetScene()->GetPhysxProxy();
	PxTransform pose = PxTransform(ToPxVec3(GetTransform()->GetWorldPosition()));
	PxQueryFilterData filterData;
	filterData.data.word0 = CollisionGroup::cPlayer | CollisionGroup::cEnemy | CollisionGroup::cDestructible;

	if (pProxy->Sweep(PxSphereGeometry(m_ExplosionRadius), pose, PxVec3(1, 0, 0), 0.0f, buffer, PxHitFlag::eDEFAULT, filterData))
	{
		for (PxU32 i = 0; i < buffer.nbTouches; i++)
		{
			PxRigidActor* actor = buffer.touches[i].actor;
			if (actor != nullptr)
			{
				auto userData = actor->userData;
				if (userData != nullptr)
				{
					GameObject* hitObj = reinterpret_cast<BaseComponent*>(userData)->GetGameObject();
					if (hitObj->GetTag() == L"Player")
						reinterpret_cast<Player*>(hitObj)->ReceiveDamage(m_Damage);
					else if (hitObj->GetTag() == L"Enemy")
						reinterpret_cast<BehaviourEnemy*>(hitObj)->Damage(m_Damage);
					else if (hitObj->GetTag() == L"Barrel")
						reinterpret_cast<Barrel*>(hitObj)->Explode();
				}
			}
		}
	}
}
