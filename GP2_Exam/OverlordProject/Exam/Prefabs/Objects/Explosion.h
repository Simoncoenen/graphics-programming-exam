#pragma once
#include <Scenegraph\GameObject.h>

class Explosion : public GameObject
{
public:
	Explosion(const wstring assetPath, const float lifeTime);
	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
	~Explosion();

private:
	float m_LifeTime = 0.0f;
	wstring m_AssetPath;
};
