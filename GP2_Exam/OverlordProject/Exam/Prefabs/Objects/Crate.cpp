#include "stdafx.h"
#include "Crate.h"

#include <Physx/PhysxManager.h>
#include <Content/ContentManager.h>
#include <Base/SoundManager.h>

#include <Exam/Prefabs/Player.h>
#include <Components/Components.h>
#include <Exam/Helpers/Enumerations.h>

Crate::Crate(): m_IsUsed(false)
{}

Crate::~Crate()
{}

void Crate::Initialize(const GameContext& gameContext)
{
	UNREFERENCED_PARAMETER(gameContext);

	ModelComponent* pModel = new ModelComponent(L"./Resources/Meshes/Objects/Crate.ovm");
	pModel->SetMaterial(MaterialIndex::mSwatch);
	PxConvexMesh* pConvexMesh = ContentManager::Load<PxConvexMesh>(L"./Resources/Meshes/Objects/Crate.ovpc");
	shared_ptr<PxGeometry> geom(new PxConvexMeshGeometry(pConvexMesh));
	ColliderComponent* pCollider = new ColliderComponent(geom, *PhysxManager::GetInstance()->GetDefaultMaterial());
	RigidBodyComponent* pRigidbody = new RigidBodyComponent(true);
	AddComponent(pModel);
	AddComponent(pRigidbody);
	AddComponent(pCollider);

	pRigidbody->SetCollisionGroup(CollisionGroup::cTrigger);

	m_pSound = SoundManager::GetInstance()->LoadSound("./Resources/Sounds/CratePickup.mp3", FMOD_2D);

	SetOnTriggerCallBack([this](GameObject* trigger, GameObject* receiver, TriggerAction action)
	{
		UNREFERENCED_PARAMETER(trigger);
		if (receiver->GetTag() == L"Player")
		{
			if (action == TriggerAction::ENTER)
			{
				static_cast<Player*>(receiver)->AddCrate();
				m_IsUsed = true;
				SoundManager::GetInstance()->GetSystem()->playSound(m_pSound, nullptr, false, &m_pChannel);
			}
		}
	});
	pCollider->EnableTrigger(true);
}
