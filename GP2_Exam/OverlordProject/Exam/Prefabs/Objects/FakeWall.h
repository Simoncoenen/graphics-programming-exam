#pragma once
#include <Scenegraph/GameObject.h>

class FakeWall : public GameObject
{
public:
	FakeWall();
	~FakeWall();

	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
	void Damage(int damage);
private:
	int m_Health = 5;
};

