#pragma once
#include <Scenegraph\GameObject.h>
class Mine : public GameObject
{
public:
	Mine();
	~Mine();

	void Initialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);

private:
	void Explode();
	void DealDamage();

	int m_Damage = 40;
	float m_ExplosionDelay = 3.0f;
	float m_ExplostionTimer = 0.0f;
	float m_ExplosionRadius = 30.0f;
	bool m_Triggered = false;

	FMOD::Sound* m_pExplosionSound = nullptr;
	FMOD::Sound* m_pTriggerSound = nullptr;
	FMOD::Channel* m_pChannel = nullptr;
};