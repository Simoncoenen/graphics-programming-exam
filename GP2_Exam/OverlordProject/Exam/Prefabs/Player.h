#include "../OverlordEngine/Scenegraph/GameObject.h"
class ControllerComponent;
class Weapon;
class PostVignette;
class ModelAnimator;
class HUDComponent;

class Player : public GameObject
{
public:

	//Constructor/Destructor
	Player();
	~Player();

	//Delete copy and assignment constructor
	Player& operator=(const Player& other) = delete;
	Player(const Player& other) = delete;

	void Initialize(const GameContext& gameContext);
	void PostInitialize(const GameContext& gameContext);
	void Update(const GameContext& gameContext);
	void ReceiveDamage(int damage);
	void AddCrate();
	void AddKill();
	void EnableHighscoreRegistration(const bool enabled) { m_RegisterHighscore = enabled; }
	void EnableGodMode(bool godMode) { m_GodMode = godMode; }
	bool IsGodModeEnabled() const { return m_GodMode; }

	enum WeaponID : int
	{
		PISTOL = 0,
		UZI = 1,
		SHOTGUN = 2,
		BARRELS = 3,
		FAKEWALLS = 4,
		MINES = 5,
	};

	//Getters
	float GetHealth() const { return static_cast<float>(m_Health) / static_cast<float>(m_MaxHealth); }
	float GetComboPercent() const{ return 1.0f-m_ComboTimer; }
	int GetCombo() const { return m_CurrentCombo; }
	int GetScore() const { return m_ShownScore; }
	wstring GetCurrentWeapon();
	int GetAmmo() const;
	bool IsAmmoMaxed() const;
	void SetBounds(const XMFLOAT3& bounds) { m_Bounds = bounds; }
	bool IsDead() const { return m_Dead; }

private:
	enum PlayerInput : int
	{
		FORWARD,
		BACKWARD,
		LEFT,
		RIGHT,
		SHOOT,
		NEXTWEAPON,
		PREVWEAPON,
		SELECT1,
		SELECT2,
		SELECT3,
		SELECT4,
		SELECT5,
		SELECT6,
		GODMODE,
	};

	void InitializeInputs(const GameContext gameContext);

	//Constants
	void Die();
	void Move(const GameContext& gameContext);
	float m_Radius = 4.0f;
	float m_Height = 7.0f;
	float m_MoveSpeed = 30.0f;
	float m_Gravity = -9.81f;
	bool m_GodMode = false;
	XMFLOAT3 m_Bounds = { 100000, 0, 100000 };

	XMFLOAT3 m_HandPosition = { 3.4f, 0.25f, -4.3f };
	XMFLOAT3 m_ObjectSpawnPosition = { 0.0f, 0.0f, -12.0f };
	XMFLOAT3 m_LastDirection = {1.0f, 0.0f, 0.0f};

	int m_MaxHealth = 100;
	int m_Health;
	bool m_Dead = false;

	//Weapons
	void InitializeWeapons();
	void NextWeapon();
	void PreviousWeapon();
	void SelectWeapon(const WeaponID id);
	void UpdateWeapons(const GameContext& gameContext);

	int m_CurrentWeapon = 0;
	int m_UnlockedWeapons = 0;
	vector<Weapon*> m_WeaponsArr;
	GameObject* m_pHandSocket = nullptr;
	GameObject* m_pSpawnSocket = nullptr;

	XMFLOAT4 m_Rotation = { 0.0f, 0.0f ,0.0f ,0.0f };

	//Combo's
	void ScoreUpdate(const GameContext& gameContext);
	int m_CurrentCombo = 0;
	float m_ComboTimer = 1.0f;
	int m_NextGoal = 3;
	int m_Score = 0;
	int m_LastScore = 0;
	int m_ShownScore = 0;

	HUDComponent* m_pHudComponent = nullptr;

	//Components
	ControllerComponent* m_pController = nullptr;
	CameraComponent* m_pCamera = nullptr;

	//Animations
	ModelAnimator* m_pAnimator = nullptr;

	//Post processing
	void VignetteUpdate(const GameContext& gameContext);
	PostVignette* m_pPostVignette = nullptr;
	float m_VignettePulseTimer = 0.0f;

	//Highscores
	bool m_RegisterHighscore = true;
	bool m_HighscoreRegistered = false;

	//Sounds
	FMOD::Sound* m_pUpgradeSound = nullptr;
	FMOD::Sound* m_pDamageSound = nullptr;
};